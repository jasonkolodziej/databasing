package com.PA2.DML;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.tree.ParseTree;

public class TestSQLParser {
    public static void main( String[] args )
    {
        String sql = "SHOW ANIMALS";

        SQLLexer lexer = new SQLLexer(CharStreams.fromString(sql));
        SQLParser parser = new SQLParser(new CommonTokenStream(lexer));
        ParseTree root = parser.dmlStatement();

        System.out.println(root.toStringTree(parser));
    }
}
