// Generated from /Users/jasonkolodziej/Desktop/Fall 2018/CSCE315/PA2/Database/src/SQLParser.g4 by ANTLR 4.7
package com.PA2.DML;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLParser}.
 */
public interface SQLParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLParser#root}.
	 * @param ctx the parse tree
	 */
	void enterRoot(SQLParser.RootContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#root}.
	 * @param ctx the parse tree
	 */
	void exitRoot(SQLParser.RootContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sqlStatements}.
	 * @param ctx the parse tree
	 */
	void enterSqlStatements(SQLParser.SqlStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sqlStatements}.
	 * @param ctx the parse tree
	 */
	void exitSqlStatements(SQLParser.SqlStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sqlStatement}.
	 * @param ctx the parse tree
	 */
	void enterSqlStatement(SQLParser.SqlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sqlStatement}.
	 * @param ctx the parse tree
	 */
	void exitSqlStatement(SQLParser.SqlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#emptyStatement}.
	 * @param ctx the parse tree
	 */
	void enterEmptyStatement(SQLParser.EmptyStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#emptyStatement}.
	 * @param ctx the parse tree
	 */
	void exitEmptyStatement(SQLParser.EmptyStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ddlStatement}.
	 * @param ctx the parse tree
	 */
	void enterDdlStatement(SQLParser.DdlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ddlStatement}.
	 * @param ctx the parse tree
	 */
	void exitDdlStatement(SQLParser.DdlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dmlStatement}.
	 * @param ctx the parse tree
	 */
	void enterDmlStatement(SQLParser.DmlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dmlStatement}.
	 * @param ctx the parse tree
	 */
	void exitDmlStatement(SQLParser.DmlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transactionStatement}.
	 * @param ctx the parse tree
	 */
	void enterTransactionStatement(SQLParser.TransactionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transactionStatement}.
	 * @param ctx the parse tree
	 */
	void exitTransactionStatement(SQLParser.TransactionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#replicationStatement}.
	 * @param ctx the parse tree
	 */
	void enterReplicationStatement(SQLParser.ReplicationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#replicationStatement}.
	 * @param ctx the parse tree
	 */
	void exitReplicationStatement(SQLParser.ReplicationStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#preparedStatement}.
	 * @param ctx the parse tree
	 */
	void enterPreparedStatement(SQLParser.PreparedStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#preparedStatement}.
	 * @param ctx the parse tree
	 */
	void exitPreparedStatement(SQLParser.PreparedStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStatement(SQLParser.CompoundStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#compoundStatement}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStatement(SQLParser.CompoundStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#administrationStatement}.
	 * @param ctx the parse tree
	 */
	void enterAdministrationStatement(SQLParser.AdministrationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#administrationStatement}.
	 * @param ctx the parse tree
	 */
	void exitAdministrationStatement(SQLParser.AdministrationStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#utilityStatement}.
	 * @param ctx the parse tree
	 */
	void enterUtilityStatement(SQLParser.UtilityStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#utilityStatement}.
	 * @param ctx the parse tree
	 */
	void exitUtilityStatement(SQLParser.UtilityStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createDatabase}.
	 * @param ctx the parse tree
	 */
	void enterCreateDatabase(SQLParser.CreateDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createDatabase}.
	 * @param ctx the parse tree
	 */
	void exitCreateDatabase(SQLParser.CreateDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createEvent}.
	 * @param ctx the parse tree
	 */
	void enterCreateEvent(SQLParser.CreateEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createEvent}.
	 * @param ctx the parse tree
	 */
	void exitCreateEvent(SQLParser.CreateEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createIndex}.
	 * @param ctx the parse tree
	 */
	void enterCreateIndex(SQLParser.CreateIndexContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createIndex}.
	 * @param ctx the parse tree
	 */
	void exitCreateIndex(SQLParser.CreateIndexContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterCreateLogfileGroup(SQLParser.CreateLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitCreateLogfileGroup(SQLParser.CreateLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createProcedure}.
	 * @param ctx the parse tree
	 */
	void enterCreateProcedure(SQLParser.CreateProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createProcedure}.
	 * @param ctx the parse tree
	 */
	void exitCreateProcedure(SQLParser.CreateProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createFunction}.
	 * @param ctx the parse tree
	 */
	void enterCreateFunction(SQLParser.CreateFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createFunction}.
	 * @param ctx the parse tree
	 */
	void exitCreateFunction(SQLParser.CreateFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createServer}.
	 * @param ctx the parse tree
	 */
	void enterCreateServer(SQLParser.CreateServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createServer}.
	 * @param ctx the parse tree
	 */
	void exitCreateServer(SQLParser.CreateServerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code copyCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void enterCopyCreateTable(SQLParser.CopyCreateTableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code copyCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void exitCopyCreateTable(SQLParser.CopyCreateTableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code queryCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void enterQueryCreateTable(SQLParser.QueryCreateTableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code queryCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void exitQueryCreateTable(SQLParser.QueryCreateTableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code columnCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void enterColumnCreateTable(SQLParser.ColumnCreateTableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code columnCreateTable}
	 * labeled alternative in {@link SQLParser#createTable}.
	 * @param ctx the parse tree
	 */
	void exitColumnCreateTable(SQLParser.ColumnCreateTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createTablespaceInnodb}.
	 * @param ctx the parse tree
	 */
	void enterCreateTablespaceInnodb(SQLParser.CreateTablespaceInnodbContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createTablespaceInnodb}.
	 * @param ctx the parse tree
	 */
	void exitCreateTablespaceInnodb(SQLParser.CreateTablespaceInnodbContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createTablespaceNdb}.
	 * @param ctx the parse tree
	 */
	void enterCreateTablespaceNdb(SQLParser.CreateTablespaceNdbContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createTablespaceNdb}.
	 * @param ctx the parse tree
	 */
	void exitCreateTablespaceNdb(SQLParser.CreateTablespaceNdbContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createTrigger}.
	 * @param ctx the parse tree
	 */
	void enterCreateTrigger(SQLParser.CreateTriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createTrigger}.
	 * @param ctx the parse tree
	 */
	void exitCreateTrigger(SQLParser.CreateTriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createView}.
	 * @param ctx the parse tree
	 */
	void enterCreateView(SQLParser.CreateViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createView}.
	 * @param ctx the parse tree
	 */
	void exitCreateView(SQLParser.CreateViewContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createDatabaseOption}.
	 * @param ctx the parse tree
	 */
	void enterCreateDatabaseOption(SQLParser.CreateDatabaseOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createDatabaseOption}.
	 * @param ctx the parse tree
	 */
	void exitCreateDatabaseOption(SQLParser.CreateDatabaseOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ownerStatement}.
	 * @param ctx the parse tree
	 */
	void enterOwnerStatement(SQLParser.OwnerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ownerStatement}.
	 * @param ctx the parse tree
	 */
	void exitOwnerStatement(SQLParser.OwnerStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code preciseSchedule}
	 * labeled alternative in {@link SQLParser#scheduleExpression}.
	 * @param ctx the parse tree
	 */
	void enterPreciseSchedule(SQLParser.PreciseScheduleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code preciseSchedule}
	 * labeled alternative in {@link SQLParser#scheduleExpression}.
	 * @param ctx the parse tree
	 */
	void exitPreciseSchedule(SQLParser.PreciseScheduleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intervalSchedule}
	 * labeled alternative in {@link SQLParser#scheduleExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntervalSchedule(SQLParser.IntervalScheduleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intervalSchedule}
	 * labeled alternative in {@link SQLParser#scheduleExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntervalSchedule(SQLParser.IntervalScheduleContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#timestampValue}.
	 * @param ctx the parse tree
	 */
	void enterTimestampValue(SQLParser.TimestampValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#timestampValue}.
	 * @param ctx the parse tree
	 */
	void exitTimestampValue(SQLParser.TimestampValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#intervalExpr}.
	 * @param ctx the parse tree
	 */
	void enterIntervalExpr(SQLParser.IntervalExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#intervalExpr}.
	 * @param ctx the parse tree
	 */
	void exitIntervalExpr(SQLParser.IntervalExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#intervalType}.
	 * @param ctx the parse tree
	 */
	void enterIntervalType(SQLParser.IntervalTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#intervalType}.
	 * @param ctx the parse tree
	 */
	void exitIntervalType(SQLParser.IntervalTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#enableType}.
	 * @param ctx the parse tree
	 */
	void enterEnableType(SQLParser.EnableTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#enableType}.
	 * @param ctx the parse tree
	 */
	void exitEnableType(SQLParser.EnableTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexType}.
	 * @param ctx the parse tree
	 */
	void enterIndexType(SQLParser.IndexTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexType}.
	 * @param ctx the parse tree
	 */
	void exitIndexType(SQLParser.IndexTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexOption}.
	 * @param ctx the parse tree
	 */
	void enterIndexOption(SQLParser.IndexOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexOption}.
	 * @param ctx the parse tree
	 */
	void exitIndexOption(SQLParser.IndexOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#procedureParameter}.
	 * @param ctx the parse tree
	 */
	void enterProcedureParameter(SQLParser.ProcedureParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#procedureParameter}.
	 * @param ctx the parse tree
	 */
	void exitProcedureParameter(SQLParser.ProcedureParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#functionParameter}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParameter(SQLParser.FunctionParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#functionParameter}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParameter(SQLParser.FunctionParameterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code routineComment}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void enterRoutineComment(SQLParser.RoutineCommentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code routineComment}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void exitRoutineComment(SQLParser.RoutineCommentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code routineLanguage}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void enterRoutineLanguage(SQLParser.RoutineLanguageContext ctx);
	/**
	 * Exit a parse tree produced by the {@code routineLanguage}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void exitRoutineLanguage(SQLParser.RoutineLanguageContext ctx);
	/**
	 * Enter a parse tree produced by the {@code routineBehavior}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void enterRoutineBehavior(SQLParser.RoutineBehaviorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code routineBehavior}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void exitRoutineBehavior(SQLParser.RoutineBehaviorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code routineData}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void enterRoutineData(SQLParser.RoutineDataContext ctx);
	/**
	 * Exit a parse tree produced by the {@code routineData}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void exitRoutineData(SQLParser.RoutineDataContext ctx);
	/**
	 * Enter a parse tree produced by the {@code routineSecurity}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void enterRoutineSecurity(SQLParser.RoutineSecurityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code routineSecurity}
	 * labeled alternative in {@link SQLParser#routineOption}.
	 * @param ctx the parse tree
	 */
	void exitRoutineSecurity(SQLParser.RoutineSecurityContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#serverOption}.
	 * @param ctx the parse tree
	 */
	void enterServerOption(SQLParser.ServerOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#serverOption}.
	 * @param ctx the parse tree
	 */
	void exitServerOption(SQLParser.ServerOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createDefinitions}.
	 * @param ctx the parse tree
	 */
	void enterCreateDefinitions(SQLParser.CreateDefinitionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createDefinitions}.
	 * @param ctx the parse tree
	 */
	void exitCreateDefinitions(SQLParser.CreateDefinitionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code columnDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void enterColumnDeclaration(SQLParser.ColumnDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code columnDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void exitColumnDeclaration(SQLParser.ColumnDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constraintDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void enterConstraintDeclaration(SQLParser.ConstraintDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constraintDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void exitConstraintDeclaration(SQLParser.ConstraintDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code indexDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void enterIndexDeclaration(SQLParser.IndexDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code indexDeclaration}
	 * labeled alternative in {@link SQLParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void exitIndexDeclaration(SQLParser.IndexDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#columnDefinition}.
	 * @param ctx the parse tree
	 */
	void enterColumnDefinition(SQLParser.ColumnDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#columnDefinition}.
	 * @param ctx the parse tree
	 */
	void exitColumnDefinition(SQLParser.ColumnDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterNullColumnConstraint(SQLParser.NullColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitNullColumnConstraint(SQLParser.NullColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defaultColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterDefaultColumnConstraint(SQLParser.DefaultColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defaultColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitDefaultColumnConstraint(SQLParser.DefaultColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code autoIncrementColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterAutoIncrementColumnConstraint(SQLParser.AutoIncrementColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code autoIncrementColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitAutoIncrementColumnConstraint(SQLParser.AutoIncrementColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryKeyColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryKeyColumnConstraint(SQLParser.PrimaryKeyColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryKeyColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryKeyColumnConstraint(SQLParser.PrimaryKeyColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code uniqueKeyColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterUniqueKeyColumnConstraint(SQLParser.UniqueKeyColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code uniqueKeyColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitUniqueKeyColumnConstraint(SQLParser.UniqueKeyColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code commentColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterCommentColumnConstraint(SQLParser.CommentColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code commentColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitCommentColumnConstraint(SQLParser.CommentColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code formatColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterFormatColumnConstraint(SQLParser.FormatColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code formatColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitFormatColumnConstraint(SQLParser.FormatColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code storageColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterStorageColumnConstraint(SQLParser.StorageColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code storageColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitStorageColumnConstraint(SQLParser.StorageColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code referenceColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void enterReferenceColumnConstraint(SQLParser.ReferenceColumnConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code referenceColumnConstraint}
	 * labeled alternative in {@link SQLParser#columnConstraint}.
	 * @param ctx the parse tree
	 */
	void exitReferenceColumnConstraint(SQLParser.ReferenceColumnConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryKeyTableConstraint(SQLParser.PrimaryKeyTableConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryKeyTableConstraint(SQLParser.PrimaryKeyTableConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code uniqueKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void enterUniqueKeyTableConstraint(SQLParser.UniqueKeyTableConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code uniqueKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void exitUniqueKeyTableConstraint(SQLParser.UniqueKeyTableConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code foreignKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void enterForeignKeyTableConstraint(SQLParser.ForeignKeyTableConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code foreignKeyTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void exitForeignKeyTableConstraint(SQLParser.ForeignKeyTableConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code checkTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void enterCheckTableConstraint(SQLParser.CheckTableConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code checkTableConstraint}
	 * labeled alternative in {@link SQLParser#tableConstraint}.
	 * @param ctx the parse tree
	 */
	void exitCheckTableConstraint(SQLParser.CheckTableConstraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#referenceDefinition}.
	 * @param ctx the parse tree
	 */
	void enterReferenceDefinition(SQLParser.ReferenceDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#referenceDefinition}.
	 * @param ctx the parse tree
	 */
	void exitReferenceDefinition(SQLParser.ReferenceDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#referenceAction}.
	 * @param ctx the parse tree
	 */
	void enterReferenceAction(SQLParser.ReferenceActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#referenceAction}.
	 * @param ctx the parse tree
	 */
	void exitReferenceAction(SQLParser.ReferenceActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#referenceControlType}.
	 * @param ctx the parse tree
	 */
	void enterReferenceControlType(SQLParser.ReferenceControlTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#referenceControlType}.
	 * @param ctx the parse tree
	 */
	void exitReferenceControlType(SQLParser.ReferenceControlTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleIndexDeclaration}
	 * labeled alternative in {@link SQLParser#indexColumnDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSimpleIndexDeclaration(SQLParser.SimpleIndexDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleIndexDeclaration}
	 * labeled alternative in {@link SQLParser#indexColumnDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSimpleIndexDeclaration(SQLParser.SimpleIndexDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specialIndexDeclaration}
	 * labeled alternative in {@link SQLParser#indexColumnDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSpecialIndexDeclaration(SQLParser.SpecialIndexDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specialIndexDeclaration}
	 * labeled alternative in {@link SQLParser#indexColumnDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSpecialIndexDeclaration(SQLParser.SpecialIndexDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionEngine}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionEngine(SQLParser.TableOptionEngineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionEngine}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionEngine(SQLParser.TableOptionEngineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionAutoIncrement}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionAutoIncrement(SQLParser.TableOptionAutoIncrementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionAutoIncrement}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionAutoIncrement(SQLParser.TableOptionAutoIncrementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionAverage}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionAverage(SQLParser.TableOptionAverageContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionAverage}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionAverage(SQLParser.TableOptionAverageContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionCharset}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionCharset(SQLParser.TableOptionCharsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionCharset}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionCharset(SQLParser.TableOptionCharsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionChecksum}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionChecksum(SQLParser.TableOptionChecksumContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionChecksum}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionChecksum(SQLParser.TableOptionChecksumContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionCollate}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionCollate(SQLParser.TableOptionCollateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionCollate}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionCollate(SQLParser.TableOptionCollateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionComment}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionComment(SQLParser.TableOptionCommentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionComment}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionComment(SQLParser.TableOptionCommentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionCompression}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionCompression(SQLParser.TableOptionCompressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionCompression}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionCompression(SQLParser.TableOptionCompressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionConnection}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionConnection(SQLParser.TableOptionConnectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionConnection}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionConnection(SQLParser.TableOptionConnectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionDataDirectory}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionDataDirectory(SQLParser.TableOptionDataDirectoryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionDataDirectory}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionDataDirectory(SQLParser.TableOptionDataDirectoryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionDelay}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionDelay(SQLParser.TableOptionDelayContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionDelay}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionDelay(SQLParser.TableOptionDelayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionEncryption}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionEncryption(SQLParser.TableOptionEncryptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionEncryption}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionEncryption(SQLParser.TableOptionEncryptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionIndexDirectory}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionIndexDirectory(SQLParser.TableOptionIndexDirectoryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionIndexDirectory}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionIndexDirectory(SQLParser.TableOptionIndexDirectoryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionInsertMethod}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionInsertMethod(SQLParser.TableOptionInsertMethodContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionInsertMethod}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionInsertMethod(SQLParser.TableOptionInsertMethodContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionKeyBlockSize}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionKeyBlockSize(SQLParser.TableOptionKeyBlockSizeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionKeyBlockSize}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionKeyBlockSize(SQLParser.TableOptionKeyBlockSizeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionMaxRows}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionMaxRows(SQLParser.TableOptionMaxRowsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionMaxRows}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionMaxRows(SQLParser.TableOptionMaxRowsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionMinRows}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionMinRows(SQLParser.TableOptionMinRowsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionMinRows}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionMinRows(SQLParser.TableOptionMinRowsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionPackKeys}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionPackKeys(SQLParser.TableOptionPackKeysContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionPackKeys}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionPackKeys(SQLParser.TableOptionPackKeysContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionPassword}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionPassword(SQLParser.TableOptionPasswordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionPassword}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionPassword(SQLParser.TableOptionPasswordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionRowFormat}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionRowFormat(SQLParser.TableOptionRowFormatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionRowFormat}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionRowFormat(SQLParser.TableOptionRowFormatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionRecalculation}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionRecalculation(SQLParser.TableOptionRecalculationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionRecalculation}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionRecalculation(SQLParser.TableOptionRecalculationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionPersistent}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionPersistent(SQLParser.TableOptionPersistentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionPersistent}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionPersistent(SQLParser.TableOptionPersistentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionSamplePage}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionSamplePage(SQLParser.TableOptionSamplePageContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionSamplePage}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionSamplePage(SQLParser.TableOptionSamplePageContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionTablespace}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionTablespace(SQLParser.TableOptionTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionTablespace}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionTablespace(SQLParser.TableOptionTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableOptionUnion}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void enterTableOptionUnion(SQLParser.TableOptionUnionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableOptionUnion}
	 * labeled alternative in {@link SQLParser#tableOption}.
	 * @param ctx the parse tree
	 */
	void exitTableOptionUnion(SQLParser.TableOptionUnionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tablespaceStorage}.
	 * @param ctx the parse tree
	 */
	void enterTablespaceStorage(SQLParser.TablespaceStorageContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tablespaceStorage}.
	 * @param ctx the parse tree
	 */
	void exitTablespaceStorage(SQLParser.TablespaceStorageContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#partitionDefinitions}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinitions(SQLParser.PartitionDefinitionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#partitionDefinitions}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinitions(SQLParser.PartitionDefinitionsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionFunctionHash}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionFunctionHash(SQLParser.PartitionFunctionHashContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionFunctionHash}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionFunctionHash(SQLParser.PartitionFunctionHashContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionFunctionKey}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionFunctionKey(SQLParser.PartitionFunctionKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionFunctionKey}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionFunctionKey(SQLParser.PartitionFunctionKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionFunctionRange}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionFunctionRange(SQLParser.PartitionFunctionRangeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionFunctionRange}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionFunctionRange(SQLParser.PartitionFunctionRangeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionFunctionList}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionFunctionList(SQLParser.PartitionFunctionListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionFunctionList}
	 * labeled alternative in {@link SQLParser#partitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionFunctionList(SQLParser.PartitionFunctionListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subPartitionFunctionHash}
	 * labeled alternative in {@link SQLParser#subpartitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSubPartitionFunctionHash(SQLParser.SubPartitionFunctionHashContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subPartitionFunctionHash}
	 * labeled alternative in {@link SQLParser#subpartitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSubPartitionFunctionHash(SQLParser.SubPartitionFunctionHashContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subPartitionFunctionKey}
	 * labeled alternative in {@link SQLParser#subpartitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSubPartitionFunctionKey(SQLParser.SubPartitionFunctionKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subPartitionFunctionKey}
	 * labeled alternative in {@link SQLParser#subpartitionFunctionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSubPartitionFunctionKey(SQLParser.SubPartitionFunctionKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionComparision}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionComparision(SQLParser.PartitionComparisionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionComparision}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionComparision(SQLParser.PartitionComparisionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionListAtom}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionListAtom(SQLParser.PartitionListAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionListAtom}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionListAtom(SQLParser.PartitionListAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionListVector}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionListVector(SQLParser.PartitionListVectorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionListVector}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionListVector(SQLParser.PartitionListVectorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionSimple}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterPartitionSimple(SQLParser.PartitionSimpleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionSimple}
	 * labeled alternative in {@link SQLParser#partitionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitPartitionSimple(SQLParser.PartitionSimpleContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#partitionDefinerAtom}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinerAtom(SQLParser.PartitionDefinerAtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#partitionDefinerAtom}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinerAtom(SQLParser.PartitionDefinerAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#partitionDefinerVector}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinerVector(SQLParser.PartitionDefinerVectorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#partitionDefinerVector}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinerVector(SQLParser.PartitionDefinerVectorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#subpartitionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSubpartitionDefinition(SQLParser.SubpartitionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#subpartitionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSubpartitionDefinition(SQLParser.SubpartitionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionEngine}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionEngine(SQLParser.PartitionOptionEngineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionEngine}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionEngine(SQLParser.PartitionOptionEngineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionComment}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionComment(SQLParser.PartitionOptionCommentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionComment}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionComment(SQLParser.PartitionOptionCommentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionDataDirectory}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionDataDirectory(SQLParser.PartitionOptionDataDirectoryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionDataDirectory}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionDataDirectory(SQLParser.PartitionOptionDataDirectoryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionIndexDirectory}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionIndexDirectory(SQLParser.PartitionOptionIndexDirectoryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionIndexDirectory}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionIndexDirectory(SQLParser.PartitionOptionIndexDirectoryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionMaxRows}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionMaxRows(SQLParser.PartitionOptionMaxRowsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionMaxRows}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionMaxRows(SQLParser.PartitionOptionMaxRowsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionMinRows}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionMinRows(SQLParser.PartitionOptionMinRowsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionMinRows}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionMinRows(SQLParser.PartitionOptionMinRowsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionTablespace}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionTablespace(SQLParser.PartitionOptionTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionTablespace}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionTablespace(SQLParser.PartitionOptionTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code partitionOptionNodeGroup}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void enterPartitionOptionNodeGroup(SQLParser.PartitionOptionNodeGroupContext ctx);
	/**
	 * Exit a parse tree produced by the {@code partitionOptionNodeGroup}
	 * labeled alternative in {@link SQLParser#partitionOption}.
	 * @param ctx the parse tree
	 */
	void exitPartitionOptionNodeGroup(SQLParser.PartitionOptionNodeGroupContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterSimpleDatabase}
	 * labeled alternative in {@link SQLParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void enterAlterSimpleDatabase(SQLParser.AlterSimpleDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterSimpleDatabase}
	 * labeled alternative in {@link SQLParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void exitAlterSimpleDatabase(SQLParser.AlterSimpleDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterUpgradeName}
	 * labeled alternative in {@link SQLParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void enterAlterUpgradeName(SQLParser.AlterUpgradeNameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterUpgradeName}
	 * labeled alternative in {@link SQLParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void exitAlterUpgradeName(SQLParser.AlterUpgradeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterEvent}.
	 * @param ctx the parse tree
	 */
	void enterAlterEvent(SQLParser.AlterEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterEvent}.
	 * @param ctx the parse tree
	 */
	void exitAlterEvent(SQLParser.AlterEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterFunction}.
	 * @param ctx the parse tree
	 */
	void enterAlterFunction(SQLParser.AlterFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterFunction}.
	 * @param ctx the parse tree
	 */
	void exitAlterFunction(SQLParser.AlterFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterInstance}.
	 * @param ctx the parse tree
	 */
	void enterAlterInstance(SQLParser.AlterInstanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterInstance}.
	 * @param ctx the parse tree
	 */
	void exitAlterInstance(SQLParser.AlterInstanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterAlterLogfileGroup(SQLParser.AlterLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitAlterLogfileGroup(SQLParser.AlterLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterProcedure}.
	 * @param ctx the parse tree
	 */
	void enterAlterProcedure(SQLParser.AlterProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterProcedure}.
	 * @param ctx the parse tree
	 */
	void exitAlterProcedure(SQLParser.AlterProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterServer}.
	 * @param ctx the parse tree
	 */
	void enterAlterServer(SQLParser.AlterServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterServer}.
	 * @param ctx the parse tree
	 */
	void exitAlterServer(SQLParser.AlterServerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterTable}.
	 * @param ctx the parse tree
	 */
	void enterAlterTable(SQLParser.AlterTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterTable}.
	 * @param ctx the parse tree
	 */
	void exitAlterTable(SQLParser.AlterTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterTablespace}.
	 * @param ctx the parse tree
	 */
	void enterAlterTablespace(SQLParser.AlterTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterTablespace}.
	 * @param ctx the parse tree
	 */
	void exitAlterTablespace(SQLParser.AlterTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alterView}.
	 * @param ctx the parse tree
	 */
	void enterAlterView(SQLParser.AlterViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alterView}.
	 * @param ctx the parse tree
	 */
	void exitAlterView(SQLParser.AlterViewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByTableOption}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByTableOption(SQLParser.AlterByTableOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByTableOption}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByTableOption(SQLParser.AlterByTableOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddColumn(SQLParser.AlterByAddColumnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddColumn(SQLParser.AlterByAddColumnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddColumns}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddColumns(SQLParser.AlterByAddColumnsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddColumns}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddColumns(SQLParser.AlterByAddColumnsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddIndex(SQLParser.AlterByAddIndexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddIndex(SQLParser.AlterByAddIndexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddPrimaryKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddPrimaryKey(SQLParser.AlterByAddPrimaryKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddPrimaryKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddPrimaryKey(SQLParser.AlterByAddPrimaryKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddUniqueKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddUniqueKey(SQLParser.AlterByAddUniqueKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddUniqueKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddUniqueKey(SQLParser.AlterByAddUniqueKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddSpecialIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddSpecialIndex(SQLParser.AlterByAddSpecialIndexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddSpecialIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddSpecialIndex(SQLParser.AlterByAddSpecialIndexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddForeignKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddForeignKey(SQLParser.AlterByAddForeignKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddForeignKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddForeignKey(SQLParser.AlterByAddForeignKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddCheckTableConstraint}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddCheckTableConstraint(SQLParser.AlterByAddCheckTableConstraintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddCheckTableConstraint}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddCheckTableConstraint(SQLParser.AlterByAddCheckTableConstraintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterBySetAlgorithm}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterBySetAlgorithm(SQLParser.AlterBySetAlgorithmContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterBySetAlgorithm}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterBySetAlgorithm(SQLParser.AlterBySetAlgorithmContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByChangeDefault}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByChangeDefault(SQLParser.AlterByChangeDefaultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByChangeDefault}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByChangeDefault(SQLParser.AlterByChangeDefaultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByChangeColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByChangeColumn(SQLParser.AlterByChangeColumnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByChangeColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByChangeColumn(SQLParser.AlterByChangeColumnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByLock}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByLock(SQLParser.AlterByLockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByLock}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByLock(SQLParser.AlterByLockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByModifyColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByModifyColumn(SQLParser.AlterByModifyColumnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByModifyColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByModifyColumn(SQLParser.AlterByModifyColumnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDropColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDropColumn(SQLParser.AlterByDropColumnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDropColumn}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDropColumn(SQLParser.AlterByDropColumnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDropPrimaryKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDropPrimaryKey(SQLParser.AlterByDropPrimaryKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDropPrimaryKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDropPrimaryKey(SQLParser.AlterByDropPrimaryKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDropIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDropIndex(SQLParser.AlterByDropIndexContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDropIndex}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDropIndex(SQLParser.AlterByDropIndexContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDropForeignKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDropForeignKey(SQLParser.AlterByDropForeignKeyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDropForeignKey}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDropForeignKey(SQLParser.AlterByDropForeignKeyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDisableKeys}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDisableKeys(SQLParser.AlterByDisableKeysContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDisableKeys}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDisableKeys(SQLParser.AlterByDisableKeysContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByEnableKeys}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByEnableKeys(SQLParser.AlterByEnableKeysContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByEnableKeys}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByEnableKeys(SQLParser.AlterByEnableKeysContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByRename}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByRename(SQLParser.AlterByRenameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByRename}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByRename(SQLParser.AlterByRenameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByOrder}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByOrder(SQLParser.AlterByOrderContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByOrder}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByOrder(SQLParser.AlterByOrderContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByConvertCharset}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByConvertCharset(SQLParser.AlterByConvertCharsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByConvertCharset}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByConvertCharset(SQLParser.AlterByConvertCharsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDefaultCharset}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDefaultCharset(SQLParser.AlterByDefaultCharsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDefaultCharset}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDefaultCharset(SQLParser.AlterByDefaultCharsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDiscardTablespace}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDiscardTablespace(SQLParser.AlterByDiscardTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDiscardTablespace}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDiscardTablespace(SQLParser.AlterByDiscardTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByImportTablespace}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByImportTablespace(SQLParser.AlterByImportTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByImportTablespace}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByImportTablespace(SQLParser.AlterByImportTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByForce}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByForce(SQLParser.AlterByForceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByForce}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByForce(SQLParser.AlterByForceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByValidate}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByValidate(SQLParser.AlterByValidateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByValidate}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByValidate(SQLParser.AlterByValidateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAddPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAddPartition(SQLParser.AlterByAddPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAddPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAddPartition(SQLParser.AlterByAddPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDropPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDropPartition(SQLParser.AlterByDropPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDropPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDropPartition(SQLParser.AlterByDropPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByDiscardPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByDiscardPartition(SQLParser.AlterByDiscardPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByDiscardPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByDiscardPartition(SQLParser.AlterByDiscardPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByImportPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByImportPartition(SQLParser.AlterByImportPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByImportPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByImportPartition(SQLParser.AlterByImportPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByTruncatePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByTruncatePartition(SQLParser.AlterByTruncatePartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByTruncatePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByTruncatePartition(SQLParser.AlterByTruncatePartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByCoalescePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByCoalescePartition(SQLParser.AlterByCoalescePartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByCoalescePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByCoalescePartition(SQLParser.AlterByCoalescePartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByReorganizePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByReorganizePartition(SQLParser.AlterByReorganizePartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByReorganizePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByReorganizePartition(SQLParser.AlterByReorganizePartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByExchangePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByExchangePartition(SQLParser.AlterByExchangePartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByExchangePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByExchangePartition(SQLParser.AlterByExchangePartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByAnalyzePartitiion}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByAnalyzePartitiion(SQLParser.AlterByAnalyzePartitiionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByAnalyzePartitiion}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByAnalyzePartitiion(SQLParser.AlterByAnalyzePartitiionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByCheckPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByCheckPartition(SQLParser.AlterByCheckPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByCheckPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByCheckPartition(SQLParser.AlterByCheckPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByOptimizePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByOptimizePartition(SQLParser.AlterByOptimizePartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByOptimizePartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByOptimizePartition(SQLParser.AlterByOptimizePartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByRebuildPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByRebuildPartition(SQLParser.AlterByRebuildPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByRebuildPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByRebuildPartition(SQLParser.AlterByRebuildPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByRepairPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByRepairPartition(SQLParser.AlterByRepairPartitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByRepairPartition}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByRepairPartition(SQLParser.AlterByRepairPartitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByRemovePartitioning}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByRemovePartitioning(SQLParser.AlterByRemovePartitioningContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByRemovePartitioning}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByRemovePartitioning(SQLParser.AlterByRemovePartitioningContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterByUpgradePartitioning}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterByUpgradePartitioning(SQLParser.AlterByUpgradePartitioningContext ctx);
	/**
	 * Exit a parse tree produced by the {@code alterByUpgradePartitioning}
	 * labeled alternative in {@link SQLParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterByUpgradePartitioning(SQLParser.AlterByUpgradePartitioningContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropDatabase}.
	 * @param ctx the parse tree
	 */
	void enterDropDatabase(SQLParser.DropDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropDatabase}.
	 * @param ctx the parse tree
	 */
	void exitDropDatabase(SQLParser.DropDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropEvent}.
	 * @param ctx the parse tree
	 */
	void enterDropEvent(SQLParser.DropEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropEvent}.
	 * @param ctx the parse tree
	 */
	void exitDropEvent(SQLParser.DropEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropIndex}.
	 * @param ctx the parse tree
	 */
	void enterDropIndex(SQLParser.DropIndexContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropIndex}.
	 * @param ctx the parse tree
	 */
	void exitDropIndex(SQLParser.DropIndexContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterDropLogfileGroup(SQLParser.DropLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitDropLogfileGroup(SQLParser.DropLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropProcedure}.
	 * @param ctx the parse tree
	 */
	void enterDropProcedure(SQLParser.DropProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropProcedure}.
	 * @param ctx the parse tree
	 */
	void exitDropProcedure(SQLParser.DropProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropFunction}.
	 * @param ctx the parse tree
	 */
	void enterDropFunction(SQLParser.DropFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropFunction}.
	 * @param ctx the parse tree
	 */
	void exitDropFunction(SQLParser.DropFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropServer}.
	 * @param ctx the parse tree
	 */
	void enterDropServer(SQLParser.DropServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropServer}.
	 * @param ctx the parse tree
	 */
	void exitDropServer(SQLParser.DropServerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropTable}.
	 * @param ctx the parse tree
	 */
	void enterDropTable(SQLParser.DropTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropTable}.
	 * @param ctx the parse tree
	 */
	void exitDropTable(SQLParser.DropTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropTablespace}.
	 * @param ctx the parse tree
	 */
	void enterDropTablespace(SQLParser.DropTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropTablespace}.
	 * @param ctx the parse tree
	 */
	void exitDropTablespace(SQLParser.DropTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropTrigger}.
	 * @param ctx the parse tree
	 */
	void enterDropTrigger(SQLParser.DropTriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropTrigger}.
	 * @param ctx the parse tree
	 */
	void exitDropTrigger(SQLParser.DropTriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropView}.
	 * @param ctx the parse tree
	 */
	void enterDropView(SQLParser.DropViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropView}.
	 * @param ctx the parse tree
	 */
	void exitDropView(SQLParser.DropViewContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#renameTable}.
	 * @param ctx the parse tree
	 */
	void enterRenameTable(SQLParser.RenameTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#renameTable}.
	 * @param ctx the parse tree
	 */
	void exitRenameTable(SQLParser.RenameTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#renameTableClause}.
	 * @param ctx the parse tree
	 */
	void enterRenameTableClause(SQLParser.RenameTableClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#renameTableClause}.
	 * @param ctx the parse tree
	 */
	void exitRenameTableClause(SQLParser.RenameTableClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#truncateTable}.
	 * @param ctx the parse tree
	 */
	void enterTruncateTable(SQLParser.TruncateTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#truncateTable}.
	 * @param ctx the parse tree
	 */
	void exitTruncateTable(SQLParser.TruncateTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#callStatement}.
	 * @param ctx the parse tree
	 */
	void enterCallStatement(SQLParser.CallStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#callStatement}.
	 * @param ctx the parse tree
	 */
	void exitCallStatement(SQLParser.CallStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#deleteStatement}.
	 * @param ctx the parse tree
	 */
	void enterDeleteStatement(SQLParser.DeleteStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#deleteStatement}.
	 * @param ctx the parse tree
	 */
	void exitDeleteStatement(SQLParser.DeleteStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void enterDoStatement(SQLParser.DoStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void exitDoStatement(SQLParser.DoStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerStatement(SQLParser.HandlerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerStatement(SQLParser.HandlerStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#insertStatement}.
	 * @param ctx the parse tree
	 */
	void enterInsertStatement(SQLParser.InsertStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#insertStatement}.
	 * @param ctx the parse tree
	 */
	void exitInsertStatement(SQLParser.InsertStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadDataStatement(SQLParser.LoadDataStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadDataStatement(SQLParser.LoadDataStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadXmlStatement(SQLParser.LoadXmlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadXmlStatement(SQLParser.LoadXmlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#replaceStatement}.
	 * @param ctx the parse tree
	 */
	void enterReplaceStatement(SQLParser.ReplaceStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#replaceStatement}.
	 * @param ctx the parse tree
	 */
	void exitReplaceStatement(SQLParser.ReplaceStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterSimpleSelect(SQLParser.SimpleSelectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitSimpleSelect(SQLParser.SimpleSelectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenthesisSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterParenthesisSelect(SQLParser.ParenthesisSelectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenthesisSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitParenthesisSelect(SQLParser.ParenthesisSelectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unionSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterUnionSelect(SQLParser.UnionSelectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unionSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitUnionSelect(SQLParser.UnionSelectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unionParenthesisSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterUnionParenthesisSelect(SQLParser.UnionParenthesisSelectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unionParenthesisSelect}
	 * labeled alternative in {@link SQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitUnionParenthesisSelect(SQLParser.UnionParenthesisSelectContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#updateStatement}.
	 * @param ctx the parse tree
	 */
	void enterUpdateStatement(SQLParser.UpdateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#updateStatement}.
	 * @param ctx the parse tree
	 */
	void exitUpdateStatement(SQLParser.UpdateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#insertStatementValue}.
	 * @param ctx the parse tree
	 */
	void enterInsertStatementValue(SQLParser.InsertStatementValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#insertStatementValue}.
	 * @param ctx the parse tree
	 */
	void exitInsertStatementValue(SQLParser.InsertStatementValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#updatedElement}.
	 * @param ctx the parse tree
	 */
	void enterUpdatedElement(SQLParser.UpdatedElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#updatedElement}.
	 * @param ctx the parse tree
	 */
	void exitUpdatedElement(SQLParser.UpdatedElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#assignmentField}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentField(SQLParser.AssignmentFieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#assignmentField}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentField(SQLParser.AssignmentFieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void enterLockClause(SQLParser.LockClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void exitLockClause(SQLParser.LockClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#singleDeleteStatement}.
	 * @param ctx the parse tree
	 */
	void enterSingleDeleteStatement(SQLParser.SingleDeleteStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#singleDeleteStatement}.
	 * @param ctx the parse tree
	 */
	void exitSingleDeleteStatement(SQLParser.SingleDeleteStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#multipleDeleteStatement}.
	 * @param ctx the parse tree
	 */
	void enterMultipleDeleteStatement(SQLParser.MultipleDeleteStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#multipleDeleteStatement}.
	 * @param ctx the parse tree
	 */
	void exitMultipleDeleteStatement(SQLParser.MultipleDeleteStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerOpenStatement(SQLParser.HandlerOpenStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerOpenStatement(SQLParser.HandlerOpenStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadIndexStatement(SQLParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadIndexStatement(SQLParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadStatement(SQLParser.HandlerReadStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadStatement(SQLParser.HandlerReadStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerCloseStatement(SQLParser.HandlerCloseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerCloseStatement(SQLParser.HandlerCloseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#singleUpdateStatement}.
	 * @param ctx the parse tree
	 */
	void enterSingleUpdateStatement(SQLParser.SingleUpdateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#singleUpdateStatement}.
	 * @param ctx the parse tree
	 */
	void exitSingleUpdateStatement(SQLParser.SingleUpdateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#multipleUpdateStatement}.
	 * @param ctx the parse tree
	 */
	void enterMultipleUpdateStatement(SQLParser.MultipleUpdateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#multipleUpdateStatement}.
	 * @param ctx the parse tree
	 */
	void exitMultipleUpdateStatement(SQLParser.MultipleUpdateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void enterOrderByClause(SQLParser.OrderByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void exitOrderByClause(SQLParser.OrderByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#orderByExpression}.
	 * @param ctx the parse tree
	 */
	void enterOrderByExpression(SQLParser.OrderByExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#orderByExpression}.
	 * @param ctx the parse tree
	 */
	void exitOrderByExpression(SQLParser.OrderByExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tableSources}.
	 * @param ctx the parse tree
	 */
	void enterTableSources(SQLParser.TableSourcesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tableSources}.
	 * @param ctx the parse tree
	 */
	void exitTableSources(SQLParser.TableSourcesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableSourceBase}
	 * labeled alternative in {@link SQLParser#tableSource}.
	 * @param ctx the parse tree
	 */
	void enterTableSourceBase(SQLParser.TableSourceBaseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableSourceBase}
	 * labeled alternative in {@link SQLParser#tableSource}.
	 * @param ctx the parse tree
	 */
	void exitTableSourceBase(SQLParser.TableSourceBaseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableSourceNested}
	 * labeled alternative in {@link SQLParser#tableSource}.
	 * @param ctx the parse tree
	 */
	void enterTableSourceNested(SQLParser.TableSourceNestedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableSourceNested}
	 * labeled alternative in {@link SQLParser#tableSource}.
	 * @param ctx the parse tree
	 */
	void exitTableSourceNested(SQLParser.TableSourceNestedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomTableItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void enterAtomTableItem(SQLParser.AtomTableItemContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atomTableItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void exitAtomTableItem(SQLParser.AtomTableItemContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subqueryTableItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void enterSubqueryTableItem(SQLParser.SubqueryTableItemContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subqueryTableItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void exitSubqueryTableItem(SQLParser.SubqueryTableItemContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableSourcesItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void enterTableSourcesItem(SQLParser.TableSourcesItemContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableSourcesItem}
	 * labeled alternative in {@link SQLParser#tableSourceItem}.
	 * @param ctx the parse tree
	 */
	void exitTableSourcesItem(SQLParser.TableSourcesItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexHint}.
	 * @param ctx the parse tree
	 */
	void enterIndexHint(SQLParser.IndexHintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexHint}.
	 * @param ctx the parse tree
	 */
	void exitIndexHint(SQLParser.IndexHintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexHintType}.
	 * @param ctx the parse tree
	 */
	void enterIndexHintType(SQLParser.IndexHintTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexHintType}.
	 * @param ctx the parse tree
	 */
	void exitIndexHintType(SQLParser.IndexHintTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code innerJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void enterInnerJoin(SQLParser.InnerJoinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code innerJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void exitInnerJoin(SQLParser.InnerJoinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code straightJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void enterStraightJoin(SQLParser.StraightJoinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code straightJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void exitStraightJoin(SQLParser.StraightJoinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code outerJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void enterOuterJoin(SQLParser.OuterJoinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code outerJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void exitOuterJoin(SQLParser.OuterJoinContext ctx);
	/**
	 * Enter a parse tree produced by the {@code naturalJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void enterNaturalJoin(SQLParser.NaturalJoinContext ctx);
	/**
	 * Exit a parse tree produced by the {@code naturalJoin}
	 * labeled alternative in {@link SQLParser#joinPart}.
	 * @param ctx the parse tree
	 */
	void exitNaturalJoin(SQLParser.NaturalJoinContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#queryExpression}.
	 * @param ctx the parse tree
	 */
	void enterQueryExpression(SQLParser.QueryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#queryExpression}.
	 * @param ctx the parse tree
	 */
	void exitQueryExpression(SQLParser.QueryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#queryExpressionNointo}.
	 * @param ctx the parse tree
	 */
	void enterQueryExpressionNointo(SQLParser.QueryExpressionNointoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#queryExpressionNointo}.
	 * @param ctx the parse tree
	 */
	void exitQueryExpressionNointo(SQLParser.QueryExpressionNointoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#querySpecification}.
	 * @param ctx the parse tree
	 */
	void enterQuerySpecification(SQLParser.QuerySpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#querySpecification}.
	 * @param ctx the parse tree
	 */
	void exitQuerySpecification(SQLParser.QuerySpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#querySpecificationNointo}.
	 * @param ctx the parse tree
	 */
	void enterQuerySpecificationNointo(SQLParser.QuerySpecificationNointoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#querySpecificationNointo}.
	 * @param ctx the parse tree
	 */
	void exitQuerySpecificationNointo(SQLParser.QuerySpecificationNointoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unionParenthesis}.
	 * @param ctx the parse tree
	 */
	void enterUnionParenthesis(SQLParser.UnionParenthesisContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unionParenthesis}.
	 * @param ctx the parse tree
	 */
	void exitUnionParenthesis(SQLParser.UnionParenthesisContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unionStatement}.
	 * @param ctx the parse tree
	 */
	void enterUnionStatement(SQLParser.UnionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unionStatement}.
	 * @param ctx the parse tree
	 */
	void exitUnionStatement(SQLParser.UnionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#selectSpec}.
	 * @param ctx the parse tree
	 */
	void enterSelectSpec(SQLParser.SelectSpecContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#selectSpec}.
	 * @param ctx the parse tree
	 */
	void exitSelectSpec(SQLParser.SelectSpecContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#selectElements}.
	 * @param ctx the parse tree
	 */
	void enterSelectElements(SQLParser.SelectElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#selectElements}.
	 * @param ctx the parse tree
	 */
	void exitSelectElements(SQLParser.SelectElementsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectStarElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void enterSelectStarElement(SQLParser.SelectStarElementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectStarElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void exitSelectStarElement(SQLParser.SelectStarElementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectColumnElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void enterSelectColumnElement(SQLParser.SelectColumnElementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectColumnElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void exitSelectColumnElement(SQLParser.SelectColumnElementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectFunctionElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void enterSelectFunctionElement(SQLParser.SelectFunctionElementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectFunctionElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void exitSelectFunctionElement(SQLParser.SelectFunctionElementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectExpressionElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void enterSelectExpressionElement(SQLParser.SelectExpressionElementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectExpressionElement}
	 * labeled alternative in {@link SQLParser#selectElement}.
	 * @param ctx the parse tree
	 */
	void exitSelectExpressionElement(SQLParser.SelectExpressionElementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectIntoVariables}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void enterSelectIntoVariables(SQLParser.SelectIntoVariablesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectIntoVariables}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void exitSelectIntoVariables(SQLParser.SelectIntoVariablesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectIntoDumpFile}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void enterSelectIntoDumpFile(SQLParser.SelectIntoDumpFileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectIntoDumpFile}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void exitSelectIntoDumpFile(SQLParser.SelectIntoDumpFileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectIntoTextFile}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void enterSelectIntoTextFile(SQLParser.SelectIntoTextFileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectIntoTextFile}
	 * labeled alternative in {@link SQLParser#selectIntoExpression}.
	 * @param ctx the parse tree
	 */
	void exitSelectIntoTextFile(SQLParser.SelectIntoTextFileContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#selectFieldsInto}.
	 * @param ctx the parse tree
	 */
	void enterSelectFieldsInto(SQLParser.SelectFieldsIntoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#selectFieldsInto}.
	 * @param ctx the parse tree
	 */
	void exitSelectFieldsInto(SQLParser.SelectFieldsIntoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#selectLinesInto}.
	 * @param ctx the parse tree
	 */
	void enterSelectLinesInto(SQLParser.SelectLinesIntoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#selectLinesInto}.
	 * @param ctx the parse tree
	 */
	void exitSelectLinesInto(SQLParser.SelectLinesIntoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void enterFromClause(SQLParser.FromClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void exitFromClause(SQLParser.FromClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#groupByItem}.
	 * @param ctx the parse tree
	 */
	void enterGroupByItem(SQLParser.GroupByItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#groupByItem}.
	 * @param ctx the parse tree
	 */
	void exitGroupByItem(SQLParser.GroupByItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void enterLimitClause(SQLParser.LimitClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void exitLimitClause(SQLParser.LimitClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#startTransaction}.
	 * @param ctx the parse tree
	 */
	void enterStartTransaction(SQLParser.StartTransactionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#startTransaction}.
	 * @param ctx the parse tree
	 */
	void exitStartTransaction(SQLParser.StartTransactionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#beginWork}.
	 * @param ctx the parse tree
	 */
	void enterBeginWork(SQLParser.BeginWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#beginWork}.
	 * @param ctx the parse tree
	 */
	void exitBeginWork(SQLParser.BeginWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#commitWork}.
	 * @param ctx the parse tree
	 */
	void enterCommitWork(SQLParser.CommitWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#commitWork}.
	 * @param ctx the parse tree
	 */
	void exitCommitWork(SQLParser.CommitWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#rollbackWork}.
	 * @param ctx the parse tree
	 */
	void enterRollbackWork(SQLParser.RollbackWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#rollbackWork}.
	 * @param ctx the parse tree
	 */
	void exitRollbackWork(SQLParser.RollbackWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#savepointStatement}.
	 * @param ctx the parse tree
	 */
	void enterSavepointStatement(SQLParser.SavepointStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#savepointStatement}.
	 * @param ctx the parse tree
	 */
	void exitSavepointStatement(SQLParser.SavepointStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#rollbackStatement}.
	 * @param ctx the parse tree
	 */
	void enterRollbackStatement(SQLParser.RollbackStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#rollbackStatement}.
	 * @param ctx the parse tree
	 */
	void exitRollbackStatement(SQLParser.RollbackStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#releaseStatement}.
	 * @param ctx the parse tree
	 */
	void enterReleaseStatement(SQLParser.ReleaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#releaseStatement}.
	 * @param ctx the parse tree
	 */
	void exitReleaseStatement(SQLParser.ReleaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lockTables}.
	 * @param ctx the parse tree
	 */
	void enterLockTables(SQLParser.LockTablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lockTables}.
	 * @param ctx the parse tree
	 */
	void exitLockTables(SQLParser.LockTablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unlockTables}.
	 * @param ctx the parse tree
	 */
	void enterUnlockTables(SQLParser.UnlockTablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unlockTables}.
	 * @param ctx the parse tree
	 */
	void exitUnlockTables(SQLParser.UnlockTablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#setAutocommitStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetAutocommitStatement(SQLParser.SetAutocommitStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#setAutocommitStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetAutocommitStatement(SQLParser.SetAutocommitStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#setTransactionStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetTransactionStatement(SQLParser.SetTransactionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#setTransactionStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetTransactionStatement(SQLParser.SetTransactionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transactionMode}.
	 * @param ctx the parse tree
	 */
	void enterTransactionMode(SQLParser.TransactionModeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transactionMode}.
	 * @param ctx the parse tree
	 */
	void exitTransactionMode(SQLParser.TransactionModeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lockTableElement}.
	 * @param ctx the parse tree
	 */
	void enterLockTableElement(SQLParser.LockTableElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lockTableElement}.
	 * @param ctx the parse tree
	 */
	void exitLockTableElement(SQLParser.LockTableElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lockAction}.
	 * @param ctx the parse tree
	 */
	void enterLockAction(SQLParser.LockActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lockAction}.
	 * @param ctx the parse tree
	 */
	void exitLockAction(SQLParser.LockActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transactionOption}.
	 * @param ctx the parse tree
	 */
	void enterTransactionOption(SQLParser.TransactionOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transactionOption}.
	 * @param ctx the parse tree
	 */
	void exitTransactionOption(SQLParser.TransactionOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transactionLevel}.
	 * @param ctx the parse tree
	 */
	void enterTransactionLevel(SQLParser.TransactionLevelContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transactionLevel}.
	 * @param ctx the parse tree
	 */
	void exitTransactionLevel(SQLParser.TransactionLevelContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#changeMaster}.
	 * @param ctx the parse tree
	 */
	void enterChangeMaster(SQLParser.ChangeMasterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#changeMaster}.
	 * @param ctx the parse tree
	 */
	void exitChangeMaster(SQLParser.ChangeMasterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#changeReplicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterChangeReplicationFilter(SQLParser.ChangeReplicationFilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#changeReplicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitChangeReplicationFilter(SQLParser.ChangeReplicationFilterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#purgeBinaryLogs}.
	 * @param ctx the parse tree
	 */
	void enterPurgeBinaryLogs(SQLParser.PurgeBinaryLogsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#purgeBinaryLogs}.
	 * @param ctx the parse tree
	 */
	void exitPurgeBinaryLogs(SQLParser.PurgeBinaryLogsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#resetMaster}.
	 * @param ctx the parse tree
	 */
	void enterResetMaster(SQLParser.ResetMasterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#resetMaster}.
	 * @param ctx the parse tree
	 */
	void exitResetMaster(SQLParser.ResetMasterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#resetSlave}.
	 * @param ctx the parse tree
	 */
	void enterResetSlave(SQLParser.ResetSlaveContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#resetSlave}.
	 * @param ctx the parse tree
	 */
	void exitResetSlave(SQLParser.ResetSlaveContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#startSlave}.
	 * @param ctx the parse tree
	 */
	void enterStartSlave(SQLParser.StartSlaveContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#startSlave}.
	 * @param ctx the parse tree
	 */
	void exitStartSlave(SQLParser.StartSlaveContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#stopSlave}.
	 * @param ctx the parse tree
	 */
	void enterStopSlave(SQLParser.StopSlaveContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#stopSlave}.
	 * @param ctx the parse tree
	 */
	void exitStopSlave(SQLParser.StopSlaveContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#startGroupReplication}.
	 * @param ctx the parse tree
	 */
	void enterStartGroupReplication(SQLParser.StartGroupReplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#startGroupReplication}.
	 * @param ctx the parse tree
	 */
	void exitStartGroupReplication(SQLParser.StartGroupReplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#stopGroupReplication}.
	 * @param ctx the parse tree
	 */
	void enterStopGroupReplication(SQLParser.StopGroupReplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#stopGroupReplication}.
	 * @param ctx the parse tree
	 */
	void exitStopGroupReplication(SQLParser.StopGroupReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterStringOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterStringOption(SQLParser.MasterStringOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterStringOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterStringOption(SQLParser.MasterStringOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterDecimalOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterDecimalOption(SQLParser.MasterDecimalOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterDecimalOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterDecimalOption(SQLParser.MasterDecimalOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterBoolOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterBoolOption(SQLParser.MasterBoolOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterBoolOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterBoolOption(SQLParser.MasterBoolOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterRealOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterRealOption(SQLParser.MasterRealOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterRealOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterRealOption(SQLParser.MasterRealOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterUidListOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterUidListOption(SQLParser.MasterUidListOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterUidListOption}
	 * labeled alternative in {@link SQLParser#masterOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterUidListOption(SQLParser.MasterUidListOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#stringMasterOption}.
	 * @param ctx the parse tree
	 */
	void enterStringMasterOption(SQLParser.StringMasterOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#stringMasterOption}.
	 * @param ctx the parse tree
	 */
	void exitStringMasterOption(SQLParser.StringMasterOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#decimalMasterOption}.
	 * @param ctx the parse tree
	 */
	void enterDecimalMasterOption(SQLParser.DecimalMasterOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#decimalMasterOption}.
	 * @param ctx the parse tree
	 */
	void exitDecimalMasterOption(SQLParser.DecimalMasterOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#boolMasterOption}.
	 * @param ctx the parse tree
	 */
	void enterBoolMasterOption(SQLParser.BoolMasterOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#boolMasterOption}.
	 * @param ctx the parse tree
	 */
	void exitBoolMasterOption(SQLParser.BoolMasterOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#channelOption}.
	 * @param ctx the parse tree
	 */
	void enterChannelOption(SQLParser.ChannelOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#channelOption}.
	 * @param ctx the parse tree
	 */
	void exitChannelOption(SQLParser.ChannelOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterDoDbReplication(SQLParser.DoDbReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitDoDbReplication(SQLParser.DoDbReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ignoreDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterIgnoreDbReplication(SQLParser.IgnoreDbReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ignoreDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitIgnoreDbReplication(SQLParser.IgnoreDbReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterDoTableReplication(SQLParser.DoTableReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitDoTableReplication(SQLParser.DoTableReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ignoreTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterIgnoreTableReplication(SQLParser.IgnoreTableReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ignoreTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitIgnoreTableReplication(SQLParser.IgnoreTableReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wildDoTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterWildDoTableReplication(SQLParser.WildDoTableReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wildDoTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitWildDoTableReplication(SQLParser.WildDoTableReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wildIgnoreTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterWildIgnoreTableReplication(SQLParser.WildIgnoreTableReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wildIgnoreTableReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitWildIgnoreTableReplication(SQLParser.WildIgnoreTableReplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code rewriteDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void enterRewriteDbReplication(SQLParser.RewriteDbReplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code rewriteDbReplication}
	 * labeled alternative in {@link SQLParser#replicationFilter}.
	 * @param ctx the parse tree
	 */
	void exitRewriteDbReplication(SQLParser.RewriteDbReplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tablePair}.
	 * @param ctx the parse tree
	 */
	void enterTablePair(SQLParser.TablePairContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tablePair}.
	 * @param ctx the parse tree
	 */
	void exitTablePair(SQLParser.TablePairContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#threadType}.
	 * @param ctx the parse tree
	 */
	void enterThreadType(SQLParser.ThreadTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#threadType}.
	 * @param ctx the parse tree
	 */
	void exitThreadType(SQLParser.ThreadTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtidsUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void enterGtidsUntilOption(SQLParser.GtidsUntilOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtidsUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void exitGtidsUntilOption(SQLParser.GtidsUntilOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code masterLogUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void enterMasterLogUntilOption(SQLParser.MasterLogUntilOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code masterLogUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void exitMasterLogUntilOption(SQLParser.MasterLogUntilOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relayLogUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void enterRelayLogUntilOption(SQLParser.RelayLogUntilOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relayLogUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void exitRelayLogUntilOption(SQLParser.RelayLogUntilOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sqlGapsUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void enterSqlGapsUntilOption(SQLParser.SqlGapsUntilOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sqlGapsUntilOption}
	 * labeled alternative in {@link SQLParser#untilOption}.
	 * @param ctx the parse tree
	 */
	void exitSqlGapsUntilOption(SQLParser.SqlGapsUntilOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code userConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void enterUserConnectionOption(SQLParser.UserConnectionOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code userConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void exitUserConnectionOption(SQLParser.UserConnectionOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code passwordConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void enterPasswordConnectionOption(SQLParser.PasswordConnectionOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code passwordConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void exitPasswordConnectionOption(SQLParser.PasswordConnectionOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defaultAuthConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void enterDefaultAuthConnectionOption(SQLParser.DefaultAuthConnectionOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defaultAuthConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void exitDefaultAuthConnectionOption(SQLParser.DefaultAuthConnectionOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code pluginDirConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void enterPluginDirConnectionOption(SQLParser.PluginDirConnectionOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code pluginDirConnectionOption}
	 * labeled alternative in {@link SQLParser#connectionOption}.
	 * @param ctx the parse tree
	 */
	void exitPluginDirConnectionOption(SQLParser.PluginDirConnectionOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#gtuidSet}.
	 * @param ctx the parse tree
	 */
	void enterGtuidSet(SQLParser.GtuidSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#gtuidSet}.
	 * @param ctx the parse tree
	 */
	void exitGtuidSet(SQLParser.GtuidSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaStartTransaction}.
	 * @param ctx the parse tree
	 */
	void enterXaStartTransaction(SQLParser.XaStartTransactionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaStartTransaction}.
	 * @param ctx the parse tree
	 */
	void exitXaStartTransaction(SQLParser.XaStartTransactionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaEndTransaction}.
	 * @param ctx the parse tree
	 */
	void enterXaEndTransaction(SQLParser.XaEndTransactionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaEndTransaction}.
	 * @param ctx the parse tree
	 */
	void exitXaEndTransaction(SQLParser.XaEndTransactionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaPrepareStatement}.
	 * @param ctx the parse tree
	 */
	void enterXaPrepareStatement(SQLParser.XaPrepareStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaPrepareStatement}.
	 * @param ctx the parse tree
	 */
	void exitXaPrepareStatement(SQLParser.XaPrepareStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaCommitWork}.
	 * @param ctx the parse tree
	 */
	void enterXaCommitWork(SQLParser.XaCommitWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaCommitWork}.
	 * @param ctx the parse tree
	 */
	void exitXaCommitWork(SQLParser.XaCommitWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaRollbackWork}.
	 * @param ctx the parse tree
	 */
	void enterXaRollbackWork(SQLParser.XaRollbackWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaRollbackWork}.
	 * @param ctx the parse tree
	 */
	void exitXaRollbackWork(SQLParser.XaRollbackWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xaRecoverWork}.
	 * @param ctx the parse tree
	 */
	void enterXaRecoverWork(SQLParser.XaRecoverWorkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xaRecoverWork}.
	 * @param ctx the parse tree
	 */
	void exitXaRecoverWork(SQLParser.XaRecoverWorkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#prepareStatement}.
	 * @param ctx the parse tree
	 */
	void enterPrepareStatement(SQLParser.PrepareStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#prepareStatement}.
	 * @param ctx the parse tree
	 */
	void exitPrepareStatement(SQLParser.PrepareStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#executeStatement}.
	 * @param ctx the parse tree
	 */
	void enterExecuteStatement(SQLParser.ExecuteStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#executeStatement}.
	 * @param ctx the parse tree
	 */
	void exitExecuteStatement(SQLParser.ExecuteStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#deallocatePrepare}.
	 * @param ctx the parse tree
	 */
	void enterDeallocatePrepare(SQLParser.DeallocatePrepareContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#deallocatePrepare}.
	 * @param ctx the parse tree
	 */
	void exitDeallocatePrepare(SQLParser.DeallocatePrepareContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void enterRoutineBody(SQLParser.RoutineBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void exitRoutineBody(SQLParser.RoutineBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(SQLParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(SQLParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void enterCaseStatement(SQLParser.CaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void exitCaseStatement(SQLParser.CaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(SQLParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(SQLParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#iterateStatement}.
	 * @param ctx the parse tree
	 */
	void enterIterateStatement(SQLParser.IterateStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#iterateStatement}.
	 * @param ctx the parse tree
	 */
	void exitIterateStatement(SQLParser.IterateStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#leaveStatement}.
	 * @param ctx the parse tree
	 */
	void enterLeaveStatement(SQLParser.LeaveStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#leaveStatement}.
	 * @param ctx the parse tree
	 */
	void exitLeaveStatement(SQLParser.LeaveStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoopStatement(SQLParser.LoopStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoopStatement(SQLParser.LoopStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#repeatStatement}.
	 * @param ctx the parse tree
	 */
	void enterRepeatStatement(SQLParser.RepeatStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#repeatStatement}.
	 * @param ctx the parse tree
	 */
	void exitRepeatStatement(SQLParser.RepeatStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(SQLParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(SQLParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(SQLParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(SQLParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CloseCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void enterCloseCursor(SQLParser.CloseCursorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CloseCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void exitCloseCursor(SQLParser.CloseCursorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FetchCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void enterFetchCursor(SQLParser.FetchCursorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FetchCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void exitFetchCursor(SQLParser.FetchCursorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OpenCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void enterOpenCursor(SQLParser.OpenCursorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OpenCursor}
	 * labeled alternative in {@link SQLParser#cursorStatement}.
	 * @param ctx the parse tree
	 */
	void exitOpenCursor(SQLParser.OpenCursorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declareVariable}.
	 * @param ctx the parse tree
	 */
	void enterDeclareVariable(SQLParser.DeclareVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declareVariable}.
	 * @param ctx the parse tree
	 */
	void exitDeclareVariable(SQLParser.DeclareVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declareCondition}.
	 * @param ctx the parse tree
	 */
	void enterDeclareCondition(SQLParser.DeclareConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declareCondition}.
	 * @param ctx the parse tree
	 */
	void exitDeclareCondition(SQLParser.DeclareConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declareCursor}.
	 * @param ctx the parse tree
	 */
	void enterDeclareCursor(SQLParser.DeclareCursorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declareCursor}.
	 * @param ctx the parse tree
	 */
	void exitDeclareCursor(SQLParser.DeclareCursorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declareHandler}.
	 * @param ctx the parse tree
	 */
	void enterDeclareHandler(SQLParser.DeclareHandlerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declareHandler}.
	 * @param ctx the parse tree
	 */
	void exitDeclareHandler(SQLParser.DeclareHandlerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionCode}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionCode(SQLParser.HandlerConditionCodeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionCode}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionCode(SQLParser.HandlerConditionCodeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionState}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionState(SQLParser.HandlerConditionStateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionState}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionState(SQLParser.HandlerConditionStateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionName}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionName(SQLParser.HandlerConditionNameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionName}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionName(SQLParser.HandlerConditionNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionWarning}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionWarning(SQLParser.HandlerConditionWarningContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionWarning}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionWarning(SQLParser.HandlerConditionWarningContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionNotfound}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionNotfound(SQLParser.HandlerConditionNotfoundContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionNotfound}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionNotfound(SQLParser.HandlerConditionNotfoundContext ctx);
	/**
	 * Enter a parse tree produced by the {@code handlerConditionException}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void enterHandlerConditionException(SQLParser.HandlerConditionExceptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code handlerConditionException}
	 * labeled alternative in {@link SQLParser#handlerConditionValue}.
	 * @param ctx the parse tree
	 */
	void exitHandlerConditionException(SQLParser.HandlerConditionExceptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#procedureSqlStatement}.
	 * @param ctx the parse tree
	 */
	void enterProcedureSqlStatement(SQLParser.ProcedureSqlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#procedureSqlStatement}.
	 * @param ctx the parse tree
	 */
	void exitProcedureSqlStatement(SQLParser.ProcedureSqlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#caseAlternative}.
	 * @param ctx the parse tree
	 */
	void enterCaseAlternative(SQLParser.CaseAlternativeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#caseAlternative}.
	 * @param ctx the parse tree
	 */
	void exitCaseAlternative(SQLParser.CaseAlternativeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#elifAlternative}.
	 * @param ctx the parse tree
	 */
	void enterElifAlternative(SQLParser.ElifAlternativeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#elifAlternative}.
	 * @param ctx the parse tree
	 */
	void exitElifAlternative(SQLParser.ElifAlternativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code alterUserMysqlV56}
	 * labeled alternative in {@link SQLParser#alterUser}.
	 * @param ctx the parse tree
	 */
	void enterAlterUserMysqlV56(SQLParser.AlterUserMysqlV56Context ctx);
	/**
	 * Exit a parse tree produced by the {@code alterUserMysqlV56}
	 * labeled alternative in {@link SQLParser#alterUser}.
	 * @param ctx the parse tree
	 */
	void exitAlterUserMysqlV56(SQLParser.AlterUserMysqlV56Context ctx);
	/**
	 * Enter a parse tree produced by the {@code alterUserMysqlV57}
	 * labeled alternative in {@link SQLParser#alterUser}.
	 * @param ctx the parse tree
	 */
	void enterAlterUserMysqlV57(SQLParser.AlterUserMysqlV57Context ctx);
	/**
	 * Exit a parse tree produced by the {@code alterUserMysqlV57}
	 * labeled alternative in {@link SQLParser#alterUser}.
	 * @param ctx the parse tree
	 */
	void exitAlterUserMysqlV57(SQLParser.AlterUserMysqlV57Context ctx);
	/**
	 * Enter a parse tree produced by the {@code createUserMysqlV56}
	 * labeled alternative in {@link SQLParser#createUser}.
	 * @param ctx the parse tree
	 */
	void enterCreateUserMysqlV56(SQLParser.CreateUserMysqlV56Context ctx);
	/**
	 * Exit a parse tree produced by the {@code createUserMysqlV56}
	 * labeled alternative in {@link SQLParser#createUser}.
	 * @param ctx the parse tree
	 */
	void exitCreateUserMysqlV56(SQLParser.CreateUserMysqlV56Context ctx);
	/**
	 * Enter a parse tree produced by the {@code createUserMysqlV57}
	 * labeled alternative in {@link SQLParser#createUser}.
	 * @param ctx the parse tree
	 */
	void enterCreateUserMysqlV57(SQLParser.CreateUserMysqlV57Context ctx);
	/**
	 * Exit a parse tree produced by the {@code createUserMysqlV57}
	 * labeled alternative in {@link SQLParser#createUser}.
	 * @param ctx the parse tree
	 */
	void exitCreateUserMysqlV57(SQLParser.CreateUserMysqlV57Context ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dropUser}.
	 * @param ctx the parse tree
	 */
	void enterDropUser(SQLParser.DropUserContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dropUser}.
	 * @param ctx the parse tree
	 */
	void exitDropUser(SQLParser.DropUserContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#grantStatement}.
	 * @param ctx the parse tree
	 */
	void enterGrantStatement(SQLParser.GrantStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#grantStatement}.
	 * @param ctx the parse tree
	 */
	void exitGrantStatement(SQLParser.GrantStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#grantProxy}.
	 * @param ctx the parse tree
	 */
	void enterGrantProxy(SQLParser.GrantProxyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#grantProxy}.
	 * @param ctx the parse tree
	 */
	void exitGrantProxy(SQLParser.GrantProxyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#renameUser}.
	 * @param ctx the parse tree
	 */
	void enterRenameUser(SQLParser.RenameUserContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#renameUser}.
	 * @param ctx the parse tree
	 */
	void exitRenameUser(SQLParser.RenameUserContext ctx);
	/**
	 * Enter a parse tree produced by the {@code detailRevoke}
	 * labeled alternative in {@link SQLParser#revokeStatement}.
	 * @param ctx the parse tree
	 */
	void enterDetailRevoke(SQLParser.DetailRevokeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code detailRevoke}
	 * labeled alternative in {@link SQLParser#revokeStatement}.
	 * @param ctx the parse tree
	 */
	void exitDetailRevoke(SQLParser.DetailRevokeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code shortRevoke}
	 * labeled alternative in {@link SQLParser#revokeStatement}.
	 * @param ctx the parse tree
	 */
	void enterShortRevoke(SQLParser.ShortRevokeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code shortRevoke}
	 * labeled alternative in {@link SQLParser#revokeStatement}.
	 * @param ctx the parse tree
	 */
	void exitShortRevoke(SQLParser.ShortRevokeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#revokeProxy}.
	 * @param ctx the parse tree
	 */
	void enterRevokeProxy(SQLParser.RevokeProxyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#revokeProxy}.
	 * @param ctx the parse tree
	 */
	void exitRevokeProxy(SQLParser.RevokeProxyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#setPasswordStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetPasswordStatement(SQLParser.SetPasswordStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#setPasswordStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetPasswordStatement(SQLParser.SetPasswordStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userSpecification}.
	 * @param ctx the parse tree
	 */
	void enterUserSpecification(SQLParser.UserSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userSpecification}.
	 * @param ctx the parse tree
	 */
	void exitUserSpecification(SQLParser.UserSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code passwordAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void enterPasswordAuthOption(SQLParser.PasswordAuthOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code passwordAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void exitPasswordAuthOption(SQLParser.PasswordAuthOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void enterStringAuthOption(SQLParser.StringAuthOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void exitStringAuthOption(SQLParser.StringAuthOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hashAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void enterHashAuthOption(SQLParser.HashAuthOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hashAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void exitHashAuthOption(SQLParser.HashAuthOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void enterSimpleAuthOption(SQLParser.SimpleAuthOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleAuthOption}
	 * labeled alternative in {@link SQLParser#userAuthOption}.
	 * @param ctx the parse tree
	 */
	void exitSimpleAuthOption(SQLParser.SimpleAuthOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tlsOption}.
	 * @param ctx the parse tree
	 */
	void enterTlsOption(SQLParser.TlsOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tlsOption}.
	 * @param ctx the parse tree
	 */
	void exitTlsOption(SQLParser.TlsOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userResourceOption}.
	 * @param ctx the parse tree
	 */
	void enterUserResourceOption(SQLParser.UserResourceOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userResourceOption}.
	 * @param ctx the parse tree
	 */
	void exitUserResourceOption(SQLParser.UserResourceOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userPasswordOption}.
	 * @param ctx the parse tree
	 */
	void enterUserPasswordOption(SQLParser.UserPasswordOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userPasswordOption}.
	 * @param ctx the parse tree
	 */
	void exitUserPasswordOption(SQLParser.UserPasswordOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userLockOption}.
	 * @param ctx the parse tree
	 */
	void enterUserLockOption(SQLParser.UserLockOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userLockOption}.
	 * @param ctx the parse tree
	 */
	void exitUserLockOption(SQLParser.UserLockOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#privelegeClause}.
	 * @param ctx the parse tree
	 */
	void enterPrivelegeClause(SQLParser.PrivelegeClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#privelegeClause}.
	 * @param ctx the parse tree
	 */
	void exitPrivelegeClause(SQLParser.PrivelegeClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#privilege}.
	 * @param ctx the parse tree
	 */
	void enterPrivilege(SQLParser.PrivilegeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#privilege}.
	 * @param ctx the parse tree
	 */
	void exitPrivilege(SQLParser.PrivilegeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code currentSchemaPriviLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void enterCurrentSchemaPriviLevel(SQLParser.CurrentSchemaPriviLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code currentSchemaPriviLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void exitCurrentSchemaPriviLevel(SQLParser.CurrentSchemaPriviLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code globalPrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void enterGlobalPrivLevel(SQLParser.GlobalPrivLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code globalPrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void exitGlobalPrivLevel(SQLParser.GlobalPrivLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code definiteSchemaPrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void enterDefiniteSchemaPrivLevel(SQLParser.DefiniteSchemaPrivLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code definiteSchemaPrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void exitDefiniteSchemaPrivLevel(SQLParser.DefiniteSchemaPrivLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code definiteFullTablePrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void enterDefiniteFullTablePrivLevel(SQLParser.DefiniteFullTablePrivLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code definiteFullTablePrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void exitDefiniteFullTablePrivLevel(SQLParser.DefiniteFullTablePrivLevelContext ctx);
	/**
	 * Enter a parse tree produced by the {@code definiteTablePrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void enterDefiniteTablePrivLevel(SQLParser.DefiniteTablePrivLevelContext ctx);
	/**
	 * Exit a parse tree produced by the {@code definiteTablePrivLevel}
	 * labeled alternative in {@link SQLParser#privilegeLevel}.
	 * @param ctx the parse tree
	 */
	void exitDefiniteTablePrivLevel(SQLParser.DefiniteTablePrivLevelContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#renameUserClause}.
	 * @param ctx the parse tree
	 */
	void enterRenameUserClause(SQLParser.RenameUserClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#renameUserClause}.
	 * @param ctx the parse tree
	 */
	void exitRenameUserClause(SQLParser.RenameUserClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#analyzeTable}.
	 * @param ctx the parse tree
	 */
	void enterAnalyzeTable(SQLParser.AnalyzeTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#analyzeTable}.
	 * @param ctx the parse tree
	 */
	void exitAnalyzeTable(SQLParser.AnalyzeTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#checkTable}.
	 * @param ctx the parse tree
	 */
	void enterCheckTable(SQLParser.CheckTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#checkTable}.
	 * @param ctx the parse tree
	 */
	void exitCheckTable(SQLParser.CheckTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#checksumTable}.
	 * @param ctx the parse tree
	 */
	void enterChecksumTable(SQLParser.ChecksumTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#checksumTable}.
	 * @param ctx the parse tree
	 */
	void exitChecksumTable(SQLParser.ChecksumTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#optimizeTable}.
	 * @param ctx the parse tree
	 */
	void enterOptimizeTable(SQLParser.OptimizeTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#optimizeTable}.
	 * @param ctx the parse tree
	 */
	void exitOptimizeTable(SQLParser.OptimizeTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#repairTable}.
	 * @param ctx the parse tree
	 */
	void enterRepairTable(SQLParser.RepairTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#repairTable}.
	 * @param ctx the parse tree
	 */
	void exitRepairTable(SQLParser.RepairTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#checkTableOption}.
	 * @param ctx the parse tree
	 */
	void enterCheckTableOption(SQLParser.CheckTableOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#checkTableOption}.
	 * @param ctx the parse tree
	 */
	void exitCheckTableOption(SQLParser.CheckTableOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createUdfunction}.
	 * @param ctx the parse tree
	 */
	void enterCreateUdfunction(SQLParser.CreateUdfunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createUdfunction}.
	 * @param ctx the parse tree
	 */
	void exitCreateUdfunction(SQLParser.CreateUdfunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#installPlugin}.
	 * @param ctx the parse tree
	 */
	void enterInstallPlugin(SQLParser.InstallPluginContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#installPlugin}.
	 * @param ctx the parse tree
	 */
	void exitInstallPlugin(SQLParser.InstallPluginContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#uninstallPlugin}.
	 * @param ctx the parse tree
	 */
	void enterUninstallPlugin(SQLParser.UninstallPluginContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#uninstallPlugin}.
	 * @param ctx the parse tree
	 */
	void exitUninstallPlugin(SQLParser.UninstallPluginContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setVariable}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetVariable(SQLParser.SetVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setVariable}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetVariable(SQLParser.SetVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setCharset}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetCharset(SQLParser.SetCharsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setCharset}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetCharset(SQLParser.SetCharsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setNames}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetNames(SQLParser.SetNamesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setNames}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetNames(SQLParser.SetNamesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setPassword}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetPassword(SQLParser.SetPasswordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setPassword}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetPassword(SQLParser.SetPasswordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setTransaction}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetTransaction(SQLParser.SetTransactionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setTransaction}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetTransaction(SQLParser.SetTransactionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setAutocommit}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetAutocommit(SQLParser.SetAutocommitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setAutocommit}
	 * labeled alternative in {@link SQLParser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetAutocommit(SQLParser.SetAutocommitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showMasterLogs}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowMasterLogs(SQLParser.ShowMasterLogsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showMasterLogs}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowMasterLogs(SQLParser.ShowMasterLogsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showLogEvents}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowLogEvents(SQLParser.ShowLogEventsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showLogEvents}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowLogEvents(SQLParser.ShowLogEventsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showObjectFilter}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowObjectFilter(SQLParser.ShowObjectFilterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showObjectFilter}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowObjectFilter(SQLParser.ShowObjectFilterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showColumns}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowColumns(SQLParser.ShowColumnsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showColumns}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowColumns(SQLParser.ShowColumnsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showCreateDb}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowCreateDb(SQLParser.ShowCreateDbContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showCreateDb}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowCreateDb(SQLParser.ShowCreateDbContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showCreateFullIdObject}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowCreateFullIdObject(SQLParser.ShowCreateFullIdObjectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showCreateFullIdObject}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowCreateFullIdObject(SQLParser.ShowCreateFullIdObjectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showCreateUser}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowCreateUser(SQLParser.ShowCreateUserContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showCreateUser}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowCreateUser(SQLParser.ShowCreateUserContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showEngine}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowEngine(SQLParser.ShowEngineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showEngine}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowEngine(SQLParser.ShowEngineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showGlobalInfo}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowGlobalInfo(SQLParser.ShowGlobalInfoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showGlobalInfo}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowGlobalInfo(SQLParser.ShowGlobalInfoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showErrors}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowErrors(SQLParser.ShowErrorsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showErrors}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowErrors(SQLParser.ShowErrorsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showCountErrors}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowCountErrors(SQLParser.ShowCountErrorsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showCountErrors}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowCountErrors(SQLParser.ShowCountErrorsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showSchemaFilter}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowSchemaFilter(SQLParser.ShowSchemaFilterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showSchemaFilter}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowSchemaFilter(SQLParser.ShowSchemaFilterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showRoutine}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowRoutine(SQLParser.ShowRoutineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showRoutine}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowRoutine(SQLParser.ShowRoutineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showGrants}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowGrants(SQLParser.ShowGrantsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showGrants}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowGrants(SQLParser.ShowGrantsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showIndexes}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowIndexes(SQLParser.ShowIndexesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showIndexes}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowIndexes(SQLParser.ShowIndexesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showOpenTables}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowOpenTables(SQLParser.ShowOpenTablesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showOpenTables}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowOpenTables(SQLParser.ShowOpenTablesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showProfile}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowProfile(SQLParser.ShowProfileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showProfile}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowProfile(SQLParser.ShowProfileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code showSlaveStatus}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void enterShowSlaveStatus(SQLParser.ShowSlaveStatusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code showSlaveStatus}
	 * labeled alternative in {@link SQLParser#showStatement}.
	 * @param ctx the parse tree
	 */
	void exitShowSlaveStatus(SQLParser.ShowSlaveStatusContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#variableClause}.
	 * @param ctx the parse tree
	 */
	void enterVariableClause(SQLParser.VariableClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#variableClause}.
	 * @param ctx the parse tree
	 */
	void exitVariableClause(SQLParser.VariableClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showCommonEntity}.
	 * @param ctx the parse tree
	 */
	void enterShowCommonEntity(SQLParser.ShowCommonEntityContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showCommonEntity}.
	 * @param ctx the parse tree
	 */
	void exitShowCommonEntity(SQLParser.ShowCommonEntityContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showFilter}.
	 * @param ctx the parse tree
	 */
	void enterShowFilter(SQLParser.ShowFilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showFilter}.
	 * @param ctx the parse tree
	 */
	void exitShowFilter(SQLParser.ShowFilterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showGlobalInfoClause}.
	 * @param ctx the parse tree
	 */
	void enterShowGlobalInfoClause(SQLParser.ShowGlobalInfoClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showGlobalInfoClause}.
	 * @param ctx the parse tree
	 */
	void exitShowGlobalInfoClause(SQLParser.ShowGlobalInfoClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showSchemaEntity}.
	 * @param ctx the parse tree
	 */
	void enterShowSchemaEntity(SQLParser.ShowSchemaEntityContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showSchemaEntity}.
	 * @param ctx the parse tree
	 */
	void exitShowSchemaEntity(SQLParser.ShowSchemaEntityContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showProfileType}.
	 * @param ctx the parse tree
	 */
	void enterShowProfileType(SQLParser.ShowProfileTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showProfileType}.
	 * @param ctx the parse tree
	 */
	void exitShowProfileType(SQLParser.ShowProfileTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#binlogStatement}.
	 * @param ctx the parse tree
	 */
	void enterBinlogStatement(SQLParser.BinlogStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#binlogStatement}.
	 * @param ctx the parse tree
	 */
	void exitBinlogStatement(SQLParser.BinlogStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#cacheIndexStatement}.
	 * @param ctx the parse tree
	 */
	void enterCacheIndexStatement(SQLParser.CacheIndexStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#cacheIndexStatement}.
	 * @param ctx the parse tree
	 */
	void exitCacheIndexStatement(SQLParser.CacheIndexStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#flushStatement}.
	 * @param ctx the parse tree
	 */
	void enterFlushStatement(SQLParser.FlushStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#flushStatement}.
	 * @param ctx the parse tree
	 */
	void exitFlushStatement(SQLParser.FlushStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#killStatement}.
	 * @param ctx the parse tree
	 */
	void enterKillStatement(SQLParser.KillStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#killStatement}.
	 * @param ctx the parse tree
	 */
	void exitKillStatement(SQLParser.KillStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#loadIndexIntoCache}.
	 * @param ctx the parse tree
	 */
	void enterLoadIndexIntoCache(SQLParser.LoadIndexIntoCacheContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#loadIndexIntoCache}.
	 * @param ctx the parse tree
	 */
	void exitLoadIndexIntoCache(SQLParser.LoadIndexIntoCacheContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#resetStatement}.
	 * @param ctx the parse tree
	 */
	void enterResetStatement(SQLParser.ResetStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#resetStatement}.
	 * @param ctx the parse tree
	 */
	void exitResetStatement(SQLParser.ResetStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#shutdownStatement}.
	 * @param ctx the parse tree
	 */
	void enterShutdownStatement(SQLParser.ShutdownStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#shutdownStatement}.
	 * @param ctx the parse tree
	 */
	void exitShutdownStatement(SQLParser.ShutdownStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tableIndexes}.
	 * @param ctx the parse tree
	 */
	void enterTableIndexes(SQLParser.TableIndexesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tableIndexes}.
	 * @param ctx the parse tree
	 */
	void exitTableIndexes(SQLParser.TableIndexesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void enterSimpleFlushOption(SQLParser.SimpleFlushOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void exitSimpleFlushOption(SQLParser.SimpleFlushOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code channelFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void enterChannelFlushOption(SQLParser.ChannelFlushOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code channelFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void exitChannelFlushOption(SQLParser.ChannelFlushOptionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tableFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void enterTableFlushOption(SQLParser.TableFlushOptionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tableFlushOption}
	 * labeled alternative in {@link SQLParser#flushOption}.
	 * @param ctx the parse tree
	 */
	void exitTableFlushOption(SQLParser.TableFlushOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#flushTableOption}.
	 * @param ctx the parse tree
	 */
	void enterFlushTableOption(SQLParser.FlushTableOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#flushTableOption}.
	 * @param ctx the parse tree
	 */
	void exitFlushTableOption(SQLParser.FlushTableOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#loadedTableIndexes}.
	 * @param ctx the parse tree
	 */
	void enterLoadedTableIndexes(SQLParser.LoadedTableIndexesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#loadedTableIndexes}.
	 * @param ctx the parse tree
	 */
	void exitLoadedTableIndexes(SQLParser.LoadedTableIndexesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#simpleDescribeStatement}.
	 * @param ctx the parse tree
	 */
	void enterSimpleDescribeStatement(SQLParser.SimpleDescribeStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#simpleDescribeStatement}.
	 * @param ctx the parse tree
	 */
	void exitSimpleDescribeStatement(SQLParser.SimpleDescribeStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fullDescribeStatement}.
	 * @param ctx the parse tree
	 */
	void enterFullDescribeStatement(SQLParser.FullDescribeStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fullDescribeStatement}.
	 * @param ctx the parse tree
	 */
	void exitFullDescribeStatement(SQLParser.FullDescribeStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#helpStatement}.
	 * @param ctx the parse tree
	 */
	void enterHelpStatement(SQLParser.HelpStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#helpStatement}.
	 * @param ctx the parse tree
	 */
	void exitHelpStatement(SQLParser.HelpStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#useStatement}.
	 * @param ctx the parse tree
	 */
	void enterUseStatement(SQLParser.UseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#useStatement}.
	 * @param ctx the parse tree
	 */
	void exitUseStatement(SQLParser.UseStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code describeStatements}
	 * labeled alternative in {@link SQLParser#describeObjectClause}.
	 * @param ctx the parse tree
	 */
	void enterDescribeStatements(SQLParser.DescribeStatementsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code describeStatements}
	 * labeled alternative in {@link SQLParser#describeObjectClause}.
	 * @param ctx the parse tree
	 */
	void exitDescribeStatements(SQLParser.DescribeStatementsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code describeConnection}
	 * labeled alternative in {@link SQLParser#describeObjectClause}.
	 * @param ctx the parse tree
	 */
	void enterDescribeConnection(SQLParser.DescribeConnectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code describeConnection}
	 * labeled alternative in {@link SQLParser#describeObjectClause}.
	 * @param ctx the parse tree
	 */
	void exitDescribeConnection(SQLParser.DescribeConnectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fullId}.
	 * @param ctx the parse tree
	 */
	void enterFullId(SQLParser.FullIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fullId}.
	 * @param ctx the parse tree
	 */
	void exitFullId(SQLParser.FullIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tableName}.
	 * @param ctx the parse tree
	 */
	void enterTableName(SQLParser.TableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tableName}.
	 * @param ctx the parse tree
	 */
	void exitTableName(SQLParser.TableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fullColumnName}.
	 * @param ctx the parse tree
	 */
	void enterFullColumnName(SQLParser.FullColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fullColumnName}.
	 * @param ctx the parse tree
	 */
	void exitFullColumnName(SQLParser.FullColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexColumnName}.
	 * @param ctx the parse tree
	 */
	void enterIndexColumnName(SQLParser.IndexColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexColumnName}.
	 * @param ctx the parse tree
	 */
	void exitIndexColumnName(SQLParser.IndexColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userName}.
	 * @param ctx the parse tree
	 */
	void enterUserName(SQLParser.UserNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userName}.
	 * @param ctx the parse tree
	 */
	void exitUserName(SQLParser.UserNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#mysqlVariable}.
	 * @param ctx the parse tree
	 */
	void enterMysqlVariable(SQLParser.MysqlVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#mysqlVariable}.
	 * @param ctx the parse tree
	 */
	void exitMysqlVariable(SQLParser.MysqlVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#charsetName}.
	 * @param ctx the parse tree
	 */
	void enterCharsetName(SQLParser.CharsetNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#charsetName}.
	 * @param ctx the parse tree
	 */
	void exitCharsetName(SQLParser.CharsetNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#collationName}.
	 * @param ctx the parse tree
	 */
	void enterCollationName(SQLParser.CollationNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#collationName}.
	 * @param ctx the parse tree
	 */
	void exitCollationName(SQLParser.CollationNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#engineName}.
	 * @param ctx the parse tree
	 */
	void enterEngineName(SQLParser.EngineNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#engineName}.
	 * @param ctx the parse tree
	 */
	void exitEngineName(SQLParser.EngineNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#uuidSet}.
	 * @param ctx the parse tree
	 */
	void enterUuidSet(SQLParser.UuidSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#uuidSet}.
	 * @param ctx the parse tree
	 */
	void exitUuidSet(SQLParser.UuidSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xid}.
	 * @param ctx the parse tree
	 */
	void enterXid(SQLParser.XidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xid}.
	 * @param ctx the parse tree
	 */
	void exitXid(SQLParser.XidContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#xuidStringId}.
	 * @param ctx the parse tree
	 */
	void enterXuidStringId(SQLParser.XuidStringIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#xuidStringId}.
	 * @param ctx the parse tree
	 */
	void exitXuidStringId(SQLParser.XuidStringIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#authPlugin}.
	 * @param ctx the parse tree
	 */
	void enterAuthPlugin(SQLParser.AuthPluginContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#authPlugin}.
	 * @param ctx the parse tree
	 */
	void exitAuthPlugin(SQLParser.AuthPluginContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#uid}.
	 * @param ctx the parse tree
	 */
	void enterUid(SQLParser.UidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#uid}.
	 * @param ctx the parse tree
	 */
	void exitUid(SQLParser.UidContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#simpleId}.
	 * @param ctx the parse tree
	 */
	void enterSimpleId(SQLParser.SimpleIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#simpleId}.
	 * @param ctx the parse tree
	 */
	void exitSimpleId(SQLParser.SimpleIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dottedId}.
	 * @param ctx the parse tree
	 */
	void enterDottedId(SQLParser.DottedIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dottedId}.
	 * @param ctx the parse tree
	 */
	void exitDottedId(SQLParser.DottedIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#decimalLiteral}.
	 * @param ctx the parse tree
	 */
	void enterDecimalLiteral(SQLParser.DecimalLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#decimalLiteral}.
	 * @param ctx the parse tree
	 */
	void exitDecimalLiteral(SQLParser.DecimalLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fileSizeLiteral}.
	 * @param ctx the parse tree
	 */
	void enterFileSizeLiteral(SQLParser.FileSizeLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fileSizeLiteral}.
	 * @param ctx the parse tree
	 */
	void exitFileSizeLiteral(SQLParser.FileSizeLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(SQLParser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(SQLParser.StringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(SQLParser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(SQLParser.BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#hexadecimalLiteral}.
	 * @param ctx the parse tree
	 */
	void enterHexadecimalLiteral(SQLParser.HexadecimalLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#hexadecimalLiteral}.
	 * @param ctx the parse tree
	 */
	void exitHexadecimalLiteral(SQLParser.HexadecimalLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#nullNotnull}.
	 * @param ctx the parse tree
	 */
	void enterNullNotnull(SQLParser.NullNotnullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#nullNotnull}.
	 * @param ctx the parse tree
	 */
	void exitNullNotnull(SQLParser.NullNotnullContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(SQLParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(SQLParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterStringDataType(SQLParser.StringDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitStringDataType(SQLParser.StringDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dimensionDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterDimensionDataType(SQLParser.DimensionDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dimensionDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitDimensionDataType(SQLParser.DimensionDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterSimpleDataType(SQLParser.SimpleDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitSimpleDataType(SQLParser.SimpleDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code collectionDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterCollectionDataType(SQLParser.CollectionDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code collectionDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitCollectionDataType(SQLParser.CollectionDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code spatialDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterSpatialDataType(SQLParser.SpatialDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code spatialDataType}
	 * labeled alternative in {@link SQLParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitSpatialDataType(SQLParser.SpatialDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#convertedDataType}.
	 * @param ctx the parse tree
	 */
	void enterConvertedDataType(SQLParser.ConvertedDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#convertedDataType}.
	 * @param ctx the parse tree
	 */
	void exitConvertedDataType(SQLParser.ConvertedDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lengthOneDimension}.
	 * @param ctx the parse tree
	 */
	void enterLengthOneDimension(SQLParser.LengthOneDimensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lengthOneDimension}.
	 * @param ctx the parse tree
	 */
	void exitLengthOneDimension(SQLParser.LengthOneDimensionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lengthTwoDimension}.
	 * @param ctx the parse tree
	 */
	void enterLengthTwoDimension(SQLParser.LengthTwoDimensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lengthTwoDimension}.
	 * @param ctx the parse tree
	 */
	void exitLengthTwoDimension(SQLParser.LengthTwoDimensionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#lengthTwoOptionalDimension}.
	 * @param ctx the parse tree
	 */
	void enterLengthTwoOptionalDimension(SQLParser.LengthTwoOptionalDimensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#lengthTwoOptionalDimension}.
	 * @param ctx the parse tree
	 */
	void exitLengthTwoOptionalDimension(SQLParser.LengthTwoOptionalDimensionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#uidList}.
	 * @param ctx the parse tree
	 */
	void enterUidList(SQLParser.UidListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#uidList}.
	 * @param ctx the parse tree
	 */
	void exitUidList(SQLParser.UidListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#tables}.
	 * @param ctx the parse tree
	 */
	void enterTables(SQLParser.TablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#tables}.
	 * @param ctx the parse tree
	 */
	void exitTables(SQLParser.TablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexColumnNames}.
	 * @param ctx the parse tree
	 */
	void enterIndexColumnNames(SQLParser.IndexColumnNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexColumnNames}.
	 * @param ctx the parse tree
	 */
	void exitIndexColumnNames(SQLParser.IndexColumnNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expressions}.
	 * @param ctx the parse tree
	 */
	void enterExpressions(SQLParser.ExpressionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expressions}.
	 * @param ctx the parse tree
	 */
	void exitExpressions(SQLParser.ExpressionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expressionsWithDefaults}.
	 * @param ctx the parse tree
	 */
	void enterExpressionsWithDefaults(SQLParser.ExpressionsWithDefaultsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expressionsWithDefaults}.
	 * @param ctx the parse tree
	 */
	void exitExpressionsWithDefaults(SQLParser.ExpressionsWithDefaultsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#constants}.
	 * @param ctx the parse tree
	 */
	void enterConstants(SQLParser.ConstantsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#constants}.
	 * @param ctx the parse tree
	 */
	void exitConstants(SQLParser.ConstantsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#simpleStrings}.
	 * @param ctx the parse tree
	 */
	void enterSimpleStrings(SQLParser.SimpleStringsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#simpleStrings}.
	 * @param ctx the parse tree
	 */
	void exitSimpleStrings(SQLParser.SimpleStringsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#userVariables}.
	 * @param ctx the parse tree
	 */
	void enterUserVariables(SQLParser.UserVariablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#userVariables}.
	 * @param ctx the parse tree
	 */
	void exitUserVariables(SQLParser.UserVariablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#defaultValue}.
	 * @param ctx the parse tree
	 */
	void enterDefaultValue(SQLParser.DefaultValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#defaultValue}.
	 * @param ctx the parse tree
	 */
	void exitDefaultValue(SQLParser.DefaultValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#currentTimestamp}.
	 * @param ctx the parse tree
	 */
	void enterCurrentTimestamp(SQLParser.CurrentTimestampContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#currentTimestamp}.
	 * @param ctx the parse tree
	 */
	void exitCurrentTimestamp(SQLParser.CurrentTimestampContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expressionOrDefault}.
	 * @param ctx the parse tree
	 */
	void enterExpressionOrDefault(SQLParser.ExpressionOrDefaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expressionOrDefault}.
	 * @param ctx the parse tree
	 */
	void exitExpressionOrDefault(SQLParser.ExpressionOrDefaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ifExists}.
	 * @param ctx the parse tree
	 */
	void enterIfExists(SQLParser.IfExistsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ifExists}.
	 * @param ctx the parse tree
	 */
	void exitIfExists(SQLParser.IfExistsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ifNotExists}.
	 * @param ctx the parse tree
	 */
	void enterIfNotExists(SQLParser.IfNotExistsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ifNotExists}.
	 * @param ctx the parse tree
	 */
	void exitIfNotExists(SQLParser.IfNotExistsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specificFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterSpecificFunctionCall(SQLParser.SpecificFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specificFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitSpecificFunctionCall(SQLParser.SpecificFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code aggregateFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterAggregateFunctionCall(SQLParser.AggregateFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code aggregateFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitAggregateFunctionCall(SQLParser.AggregateFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code scalarFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterScalarFunctionCall(SQLParser.ScalarFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code scalarFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitScalarFunctionCall(SQLParser.ScalarFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code udfFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterUdfFunctionCall(SQLParser.UdfFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code udfFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitUdfFunctionCall(SQLParser.UdfFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code passwordFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPasswordFunctionCall(SQLParser.PasswordFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code passwordFunctionCall}
	 * labeled alternative in {@link SQLParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPasswordFunctionCall(SQLParser.PasswordFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterSimpleFunctionCall(SQLParser.SimpleFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitSimpleFunctionCall(SQLParser.SimpleFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dataTypeFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeFunctionCall(SQLParser.DataTypeFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dataTypeFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeFunctionCall(SQLParser.DataTypeFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuesFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterValuesFunctionCall(SQLParser.ValuesFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuesFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitValuesFunctionCall(SQLParser.ValuesFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code caseFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterCaseFunctionCall(SQLParser.CaseFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code caseFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitCaseFunctionCall(SQLParser.CaseFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterCharFunctionCall(SQLParser.CharFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitCharFunctionCall(SQLParser.CharFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code positionFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterPositionFunctionCall(SQLParser.PositionFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code positionFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitPositionFunctionCall(SQLParser.PositionFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code substrFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterSubstrFunctionCall(SQLParser.SubstrFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code substrFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitSubstrFunctionCall(SQLParser.SubstrFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code trimFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterTrimFunctionCall(SQLParser.TrimFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code trimFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitTrimFunctionCall(SQLParser.TrimFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code weightFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterWeightFunctionCall(SQLParser.WeightFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code weightFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitWeightFunctionCall(SQLParser.WeightFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code extractFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterExtractFunctionCall(SQLParser.ExtractFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code extractFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitExtractFunctionCall(SQLParser.ExtractFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getFormatFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void enterGetFormatFunctionCall(SQLParser.GetFormatFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getFormatFunctionCall}
	 * labeled alternative in {@link SQLParser#specificFunction}.
	 * @param ctx the parse tree
	 */
	void exitGetFormatFunctionCall(SQLParser.GetFormatFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#caseFuncAlternative}.
	 * @param ctx the parse tree
	 */
	void enterCaseFuncAlternative(SQLParser.CaseFuncAlternativeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#caseFuncAlternative}.
	 * @param ctx the parse tree
	 */
	void exitCaseFuncAlternative(SQLParser.CaseFuncAlternativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code levelWeightList}
	 * labeled alternative in {@link SQLParser#levelsInWeightString}.
	 * @param ctx the parse tree
	 */
	void enterLevelWeightList(SQLParser.LevelWeightListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code levelWeightList}
	 * labeled alternative in {@link SQLParser#levelsInWeightString}.
	 * @param ctx the parse tree
	 */
	void exitLevelWeightList(SQLParser.LevelWeightListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code levelWeightRange}
	 * labeled alternative in {@link SQLParser#levelsInWeightString}.
	 * @param ctx the parse tree
	 */
	void enterLevelWeightRange(SQLParser.LevelWeightRangeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code levelWeightRange}
	 * labeled alternative in {@link SQLParser#levelsInWeightString}.
	 * @param ctx the parse tree
	 */
	void exitLevelWeightRange(SQLParser.LevelWeightRangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#levelInWeightListElement}.
	 * @param ctx the parse tree
	 */
	void enterLevelInWeightListElement(SQLParser.LevelInWeightListElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#levelInWeightListElement}.
	 * @param ctx the parse tree
	 */
	void exitLevelInWeightListElement(SQLParser.LevelInWeightListElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#aggregateWindowedFunction}.
	 * @param ctx the parse tree
	 */
	void enterAggregateWindowedFunction(SQLParser.AggregateWindowedFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#aggregateWindowedFunction}.
	 * @param ctx the parse tree
	 */
	void exitAggregateWindowedFunction(SQLParser.AggregateWindowedFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#scalarFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterScalarFunctionName(SQLParser.ScalarFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#scalarFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitScalarFunctionName(SQLParser.ScalarFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#passwordFunctionClause}.
	 * @param ctx the parse tree
	 */
	void enterPasswordFunctionClause(SQLParser.PasswordFunctionClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#passwordFunctionClause}.
	 * @param ctx the parse tree
	 */
	void exitPasswordFunctionClause(SQLParser.PasswordFunctionClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#functionArgs}.
	 * @param ctx the parse tree
	 */
	void enterFunctionArgs(SQLParser.FunctionArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#functionArgs}.
	 * @param ctx the parse tree
	 */
	void exitFunctionArgs(SQLParser.FunctionArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#functionArg}.
	 * @param ctx the parse tree
	 */
	void enterFunctionArg(SQLParser.FunctionArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#functionArg}.
	 * @param ctx the parse tree
	 */
	void exitFunctionArg(SQLParser.FunctionArgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code isExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIsExpression(SQLParser.IsExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code isExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIsExpression(SQLParser.IsExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(SQLParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(SQLParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpression(SQLParser.LogicalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpression(SQLParser.LogicalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predicateExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPredicateExpression(SQLParser.PredicateExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predicateExpression}
	 * labeled alternative in {@link SQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPredicateExpression(SQLParser.PredicateExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code soundsLikePredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterSoundsLikePredicate(SQLParser.SoundsLikePredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code soundsLikePredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitSoundsLikePredicate(SQLParser.SoundsLikePredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionAtomPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterExpressionAtomPredicate(SQLParser.ExpressionAtomPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionAtomPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitExpressionAtomPredicate(SQLParser.ExpressionAtomPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterInPredicate(SQLParser.InPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitInPredicate(SQLParser.InPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subqueryComparasionPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterSubqueryComparasionPredicate(SQLParser.SubqueryComparasionPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subqueryComparasionPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitSubqueryComparasionPredicate(SQLParser.SubqueryComparasionPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code betweenPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterBetweenPredicate(SQLParser.BetweenPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code betweenPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitBetweenPredicate(SQLParser.BetweenPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryComparasionPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterBinaryComparasionPredicate(SQLParser.BinaryComparasionPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryComparasionPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitBinaryComparasionPredicate(SQLParser.BinaryComparasionPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code isNullPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterIsNullPredicate(SQLParser.IsNullPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code isNullPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitIsNullPredicate(SQLParser.IsNullPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code likePredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterLikePredicate(SQLParser.LikePredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code likePredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitLikePredicate(SQLParser.LikePredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code regexpPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterRegexpPredicate(SQLParser.RegexpPredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code regexpPredicate}
	 * labeled alternative in {@link SQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitRegexpPredicate(SQLParser.RegexpPredicateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpressionAtom(SQLParser.UnaryExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpressionAtom(SQLParser.UnaryExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code collateExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterCollateExpressionAtom(SQLParser.CollateExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code collateExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitCollateExpressionAtom(SQLParser.CollateExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subqueryExpessionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterSubqueryExpessionAtom(SQLParser.SubqueryExpessionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subqueryExpessionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitSubqueryExpessionAtom(SQLParser.SubqueryExpessionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mysqlVariableExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterMysqlVariableExpressionAtom(SQLParser.MysqlVariableExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mysqlVariableExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitMysqlVariableExpressionAtom(SQLParser.MysqlVariableExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nestedExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterNestedExpressionAtom(SQLParser.NestedExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nestedExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitNestedExpressionAtom(SQLParser.NestedExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nestedRowExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterNestedRowExpressionAtom(SQLParser.NestedRowExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nestedRowExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitNestedRowExpressionAtom(SQLParser.NestedRowExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mathExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterMathExpressionAtom(SQLParser.MathExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mathExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitMathExpressionAtom(SQLParser.MathExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intervalExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterIntervalExpressionAtom(SQLParser.IntervalExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intervalExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitIntervalExpressionAtom(SQLParser.IntervalExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code existsExpessionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterExistsExpessionAtom(SQLParser.ExistsExpessionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code existsExpessionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitExistsExpessionAtom(SQLParser.ExistsExpessionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constantExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpressionAtom(SQLParser.ConstantExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constantExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpressionAtom(SQLParser.ConstantExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallExpressionAtom(SQLParser.FunctionCallExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallExpressionAtom(SQLParser.FunctionCallExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpressionAtom(SQLParser.BinaryExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpressionAtom(SQLParser.BinaryExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code fullColumnNameExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterFullColumnNameExpressionAtom(SQLParser.FullColumnNameExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code fullColumnNameExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitFullColumnNameExpressionAtom(SQLParser.FullColumnNameExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bitExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void enterBitExpressionAtom(SQLParser.BitExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bitExpressionAtom}
	 * labeled alternative in {@link SQLParser#expressionAtom}.
	 * @param ctx the parse tree
	 */
	void exitBitExpressionAtom(SQLParser.BitExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unaryOperator}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOperator(SQLParser.UnaryOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unaryOperator}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOperator(SQLParser.UnaryOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void enterComparisonOperator(SQLParser.ComparisonOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void exitComparisonOperator(SQLParser.ComparisonOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperator(SQLParser.LogicalOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperator(SQLParser.LogicalOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#bitOperator}.
	 * @param ctx the parse tree
	 */
	void enterBitOperator(SQLParser.BitOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#bitOperator}.
	 * @param ctx the parse tree
	 */
	void exitBitOperator(SQLParser.BitOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#mathOperator}.
	 * @param ctx the parse tree
	 */
	void enterMathOperator(SQLParser.MathOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#mathOperator}.
	 * @param ctx the parse tree
	 */
	void exitMathOperator(SQLParser.MathOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#charsetNameBase}.
	 * @param ctx the parse tree
	 */
	void enterCharsetNameBase(SQLParser.CharsetNameBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#charsetNameBase}.
	 * @param ctx the parse tree
	 */
	void exitCharsetNameBase(SQLParser.CharsetNameBaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transactionLevelBase}.
	 * @param ctx the parse tree
	 */
	void enterTransactionLevelBase(SQLParser.TransactionLevelBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transactionLevelBase}.
	 * @param ctx the parse tree
	 */
	void exitTransactionLevelBase(SQLParser.TransactionLevelBaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#privilegesBase}.
	 * @param ctx the parse tree
	 */
	void enterPrivilegesBase(SQLParser.PrivilegesBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#privilegesBase}.
	 * @param ctx the parse tree
	 */
	void exitPrivilegesBase(SQLParser.PrivilegesBaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#intervalTypeBase}.
	 * @param ctx the parse tree
	 */
	void enterIntervalTypeBase(SQLParser.IntervalTypeBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#intervalTypeBase}.
	 * @param ctx the parse tree
	 */
	void exitIntervalTypeBase(SQLParser.IntervalTypeBaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#dataTypeBase}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeBase(SQLParser.DataTypeBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#dataTypeBase}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeBase(SQLParser.DataTypeBaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#keywordsCanBeId}.
	 * @param ctx the parse tree
	 */
	void enterKeywordsCanBeId(SQLParser.KeywordsCanBeIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#keywordsCanBeId}.
	 * @param ctx the parse tree
	 */
	void exitKeywordsCanBeId(SQLParser.KeywordsCanBeIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#functionNameBase}.
	 * @param ctx the parse tree
	 */
	void enterFunctionNameBase(SQLParser.FunctionNameBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#functionNameBase}.
	 * @param ctx the parse tree
	 */
	void exitFunctionNameBase(SQLParser.FunctionNameBaseContext ctx);
}