package com.PA2.Database;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.Expose;
import javafx.scene.control.Tab;

import java.util.*;

// TODO: Look up going from a TREESET to a TREEMAP
// TODO: SPLIT function into logical operation and comparison operation
/**
 * This Class contains functions that works on the table class.
 *
 * Where there are functions that allow JSONized values to be inserted and remade into Java Objects.
 * There are some internal functions that are only are called by user-based functions that are called from the Database
 * class.
 **/
final class Table {
    // Variables for Table Object
    @Expose()
    private ArrayList<Column> ColumnsAvailable = new ArrayList<>();
    @Expose()
    private ArrayList<String> PrimaryKeys = new ArrayList<>();
    @Expose()
    private TreeMap<String,JsonObject> Entries = new TreeMap<>();
    @Expose(serialize = false, deserialize = false)
    private boolean write = false;
    @Expose(serialize = false, deserialize = false)
    private boolean temporary = false;
    @Expose(serialize = false, deserialize = false)
    private String CopiedFromTable;
    /**
     ************************************************************************
     ****************** Functions used by the FileSlave *********************
     ************************************************************************
     ************************************************************************
     **/
    public ArrayList<Column> getColumnsAvailable() {
        return ColumnsAvailable;
    }
    public void setColumnsAvailable(ArrayList<Column> columnsAvailable) {
        ColumnsAvailable = columnsAvailable;
    }
    public TreeMap<String,JsonObject> getEntries() {
        return Entries;
    }
    public void setEntries(TreeMap<String,JsonObject> entries) {
        Entries = entries;
    }
    public ArrayList<String> getPrimaryKeys() {
        return PrimaryKeys;
    }
    public void setPrimaryKeys(ArrayList<String> primaryKeys) {
        PrimaryKeys = primaryKeys;
    }

    String EntriesToString() { return new Gson().toJson(Entries); }
    /**
     ************************************************************************
     **************** I N T E R N A L  F U N C T I O N S ********************
     ************************************************************************
     ************************************************************************
     **/
    private void GeneratePrimaryKey(){
            if (PrimaryKeys.isEmpty()){
                // User did not specify any columns for primary keys
                // Therefore the engine will keep an internal one with numbers casted to a String
                PrimaryKeys.add("0");
            }
    }
    // This function solves the key for the map whether it be a specific value in the table to order the map or
    // it be a incrementing value from zero  stated by the @GeneratePrimaryKey() function
     String GatherPrimaryKeys(JsonObject objThatNeedsAKey) throws NumberFormatException, NoSuchElementException {
            GeneratePrimaryKey();
            if(PrimaryKeys.get(0).equals("0") && Entries.isEmpty()) return String.valueOf(0);
            else if (PrimaryKeys.get(0).equals("0")){
                int IsNumber = Integer.parseInt(Entries.lastKey());
                ++IsNumber;
                return String.valueOf(IsNumber);
            }else{
                StringBuilder PrimaryKeysAsString = new StringBuilder();
                for(String key : PrimaryKeys){
                    if(key.equals(PrimaryKeys.get(PrimaryKeys.size()-1))){
                        JsonElement val = objThatNeedsAKey.get(key);
                        PrimaryKeysAsString.append(val.getAsString());
                    }
                    else{
                        JsonElement val = objThatNeedsAKey.get(key);
                        PrimaryKeysAsString.append(val.getAsString() + "|");
                    }
                }
                return PrimaryKeysAsString.toString();
            }
    }
    // This function splits the values of the primary key and returns an Array List
    // on the values that were primary keys
    ArrayList<String> DigestPrimaryKeys(String TreeMapKey){
        return new ArrayList<>(Arrays.asList(TreeMapKey.split("\\|")));
    }
    // add entries from a set to the map
    protected void addEntryFromSet(TreeSet<Map.Entry<String,JsonObject>> EntriesReqiured) throws TableHandlingException{
        if(!this.Entries.isEmpty()) throw new TableHandlingException("Using this function requires" +
                " a newly initiated object!");
        else{
            for(Map.Entry<String,JsonObject> e : EntriesReqiured){
                this.Entries.put(e.getKey(),e.getValue());
            }
        }
    }
    // Rename column name and set the entries keys accordingly
    void renameColumn(Column OldColumn ,Column NewColumn) throws  TableHandlingException{
        for(Map.Entry<String,JsonObject> pair : Entries.entrySet()){
            Iterator keySet = pair.getValue().keySet().iterator();
            String key;
            while(keySet.hasNext()){
                key = (String)keySet.next();
                if(key.contentEquals(OldColumn.getName())){
                    key = NewColumn.getName();
                    break;
                }
                else
                    throw new TableHandlingException("The Entries doest not contain the specified Column name : "+
                            OldColumn.getName());
            }
        }

    }
    // Used by CompareJsonPrimitiveWithOperator to get the correct primitives for comparison
    // Where the first JsonPrimitive is always an integer specifying what the other primitves should
    // be casted to for further comparison
    private ArrayList<JsonElement> LookUpJsonObject(JsonObject fromTable,
                                                    JsonObject fromUser)
    throws TableHandlingException{
        ArrayList<JsonElement> finalAnswer = new ArrayList<>(3);
        //layout the set of key value pairs coming from the user... this is always one set
        Set UserSet = fromUser.entrySet();
        // cast this set to an object array
        Object UserSetArray[] = UserSet.toArray();
        // make a map entry (key, value) pair from the first set in the array
        Map.Entry<String,JsonElement> UserCompareEntry = (Map.Entry<String,JsonElement>)UserSetArray[0];
        // Create a Column Object to compare and set its name
        Column ColumnToCompare = new Column();
        ColumnToCompare.setName(UserCompareEntry.getKey());
        // search for that column name and get its type
        for(Column currentColumn : ColumnsAvailable){
            // Soft comparison that prohibit the user from always specifying the Column Object TYPE
            if(currentColumn.compareNames(ColumnToCompare)) {
                switch(currentColumn.getType()){
                    case  "byte": finalAnswer.add(new JsonPrimitive(0)); break;
                    case "string": finalAnswer.add(new JsonPrimitive(1));break;
                    case "integer": finalAnswer.add(new JsonPrimitive(2)); break;
                    case "short": finalAnswer.add(new JsonPrimitive(3));break;
                    case "long": finalAnswer.add(new JsonPrimitive(4));break;
                    case "double": finalAnswer.add(new JsonPrimitive(5));break;
                    case "float": finalAnswer.add(new JsonPrimitive(6));break;
                    case "bigdecimal": finalAnswer.add(new JsonPrimitive(7));break;
                    case "biginteger": finalAnswer.add(new JsonPrimitive(8));break;
                    case "char": finalAnswer.add(new JsonPrimitive(9));break;
                    case "varchar" : finalAnswer.add(new JsonPrimitive(10));break;
                    default: finalAnswer.add(new JsonPrimitive(-1));break;
                }
                break;
            }
        }
        if(finalAnswer.get(0).getAsInt()==-1) throw new TableHandlingException("Column " +
                ColumnToCompare.getName() + " does NOT Exist!");
        else{
            finalAnswer.add(fromTable.get(UserCompareEntry.getKey()));
            finalAnswer.add(UserCompareEntry.getValue());
        }
        return finalAnswer;
    }
    private boolean CompareJsonPrimativeWithOperator(JsonObject fromTable,
                                          JsonObject fromUser,
                                          String comparisonType)
            throws TableHandlingException{
        boolean returnBack = false;
            try{
                int i = 2;
                ArrayList<JsonElement> elementsToCompare = LookUpJsonObject(fromTable, fromUser);
                int CastToWhat = elementsToCompare.get(0).getAsInt();
                switch(CastToWhat) {
                    case 0: //byte
//                        byte a = b ? 0; < ? (-); > ? (+);
                         i = Byte.compare(elementsToCompare.get(1).getAsByte(),elementsToCompare.get(2).getAsByte());break;
                    case 1: //string
                        i = elementsToCompare.get(1).getAsString().compareTo(elementsToCompare.get(2).getAsString());break;
                    case 2: //integer
                      i =  Integer.compare(elementsToCompare.get(1).getAsInt(),elementsToCompare.get(2).getAsInt());break;
                    case 3: //short
                        i =  Short.compare(elementsToCompare.get(1).getAsShort(),elementsToCompare.get(2).getAsShort());break;
                    case 4: //long
                        i =  Long.compare(elementsToCompare.get(1).getAsLong(),elementsToCompare.get(2).getAsLong());break;
                    case 5: //double
                        i =  Double.compare(elementsToCompare.get(1).getAsDouble(),elementsToCompare.get(2).getAsDouble());break;
                    case 6: //float
                        i =  Float.compare(elementsToCompare.get(1).getAsFloat(),elementsToCompare.get(2).getAsFloat());break;
                    case 7: //bigdecimal
                        i =  elementsToCompare.get(1).getAsBigDecimal().compareTo(elementsToCompare.get(2).getAsBigDecimal());break;
                    case 8: //biginteger
                        i =  elementsToCompare.get(1).getAsBigInteger().compareTo(elementsToCompare.get(2).getAsBigInteger());break;
                    case 9: //char
                        i =  Character.compare(elementsToCompare.get(1).getAsCharacter(),elementsToCompare.get(2).getAsCharacter());break;
                    case 10: //varchar
                        i =  Character.compare(elementsToCompare.get(1).getAsCharacter(),elementsToCompare.get(2).getAsCharacter());break;
                }
                switch (comparisonType){
                    case "!=":
                    case "<>":
                        returnBack = i!=0;break;
                    case "=":
                        returnBack = i==0;break;
                    case ">":
                        returnBack = i > 0;break;
                    case "<":
                        returnBack = i < 0;break;
                    case ">=":
                        returnBack = (i == 0) || (i>0);break;
                    case "<=":
                        returnBack = (i == 0) || (i<0);break;
//                    case "BETWEEN":
//                    case "LIKE":
//                    case "IN":
                }
            }
            catch(TableHandlingException e){
                //If the column did not exist
                System.out.println(e.getMessage());
            }
        return returnBack;
        }
    /**
     ************************************************************************
     ****************** Database Class calling functions ********************
     ************************************************************************
     **/
    // if the user is making a temp table then copy the name of it's parent
    // this functionality will help when solving unions, differences, and products
    protected void setCopiedFrom(String TableNameCopied) {this.CopiedFromTable = TableNameCopied;}
    protected String getCopiedFrom() {return this.CopiedFromTable;}
    // has the user set this table to write, if so they are not they are able to change
    protected void putWriteFlagAs(boolean setWriteTo){this.write=setWriteTo;}
    protected boolean rtnWriteFlag(){return this.write;}
    // has the DB set this table to temporary, if so the user is making a temporary table
    protected void putTempFlagAs(boolean setTempTo){this.temporary=setTempTo;}
    protected boolean rtnTempFlag(){return this.temporary;}
    // if the current table instance contains a specific column
    protected boolean containsColumn(String ColumnName){
        Column cc = new Column();
        cc.setName(ColumnName);
        for (Column c : ColumnsAvailable) if(c.compareNames(cc)) return true;
        return false;
    }
//    void updateEntry(JsonObject fromUser) throws TableHandlingException {
//        try {
//            Iterator entriesIterator = Entries.entrySet().iterator();
//            ArrayList<String> delete = new ArrayList<>();
//            ArrayList<JsonObject> insert = new ArrayList<>();
//            //Go through each entry
//            while (entriesIterator.hasNext()) {
//                // convert to the JSON Entry and its primary key
//                Map.Entry<String, JsonObject> entryValue = (Map.Entry<String, JsonObject>) entriesIterator.next();
//                // make a deep copy
//                JsonObject newEntry = entryValue.getValue().deepCopy();
//                //set the iterator again
//                Iterator userIterator = fromUser.entrySet().iterator();
//                // now step by each column and its value of json object to go through the users changes
//                while (userIterator.hasNext()) {
//                    // extract each column and its value
//                    Map.Entry<String, JsonPrimitive> UserKeyValueEntry = (Map.Entry<String, JsonPrimitive>) userIterator.next();
//                    // if the entries in the table doesn't have what the user is wanting to change throw an exception
//                    if (!entryValue.getValue().has(UserKeyValueEntry.getKey()))
//                        throw new TableHandlingException("Values contained in " + fromUser.toString() +
//                                "does not exist in " + entryValue.getValue().toString());
//                    else {
//                        newEntry.add(UserKeyValueEntry.getKey(), UserKeyValueEntry.getValue());
//                    }
//                    insert.add(newEntry);
//                }
//                delete.add(entryValue.getKey());
//            }
//            insert.forEach(UtilException.rethrowConsumer(this::addEntryToTable));
//            deleteEntries(delete);
//        } catch (TableHandlingException e) {
//            System.out.println(e.getCause().toString());
//        }
//    }
void updateEntry(Map<String,JsonObject> updatedMapPart) throws TableHandlingException {
        if (!Entries.entrySet().addAll(updatedMapPart.entrySet()))
            throw new TableHandlingException("There was an issue updating entries!");
}
    // used to only go a rename columns, these columns are already created so the user is just changing the name
    void renameColumn (String OldColumnName, String NewColumnName){
        Column o = new Column();
        Column n = new Column();
        o.setName(OldColumnName);
        n.setName(NewColumnName);
        try {
            renameColumn(o, n);
        }catch(TableHandlingException e){
            System.out.println(e.getMessage());
        }

    }
    // Functions to handle retrieving data from the user!
//    void addEntryToTable(JsonObject objAdd) throws TableHandlingException{
//        for(String key : PrimaryKeys){
//            if(!objAdd.has(key))
//                throw new TableHandlingException("Entry " + objAdd.toString() + " already Exists");
//        }
//        Entries.put(GatherPrimaryKeys(objAdd),objAdd);
//
//    }
    void addEntryToTable(JsonObject objAdd) throws TableHandlingException{
        Iterator<Column> ColumnsAvailableIterator = ColumnsAvailable.iterator();
        while (ColumnsAvailableIterator.hasNext()){
            Column CheckColumn = ColumnsAvailableIterator.next();
            if(!objAdd.has(CheckColumn.getName()))
                throw new TableHandlingException("Entry " + objAdd.toString() + " is an invalid entry");
        }
        Entries.put(GatherPrimaryKeys(objAdd),objAdd);
    }
    void addEntryToTable(Column EnterToColumn,JsonObject objAdd) throws TableHandlingException{
            if(!ColumnsAvailable.contains(EnterToColumn))
                throw new TableHandlingException("Entry " + objAdd.toString() + " already Exists");
            //Entries.put(c,objAdd);
            Entries.put(GatherPrimaryKeys(objAdd),objAdd);

    }
    void addEntryToTable(String ColumnName,JsonObject objAdd) throws TableHandlingException{
            Iterator<Column> ColumnsAvailableIterator = ColumnsAvailable.iterator();
            boolean NoEntry = true;
            while (ColumnsAvailableIterator.hasNext()){
                Column CheckColumn = ColumnsAvailableIterator.next();
                if(CheckColumn.getName().equals(ColumnName)){
                    Entries.put(GatherPrimaryKeys(objAdd),objAdd);
                    NoEntry = false;
                    break;
                }
            }
            if(NoEntry)
                throw new TableHandlingException("Entry " + objAdd.toString() + " already Exists");
        }
    void addColumnToTable(String ColumnName, String DataType, boolean PrimaryKey ){
        try{
            addColumnToTable(ColumnName,DataType,0,PrimaryKey);

        }catch(TableHandlingException e){
            System.out.println(e.getMessage());
        }
    }
    void addColumnToTable(String ColumnName, String DataType,
                          int Constraint, boolean PrimaryKey ) throws TableHandlingException{
            if(PrimaryKey) PrimaryKeys.add(ColumnName);
            DataType = DataType.toLowerCase();
            Column ColumnToAdd = new Column(ColumnName, DataType, Constraint);
            if(ColumnsAvailable.contains(ColumnToAdd))
                throw new TableHandlingException("Column " + ColumnName + " already Exists");
            ColumnsAvailable.add(ColumnToAdd);

    }
    // Remove a column by the Column's Name from the table and it's entries
    boolean dropColumn(String ColumnName){
        try{
            Column GeneratedColumn = new Column();
            GeneratedColumn.setName(ColumnName);
            dropColumn(GeneratedColumn);
        }catch(TableHandlingException e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
    // Removes a column by the JAVA Column Object and from the rest of the values from the
    // Entries in the table
    boolean dropColumn(Column ColumnToDrop) throws TableHandlingException{
        boolean HaveColumn = false;
        for(Column currentColumn : ColumnsAvailable){
            // Soft comparison that prohibit the user from always specifying the Column Object TYPE
            if(currentColumn.compareNames(ColumnToDrop)) {
                HaveColumn = true;
                Collection c = Entries.values();
                //obtain an Iterator for Collection
                Iterator itr = c.iterator();
                while(itr.hasNext()){
                   JsonObject objectBeingAltered = (JsonObject)itr.next();
                   objectBeingAltered.remove(ColumnToDrop.getName());
                }
                //ColumnsAvailable.remove(currentColumn);
                return true;
            }
        }
        throw new TableHandlingException("Column " + ColumnToDrop.getName() + " does NOT Exist!");
    }
    // Removes entries from the table
    boolean deleteEntries(String EntryToDelete) throws TableHandlingException{
        //obtain an Iterator for Collection
        try{
        Entries.remove(EntryToDelete);
        return true;
        }
        catch(NullPointerException n){
         throw new TableHandlingException("'"+EntryToDelete+"'" + " primary key was not found in Table!");
        }
    }
    // Wrapper function that will handle one comparison
    Map<String,JsonObject> lookupEntry(JsonObject CompareObject, String Operator)
    {
        try{
        ArrayList<String> StringArray = new ArrayList<>(1);
        ArrayList<JsonObject> ObjectArray = new ArrayList<>(1);
        ArrayList<Map<String, JsonObject>> returnedMap = lookUpEntries(ObjectArray, StringArray);
        return returnedMap.get(0);
    }catch(TableHandlingException e ){
            System.out.println(e.getMessage());
        }
        return null;
    }

    // This keeps usage of ArrayList java object throughout the program to help limit the amount of casting
    // Allows the look up of table entries where the user specifies criteria to meet when performing the search
    // Called by UpdateEntries in Database class
    protected
    ArrayList<Map<String,JsonObject>>
    lookUpEntries(ArrayList<JsonObject> ComparingObjects,
                  ArrayList<String> JsonCompareOperators) throws TableHandlingException{
        //make an array list of the sets for further conjoining
      ArrayList<Map<String, JsonObject>> FurtherConjoining = new ArrayList<>();
        //there will always be the same amount of objects with amount of comparison operators
        if(JsonCompareOperators.size() != ComparingObjects.size()) throw new TableHandlingException( "The number of" +
                " comparisons : " + String.valueOf(JsonCompareOperators.size()) + " does not match the amount of values "+
                "comparing to : " + String.valueOf(ComparingObjects.size()));
        else {
            //obtain an Iterator for Array lists
            Iterator ComparingItr = ComparingObjects.iterator();
            Iterator OperatorIterator = JsonCompareOperators.iterator();
            while (ComparingItr.hasNext() && OperatorIterator.hasNext()) {
                Map<String,JsonObject> returnTree = new TreeMap<>();
                JsonObject compareJ = (JsonObject)ComparingItr.next();
                String compareS = (String)OperatorIterator.next();
                for (Map.Entry<String, JsonObject> pair : Entries.entrySet()) {
                    //iterate over the pairs
                    //if the entry meets the requirement by the user then hold in the set
                    if (CompareJsonPrimativeWithOperator(pair.getValue(), compareJ, compareS))
                        returnTree.put(pair.getKey(),pair.getValue());
                }
                FurtherConjoining.add(returnTree);
            }
        }
        return FurtherConjoining;
    }
    // gather entries of a specific column in JsonObject form
    protected ArrayList<JsonObject> gatherColumnValues(String overrideKey, String ColumnToGet)
            throws TableHandlingException{
        ArrayList<JsonObject> returningValues = new ArrayList<>();
        if(overrideKey.isEmpty()) overrideKey = ColumnToGet;
        if(!containsColumn(ColumnToGet)) throw new TableHandlingException("Column : "+ ColumnToGet +
                " does not exist!");
        for(Map.Entry<String,JsonObject> entry : Entries.entrySet()){
            JsonObject Val = new JsonObject();
            Val.add(overrideKey,entry.getValue().get(ColumnToGet));
            returningValues.add(Val);
        }
        return returningValues;
    }

    // create a table from a set and a Columns available array
    // USAGE: requires user to make sure the Columns are set properly for
    protected
    void copyEntrySet(ArrayList<Column> ColumnsFromOldTable,
                   TreeSet<Map.Entry<String, JsonObject>> SetOfEntriesFromOldTable)
    throws ColumnHandlingException{
        try{
            if(SetOfEntriesFromOldTable.first().getValue().has(ColumnsFromOldTable.get(0).getName())){
                this.ColumnsAvailable = ColumnsFromOldTable;
                addEntryFromSet(SetOfEntriesFromOldTable);
            }
            else throw new ColumnHandlingException("A specific column was not apart of the entry set");

        }catch (TableHandlingException e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    /**
     ************************************************************************
     ************************************************************************
     **/
}