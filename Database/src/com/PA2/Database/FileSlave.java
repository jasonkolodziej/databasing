package com.PA2.Database;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import com.google.gson.*;
import java.io.FileWriter;

final class FileSlave {
      static Database Parse(String FileToOpen, Database DatabaseToSet){
        Gson gson = new Gson();
        try (Reader reader = new FileReader(FileToOpen)) {
            // Convert JSON to Java Object
            DatabaseToSet = gson.fromJson(reader, Database.class);
            return DatabaseToSet;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    static void Write(String Filename, Database DatabaseToFile){
        // Convert object to JSON string
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        //String json = gson.toJson(DatabaseToFile);
       // System.out.println(json);
        try (FileWriter writer = new FileWriter(Filename)) {
            gson.toJson(DatabaseToFile, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}