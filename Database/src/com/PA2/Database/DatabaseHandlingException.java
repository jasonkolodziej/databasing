package com.PA2.Database;

class DatabaseHandlingException extends Exception {
    public DatabaseHandlingException(String whatException) {
        super(whatException);
    }
}