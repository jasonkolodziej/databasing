package com.PA2.Database;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;

public class TestDBPkg {
    private static final String dbName = "Test";
    public static void main(String[] args) {
       // try{
            //// Create db //// open,// create,
            System.out.println("-----------Creating New Database \'"+dbName+"\'-------------");
            Database d = new Database();
            d.setName(dbName);
            System.out.println("-----------Creating New Table \'"+"Address Book"+"\'-------------");
            d.CreateTableInDatabase("Address Book");
            d.CreateColumn("Address Book" ,"First Name","String");
            d.CreateColumn("Address Book" ,"Last Name","String");
            d.CreateColumn("Address Book" ,"Phone Number","String");
            System.out.println("-----------Inserting Entries in \'"+"Address Book"+"\'-------------");
            /// first entry
            JsonElement ele1 = new JsonPrimitive("Jason");
            JsonElement ele2 = new JsonPrimitive("Dog");
            JsonElement ele3= new JsonPrimitive("12345678");
            JsonObject entry1 = new JsonObject();
            entry1.add("First Name",ele1);
            entry1.add("Last Name",ele2);
            entry1.add("Phone Number",ele3);
            d.InsertEntry("Address Book", entry1);
            //second entry
            JsonElement ele12 = new JsonPrimitive("Jack");
            JsonElement ele22 = new JsonPrimitive("Black");
            JsonElement ele32= new JsonPrimitive("5556745678");
            JsonObject entry2 = new JsonObject();
            entry2.add("First Name",ele12);
            entry2.add("Last Name",ele22);
            entry2.add("Phone Number",ele32);
            d.InsertEntry("Address Book", entry2);
            //third entry
            JsonElement ele122 = new JsonPrimitive("Larry");
            JsonElement ele222 = new JsonPrimitive("Harry");
            JsonElement ele322= new JsonPrimitive("34534534343");
            JsonObject entry3 = new JsonObject();
            entry3.add("First Name",ele122);
            entry3.add("Last Name",ele222);
            entry3.add("Phone Number",ele322);
            d.InsertEntry("Address Book", entry3);
            // show,
            System.out.println("-----------Showing Changes in \'"+"Address Book"+"\'-------------");
            d.PrintTable("Address Book");
            // write,
            System.out.println("-----------Writing Changes in \'"+"Address Book"+"\'-------------");
            d.SetTableToWrite("Address Book");
            // close,            // exit,
            System.out.println("-----------Closing and Exiting Database\'"+dbName+"\'-------------");
            d.SetTableToWrite("Address Book");
            try{
                    d.CloseDatabase();
            }catch(DatabaseHandlingException de){
                    System.out.println(de.getMessage());
                    de.printStackTrace();
            }
            // update,
            System.out.println("--------------Reopening Database\'"+dbName+"\'-------------");
            Database e = new Database();
            try{
                    e.OpenDatabase(dbName+".db");

            }catch(DatabaseHandlingException de){
                    System.out.println(de.getMessage());
                    de.printStackTrace();
            }
            // delete
            System.out.println("-----------Deleting All Entries in Table where 'First Name' is " +
                    "'Larry' in "+"Address Book"+"\'-------------");
            JsonObject cc = new JsonObject();
            JsonElement rr = new JsonPrimitive("Larry");
            cc.add("First Name",rr);
            ArrayList<String> ttt = new ArrayList<>(1);
            ArrayList<JsonObject> ooo = new ArrayList<>(1);
            ooo.add(cc);
            ttt.add("=");
            e.DeleteEntries("Address Book",ooo,ttt);
/////////////////////
            // selection,
            System.out.println("-----------Selecting All Entries in Table where 'First Name' is " +
                    "'Jack' and updating to 'Jeff' in "+"Address Book"+"\'-------------");
            JsonObject ne = new JsonObject();
            JsonElement j = new JsonPrimitive("Jeff");
            ne.add("First Name",j);
            JsonObject c = new JsonObject();
            JsonElement r = new JsonPrimitive("Jack");
            c.add("First Name",r);
            ArrayList<String> tt = new ArrayList<>(1);
            ArrayList<JsonObject> oo = new ArrayList<>(1);
            oo.add(c);
            tt.add("=");
           // e.UpdateEntries("Address Book",ne,oo,tt);
            e.PrintTable("Address Book");
            // projection,
            System.out.println("Projecting Address Book to temporary table 'Shortened Contact List'");
            System.out.println("Renaming Column 'Last Name' in Address Book to 'Lname' " +
                    "temporary table 'Shortened Contact List'");
            try {
                    ArrayList<String> cn = new ArrayList<>(2);
                    cn.add("Last Name");
                    cn.add("Phone Number");
                    e.CreateTableInDatabase("Shortened Contact List",
                            true, "Address Book");
                    e.ProjectFromTable("Shortened Contact List","Address Book"
                    ,cn).renameColumn("Last Name","LName");
            }catch(DatabaseHandlingException dd){
                    System.out.println(dd.getMessage());
                    dd.printStackTrace();
            }
            e.PrintTable("Address Book");
            // renaming,
            // union,
            // difference,
            // product,
    }
}
