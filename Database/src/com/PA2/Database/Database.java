package com.PA2.Database;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.*;
 final public class Database{
    // Class member Variables
    //TODO: make sure that when doing the UNION of two sets to compare each sets keySet this will ensure they have the same column names
    @Expose()
    private String Name;
    @Expose(serialize = false, deserialize = false)
    private static String FileName = "";
    public Database(){}
    public Database(String Name ){this.Name=Name;}
    // Decide whether or not we need to make this private to the Database class / or protected
    @Expose()
    private Map<String,Table> Tables = new TreeMap<>();
    /**
     ************************************************************************
     ****************** Functions used by the File Slave ********************
     ************************************************************************
     ************************************************************************
     **/
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Map<String, Table> getTables() {
        return Tables;
    }

    public void setTables(Map<String, Table> tables) {
        Tables = tables;
    }
    /**
     ************************************************************************
     **************** I N T E R N A L  F U N C T I O N S ********************
     ************************************************************************
     ************************************************************************
     **/
    private void AddTableToDatabase(String TableName, Table TableToAdd){
        Tables.put(TableName,TableToAdd);
    }
    private void RemoveTableFromDatabase(String TableName){ Tables.remove(TableName); }
    private void RemoveTemporaryTable(String TempTableName){ if(Tables.get(TempTableName).rtnTempFlag())
        RemoveTableFromDatabase(TempTableName);
    }
    private void RemoveTemporaryTables(){Tables.keySet().forEach(this::RemoveTemporaryTable);}
    private Set<String> GetTablesToWrite(){
        Set<String> returnSet = Tables.keySet();
        for(Map.Entry<String,Table> table : Tables.entrySet()){
            if(table.getValue().rtnWriteFlag()) returnSet.remove(table.getKey());
        }
        return returnSet;
    }
    private void RevertToOldInstances(Database lastInstance){
        Set<String> tablesToKeep = GetTablesToWrite();
        Set<String> tablesToReplace = lastInstance.getTables().keySet();
        tablesToReplace.removeAll(tablesToKeep);
        for(String lastTableName : tablesToReplace){
            AddTableToDatabase(lastTableName,lastInstance.Tables.get(lastTableName));
        }
    }

     // allows the user to create a temporary instance of a table and select the columns they want to keep
     private Table ProjectFromTable(Table ReceivingTable,Table TableToCopy, ArrayList<String> ColumnsRequested){
        // ReceivingTable was a newly created table from CreateTableInDatabase
        ReceivingTable.setEntries(TableToCopy.getEntries());
        ReceivingTable.setColumnsAvailable(TableToCopy.getColumnsAvailable());
        ArrayList<Column> curr = ReceivingTable.getColumnsAvailable();
         Iterator col = curr.iterator();
         try{
             while (col.hasNext()){
                 Column c = (Column)col.next();
                 if(!ColumnsRequested.contains(c.getName()))
                     ReceivingTable.dropColumn(c);
             }
             for (Column d:ReceivingTable.getColumnsAvailable()
                  ) {if (ColumnsRequested.contains(d.getName())) ReceivingTable.getColumnsAvailable().remove(d);}
             return ReceivingTable;
         }catch (TableHandlingException e){
             System.out.println(e.getMessage());
             e.printStackTrace();
         }
        return null;
     }
     // construct a new ColumnsAvailable from Table A and B and check to see if they have the same type
     private ArrayList<Column> ConstructColumnsAvailable(Table A, Table B)
     throws DatabaseHandlingException{
         ArrayList<Column> returningInstance = new ArrayList<>();
         for(int i = 0; i<A.getColumnsAvailable().size(); ++i)
         {
             Column a = new Column();
             if(!A.getColumnsAvailable().get(i).getName().equals(B.getColumnsAvailable().get(i).getName()))
             throw new DatabaseHandlingException("Column Name : " + A.getColumnsAvailable().get(i).getType() +
                     " does NOT match Column Name : " + B.getColumnsAvailable().get(i).getType());
             else if(!A.getColumnsAvailable().get(i).getType().equals(B.getColumnsAvailable().get(i).getType()))
                 throw new DatabaseHandlingException("Column Type : " + A.getColumnsAvailable().get(i).getType() +
                         " does NOT match Column Type : " + B.getColumnsAvailable().get(i).getType());
             else if (A.getColumnsAvailable().get(i).getConstraint() < B.getColumnsAvailable().get(i).getConstraint()){
                 a.setName(B.getColumnsAvailable().get(i).getName());
                 a.setType(B.getColumnsAvailable().get(i).getType());
                 a.setConstraint(B.getColumnsAvailable().get(i).getConstraint());
                 returningInstance.add(a);
             }
             else {
                 a.setName(A.getColumnsAvailable().get(i).getName());
                 a.setType(A.getColumnsAvailable().get(i).getType());
                 a.setConstraint(A.getColumnsAvailable().get(i).getConstraint());
                 returningInstance.add(a);
             }
         }
         return returningInstance;
     }
     // passed by GetTableFromDatabase, where Table A is preferred over Table B,
     // in terms of final table returned
     private Table LogicallyOperateOnTables(String TempTableName,String ParentTable,
                                            String operator, Table A, Table B)
     throws DatabaseHandlingException{
        try{
            Table returningInstance = CreateTableInDatabase(TempTableName,true,ParentTable);
            returningInstance.setColumnsAvailable(ConstructColumnsAvailable(A,B));
            if(A.getColumnsAvailable().size() < B.getColumnsAvailable().size())
                returningInstance.setEntries(B.getEntries());
            else
                returningInstance.setEntries(A.getEntries());
            if(operator.equals("*") && A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                operator = "+";
            if (operator.equals("+") && A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                returningInstance.getEntries().entrySet().addAll(B.getEntries().entrySet());
            else if(operator.equals("-") && A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                returningInstance.getEntries().entrySet().removeAll(B.getEntries().entrySet());
            else if(operator.equals("*"))
                ProductOfTables(returningInstance,B);
            else
                throw new DatabaseHandlingException("Invalid SYNTAX for Operator : " + operator);
            return returningInstance;
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
        return null;
     }
     // calculate Product of Table A on Table B, where A has more columns than B
     private Table ProductOfTables(Table A, Table B){
          // A always has more columns from LogicallyOperateOnTables
         // a has more entries in json so subtract b and get what is left b
         // then add to a new json object, then add the entry back to Table A
         try {
             for (Map.Entry<String, JsonObject> bJ : B.getEntries().entrySet()) {
                 for (JsonObject aJ : A.getEntries().values()) {
                     Iterator aCol = A.getColumnsAvailable().iterator();
                     JsonObject b = bJ.getValue();
                     JsonObject bAdd = b.deepCopy();
                     while (aCol.hasNext()) {
                         Column aC = (Column) aCol.next();
                         String aCName = aC.getName();
                         if (!bAdd.has(aCName)) bAdd.add(aCName, aJ.get(aCName));
                     }
                     A.addEntryToTable(bAdd);
                 }
             }
             return A;
         }catch (TableHandlingException e){
             System.out.println(e.getMessage());
             e.printStackTrace();
         }
         return null;
     }
    /**
     ************************************************************************
     ********** Functions used by the DML Interpreter Class *****************
     ************************************************************************
     ************************************************************************
     **/
     // allows the user to create a temporary instance of a table and select the columns they want to keep
     public Table ProjectFromTable(String TableReceiver,String TableToCopy, ArrayList<String> ColumnsRequested){
         try {
            return ProjectFromTable(GetTableInDatabase(TableReceiver),GetTableInDatabase(TableToCopy),ColumnsRequested);
         }catch (DatabaseHandlingException d){
             System.out.println(d.getMessage());
             d.printStackTrace();
         }
         return null;
     }

    // prints a particular table, specified by the user, in the tables map according to JSON format
    public void PrintTable(String TableName){
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithoutExposeAnnotation();
            builder.setPrettyPrinting();
            Gson gson = builder.create();
            String json = gson.toJson(GetTableInDatabase(TableName));
            System.out.print("\""+TableName+"\": ");
            System.out.println(json);
        }catch (DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }
    // insert an entry into a specific table
    public void InsertEntry(String TableName, JsonObject Entry){
     try{
         Table mod = GetTableInDatabase(TableName);
         mod.addEntryToTable(Entry);
     }catch(DatabaseHandlingException d){
         System.out.println(d.getMessage());
         d.printStackTrace();
     }
     catch(TableHandlingException e){
         e.printStackTrace();
     }
    }
    // insert column in database
    public void CreateColumn(String TableName, String ColumnName, String ColumnType){
        try{
            Table mod = GetTableInDatabase(TableName);
            mod.addColumnToTable(ColumnName,ColumnType,false);
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }
    // will set a particular table, given by the user, to write.
    public void SetTableToWrite(String TableName){try{GetTableInDatabase(TableName).putWriteFlagAs(true);}
    catch (DatabaseHandlingException d){ System.out.println(d.getMessage()); d.printStackTrace();}}
    // for actual created tables by the user
    public Table CreateTableInDatabase(String TableName){
        try { return CreateTableInDatabase(TableName,false, "");}
        catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
        }
        return null;
    }
    // for temporary tables wanted by the user
    public Table CreateTableInDatabase(String TableName, boolean TempTable, String ParentTable)
            throws DatabaseHandlingException{
        if(Tables.containsKey(TableName)) throw new DatabaseHandlingException("Table " + TableName + " already exists!");
        else{
            Table NewTable = new Table();
            NewTable.setCopiedFrom(ParentTable);
            NewTable.putTempFlagAs(TempTable);
            AddTableToDatabase(TableName, NewTable);
            return NewTable;
        }
    }
     public void DeleteEntries(String TableName,
                               ArrayList<JsonObject> ObjToCmp,
                               ArrayList<String>CompStr){
         try {
             Table u = GetTableInDatabase(TableName);
             ArrayList<Map<String,JsonObject>> m = u.lookUpEntries(ObjToCmp,CompStr);
             Iterator<Map<String,JsonObject>> i = m.iterator();
             while (i.hasNext()){
                   for (String d : i.next().keySet()){
                       u.deleteEntries(d);
                   }
             }
         }catch(DatabaseHandlingException de){
             System.out.println(de.getMessage());
             de.printStackTrace();
         }catch (TableHandlingException t){
             System.out.println(t.getMessage());
             t.printStackTrace();
         }
     }

    //update Entries given the following constraints
    public void UpdateEntries(String TableName, JsonObject replaceWith,
                              ArrayList<JsonObject> ObjToCmp,
                              ArrayList<String>CompStr){
         try {
           Table u = GetTableInDatabase(TableName);
           ArrayList<Map<String,JsonObject>> m = u.lookUpEntries(ObjToCmp,CompStr);
           while (m.iterator().hasNext()){
               Map<String,JsonObject> n = m.iterator().next();
               Set<Map.Entry<String,JsonObject>> l = new TreeSet<>(JsonUtil.MAP_OBJECT_KEY_and_JSON_VALUE_SET_COMPARATOR);
               l.addAll(n.entrySet());
               Map<String,JsonObject> nm = new TreeMap<>();
               if(!u.getPrimaryKeys().get(0).equals("0")){ //PrimaryKeys were set
                   while(l.iterator().hasNext()){
                       JsonObject copy = l.iterator().next().getValue().deepCopy();
                       copy.entrySet().addAll(replaceWith.entrySet());
                       String NewPK = u.GatherPrimaryKeys(copy);
                       nm.put(NewPK,copy);
                   }
               }
               while(l.iterator().hasNext()){
                   l.iterator().next().getValue().entrySet().addAll(replaceWith.entrySet());
               }
               if(!u.getPrimaryKeys().get(0).equals("0")) u.updateEntry(nm);
               else u.updateEntry(n);
           }
         }catch (DatabaseHandlingException d){
             System.out.println(d.getMessage());
             d.printStackTrace();
         }catch (TableHandlingException t){
             System.out.println(t.getMessage());
             t.printStackTrace();
         }
    }
    //retrieve a particular data base in the Tables value
    public Table GetTableInDatabase(String TableName) throws DatabaseHandlingException{
        if(!Tables.containsKey(TableName)) throw new DatabaseHandlingException("Table name : "+ TableName +
                " does not exist!");
        return Tables.get(TableName);
    }
    // close this instance of Java Database Object
    public void CloseDatabase() throws DatabaseHandlingException{
        RemoveTemporaryTables();
        // send this class to the file handler
        Database lastInstance = new Database();
        if(FileName.isEmpty() && Name.isEmpty())
            throw new DatabaseHandlingException("A filename could not be determined!");
        else if(!FileName.isEmpty()){
            // there was a previous instance
            lastInstance.OpenDatabase(FileName);
            RevertToOldInstances(lastInstance);
            FileSlave.Write(FileName,this);
        }
        else{
            FileSlave.Write(Name + ".db",this);
        }
    }
    // close this instance of Java Database Object
    public void OpenDatabase(String DatabaseFileName)throws DatabaseHandlingException{
       Database dataIn = FileSlave.Parse(DatabaseFileName, this);
       if (dataIn==null) throw new DatabaseHandlingException("Database : "+ DatabaseFileName + " was not found!");
       this.Name = dataIn.Name;
       this.Tables = dataIn.Tables;
       this.FileName = DatabaseFileName;
        // send this class to the JsonParser
    }
    // close a particular table only if the write flag is not set
    // if the write flag is set then we need to keep it open until the user calls CloseDatabase();
    public void CloseTable(String TableName){
        try {
            if(!GetTableInDatabase(TableName).rtnWriteFlag()){RemoveTableFromDatabase(TableName);}
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }

}
