package com.PA2.Database;

class ColumnHandlingException extends Exception {
    public ColumnHandlingException(String whatException) {
        super(whatException);
    }
}