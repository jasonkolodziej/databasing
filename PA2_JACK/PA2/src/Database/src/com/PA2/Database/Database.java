package Database.src.com.PA2.Database;
import com.google.gson.*;
import com.google.gson.annotations.Expose;


import javax.json.Json;
import java.io.Serializable;
import java.util.*;
final public class Database implements Serializable {
    // Class member Variables
    //TODO: make sure that when doing the UNION of two sets to compare each sets keySet this will ensure they have the same column names
    @Expose()
    private String Name;
    @Expose(serialize = false, deserialize = false)
    private static String FileName = "";

    public Database() {
    }

    public Database(String Name) {
        this.Name = Name;
    }

    // Decide whether or not we need to make this private to the Database class / or protected
    @Expose()
    private Map<String, Table> Tables = new TreeMap<>();

    /**
     * ***********************************************************************
     * ***************** Functions used by the File Slave ********************
     * ***********************************************************************
     * ***********************************************************************
     **/
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Map<String, Table> getTables() {
        return Tables;
    }

    public void setTables(Map<String, Table> tables) {
        Tables = tables;
    }

    /**
     * ***********************************************************************
     * *************** I N T E R N A L  F U N C T I O N S ********************
     * ***********************************************************************
     * ***********************************************************************
     **/
    private void AddTableToDatabase(String TableName, Table TableToAdd) {
        Tables.put(TableName, TableToAdd);
    }

    private void RemoveTableFromDatabase(String TableName) {
        Tables.remove(TableName);
    }

    private void RemoveTemporaryTable(String TempTableName) {
        if (Tables.get(TempTableName).rtnTempFlag())
            RemoveTableFromDatabase(TempTableName);
    }

    private void RemoveTemporaryTables() {
        Tables.keySet().forEach(this::RemoveTemporaryTable);
    }

    private Set<String> GetTablesToWrite() {
        Set<String> returnSet = Tables.keySet();
        for (Map.Entry<String, Table> table : Tables.entrySet()) {
            if (table.getValue().rtnWriteFlag()) returnSet.remove(table.getKey());
        }
        return returnSet;
    }

    private void RevertToOldInstances(Database lastInstance) {
        Set<String> tablesToKeep = GetTablesToWrite();
        Set<String> tablesToReplace = lastInstance.getTables().keySet();
        tablesToReplace.removeAll(tablesToKeep);
        for (String lastTableName : tablesToReplace) {
            AddTableToDatabase(lastTableName, lastInstance.Tables.get(lastTableName));
        }
    }

    // allows the user to create a temporary instance of a table and select the columns they want to keep
    private Table ProjectFromTable(Table ReceivingTable, Table TableToCopy, ArrayList<String> ColumnsRequested) {
        // ReceivingTable was a newly created table from CreateTableInDatabase
        ReceivingTable.setColumnsAvailable((ArrayList<Column>) JsonUtil.deepClone(TableToCopy.getColumnsAvailable()));
        ReceivingTable.setEntries(JsonUtil.deepCopyMap(TableToCopy.getEntries()));
        // ReceivingTable.setColumnsAvailable(TableToCopy.getColumnsAvailable());
        ArrayList<Column> curr = TableToCopy.getColumnsAvailable();
        Iterator col = curr.iterator();
        try {
            while (col.hasNext()) {
                Column c = (Column) col.next();
                if (!ColumnsRequested.contains(c.getName()))
                    ReceivingTable.dropColumn(c);
            }
            return ReceivingTable;
        } catch (TableHandlingException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    // construct a new ColumnsAvailable from Table A and B and check to see if they have the same type
    private void ConstructColumnsAvailable(Table A, Table B,Table C, boolean product)
    //Table C is being worked on
            throws DatabaseHandlingException {
        //ArrayList<Column> returningInstance = new ArrayList<>();
        try {
            if (!product)
                for (int i = 0; i < A.getColumnsAvailable().size(); ++i) {

                    //Column a = new Column();
                    if (!A.getColumnsAvailable().get(i).getName().equals(B.getColumnsAvailable().get(i).getName()))
                        throw new DatabaseHandlingException("Column Name : " + A.getColumnsAvailable().get(i).getType() +
                                " does NOT match Column Name : " + B.getColumnsAvailable().get(i).getType());
                    else if (!A.getColumnsAvailable().get(i).getType().equals(B.getColumnsAvailable().get(i).getType()))
                        throw new DatabaseHandlingException("Column Type : " + A.getColumnsAvailable().get(i).getType() +
                                " does NOT match Column Type : " + B.getColumnsAvailable().get(i).getType());
                    else if (A.getColumnsAvailable().get(i).getConstraint() < B.getColumnsAvailable().get(i).getConstraint()) {
//                     a.setName(B.getColumnsAvailable().get(i).getName());
//                     a.setType(B.getColumnsAvailable().get(i).getType());
//                     a.setConstraint(B.getColumnsAvailable().get(i).getConstraint());
                        C.addColumnToTable(B.getColumnsAvailable().get(i).getName(),
                                B.getColumnsAvailable().get(i).getType(),
                                B.getColumnsAvailable().get(i).getConstraint(), false);
                    } else {
//                         a.setName(A.getColumnsAvailable().get(i).getName());
//                         a.setType(A.getColumnsAvailable().get(i).getType());
//                         a.setConstraint(A.getColumnsAvailable().get(i).getConstraint());
//                         returningInstance.add(a);
                        C.addColumnToTable(A.getColumnsAvailable().get(i).getName(),
                                A.getColumnsAvailable().get(i).getType(),
                                A.getColumnsAvailable().get(i).getConstraint(), false);
                    }
                }
            else {
//                 returningInstance = (ArrayList<Column>) JsonUtil.deepClone(A.getColumnsAvailable());
//                 for (Column c : B.getColumnsAvailable()) {
//                     Column ac = (Column) JsonUtil.deepClone(c);
//                     returningInstance.add(ac);
//                 }
                for (int i = 0; i < A.getColumnsAvailable().size(); ++i) {
                    C.addColumnToTable(A.getColumnsAvailable().get(i).getName(),
                            A.getColumnsAvailable().get(i).getType(),
                            A.getColumnsAvailable().get(i).getConstraint(), false);
                }
                for (int i = 0; i < B.getColumnsAvailable().size(); ++i) {
                    C.addColumnToTable(B.getColumnsAvailable().get(i).getName(),
                            B.getColumnsAvailable().get(i).getType(),
                            B.getColumnsAvailable().get(i).getConstraint(), false);
                }
            }
        } catch (TableHandlingException te) {
            System.out.println(te.getMessage());
            te.printStackTrace();
        }
        //return returningInstance;
    }

    // passed by GetTableFromDatabase, where Table A is preferred over Table B,
    // in terms of final table returned

    private void LogicallyWorkOnEntries(TreeMap<String,JsonObject> a,
                                        TreeMap<String,JsonObject> b,
                                        String operator, Table beingWorkedOn)
            throws DatabaseHandlingException {
        Map<String, JsonObject> finalMap = new TreeMap<>();
        // assume a has the bigger columns from the calling method of this function
        Set<String> as = new HashSet<>(a.keySet());
        Set<String> bs = b.keySet();
        switch(operator){
            case "+":
                as.addAll(bs); break;
            case "-":
                as.retainAll(bs); break;
            case "&": as.containsAll(bs); break;
            case "*": break;
            default: throw new DatabaseHandlingException("Operator : "+ operator +" cannot be applied!");
        }
        if(operator.equals("*")){
            try{
                finalMap = a;
                int setSize = finalMap.size();
                Set<Map.Entry<String,JsonObject>> fes = finalMap.entrySet();
                Object [] fee = fes.toArray();
                for(int i = 0; i<setSize; ++i){
                    Map.Entry<String,JsonObject> fe =(Map.Entry<String, JsonObject>) fee[i];
                    JsonObject ff =  fe.getValue();
                    boolean first = true;
                    for(JsonObject bj : b.values()){
                        JsonObject ffj = new JsonObject();
                        if(!first) ffj = ff.deepCopy();
                        for(String bjs : bj.keySet()){
                            if(!first) ffj.add(bjs,bj.get(bjs));
                            else ff.add(bjs,bj.get(bjs));
                        }
                        if(!first) beingWorkedOn.addEntryToTable(ffj);
                        first = false;
                    }
                }
            }catch(TableHandlingException t){
                System.out.println(t.getMessage());
                t.printStackTrace();
            }
        }else{
            as.addAll(bs);
            for(String ae : as)
            {
                JsonObject ja;
                if(a.get(ae)==null) ja = b.get(ae).deepCopy();
                else ja = a.get(ae).deepCopy();
                finalMap.put(ae,ja);
            }
        }
    }


    public Table LogicallyOperateOnTables(String NewTempTableName,String ParentTable,
                                          String operator, Table B)
            throws DatabaseHandlingException{
        try{
            Table returningInstance = CreateTableInDatabase(NewTempTableName,true,ParentTable);
            Table A = GetTableInDatabase(ParentTable);
            // Make sure the columns maintain the same order
            TreeMap<String, JsonObject> smallerHalf = new TreeMap<>();
            if(A.getColumnsAvailable().size() < B.getColumnsAvailable().size()){
                returningInstance.setEntries(JsonUtil.deepCopyMap(B.getEntries()));
                ConstructColumnsAvailable(B,A,returningInstance,true);
                smallerHalf = A.getEntries();
            }
            else if (A.getColumnsAvailable().size() > B.getColumnsAvailable().size()){
//                returningInstance.setColumnsAvailable(ConstructColumnsAvailable(A,B,true));
                ConstructColumnsAvailable(A,B,returningInstance,true);
                returningInstance.setEntries(JsonUtil.deepCopyMap(A.getEntries()));
                smallerHalf = B.getEntries();
            }
//            else returningInstance.setColumnsAvailable(ConstructColumnsAvailable(A,B,false));
            else ConstructColumnsAvailable(A,B,returningInstance,false);
            if(operator.equals("*") && A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                operator = "+";
            if ((operator.equals("+") || operator.equals("&")) &&
                    A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                LogicallyWorkOnEntries(returningInstance.getEntries(), smallerHalf, "+", returningInstance);
            else if(operator.equals("-") && A.getColumnsAvailable().size()==B.getColumnsAvailable().size())
                LogicallyWorkOnEntries(returningInstance.getEntries(), smallerHalf, "-", returningInstance);
            else if(operator.equals("*"))
                LogicallyWorkOnEntries(returningInstance.getEntries(), smallerHalf, "*", returningInstance);
            else
                throw new DatabaseHandlingException("Invalid SYNTAX for Operator : " + operator);
            return returningInstance;
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
        return null;
    }


    // calculate Product of Table A on Table B, where A has more columns than B
//     private Table ProductOfTables(Table A, Table B){
//          // A always has more columns from LogicallyOperateOnTables
//         // a has more entries in json so subtract b and get what is left b
//         // then add to a new json object, then add the entry back to Table A
//         try {
//             for (Map.Entry<String, JsonObject> bJ : B.getEntries().entrySet()) {
//                 for (JsonObject aJ : A.getEntries().values()) {
//                     Iterator aCol = A.getColumnsAvailable().iterator();
//                     JsonObject b = bJ.getValue();
//                     JsonObject bAdd = b.deepCopy();
//                     while (aCol.hasNext()) {
//                         Column aC = (Column) aCol.next();
//                         String aCName = aC.getName();
//                         if (!bAdd.has(aCName)) bAdd.add(aCName, aJ.get(aCName));
//                     }
//                     A.addEntryToTable(bAdd);
//                 }
//             }
//             return A;
//         }catch (TableHandlingException e){
//             System.out.println(e.getMessage());
//             e.printStackTrace();
//         }
//         return null;
//     }
    /**
     ************************************************************************
     ********** Functions used by the DML Interpreter Class *****************
     ************************************************************************
     ************************************************************************
     **/

    // allows the user to create a temporary instance of a table and select the columns they want to keep
    public Table ProjectFromTable(String TableReceiver,String TableToCopy, ArrayList<String> ColumnsRequested){
        try {
            return ProjectFromTable(GetTableInDatabase(TableReceiver),GetTableInDatabase(TableToCopy),ColumnsRequested);
        }catch (DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
        return null;
    }

    // prints a particular table, specified by the user, in the tables map according to JSON format
    public void PrintTable(String TableName){
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithoutExposeAnnotation();
            builder.setPrettyPrinting();
            Gson gson = builder.create();
            String json = gson.toJson(GetTableInDatabase(TableName));
            System.out.print("\""+TableName+"\": ");
            System.out.println(json);
        }catch (DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }
    // make it easy for the user to make an entry in the table
    public void InsertEntry(String TableName,ArrayList<String> Columns, ArrayList<JsonPrimitive> RelatedValues)
            throws DatabaseHandlingException {
        if(Columns.size()!=RelatedValues.size()){
            int sizeDiff = Columns.size() - RelatedValues.size();
            if (sizeDiff<0) throw new DatabaseHandlingException("More Values were specified that amount of columns!");
            for(int i = 0; i < sizeDiff; ++i) RelatedValues.add(null);
        }
        JsonObject objectToAdd = new JsonObject();
        for(int j = 0; j < Columns.size(); ++j) objectToAdd.add(Columns.get(j),RelatedValues.get(j));
        InsertEntry(TableName, objectToAdd);
    }
    // insert an entry into a specific table
    public void InsertEntry(String TableName, JsonObject Entry){
        try{
            Table mod = GetTableInDatabase(TableName);
            mod.addEntryToTable(Entry);
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
        catch(TableHandlingException e){
            e.printStackTrace();
        }
    }
    // insert column in database
    public void CreateColumn(String TableName, String ColumnName, String ColumnType){
        try{
            Table mod = GetTableInDatabase(TableName);
            mod.addColumnToTable(ColumnName,ColumnType,false);
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }
    // will set a particular table, given by the user, to write.
    public void SetTableToWrite(String TableName){try{GetTableInDatabase(TableName).putWriteFlagAs(true);}
    catch (DatabaseHandlingException d){ System.out.println(d.getMessage()); d.printStackTrace();}}
    // for actual created tables by the user
    public Table CreateTableInDatabase(String TableName){
        try { return CreateTableInDatabase(TableName,false, "");}
        catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
        }
        return null;
    }
    // for temporary tables wanted by the user
    public Table CreateTableInDatabase(String TableName, boolean TempTable, String ParentTable)
            throws DatabaseHandlingException{
        if(Tables.containsKey(TableName)) throw new DatabaseHandlingException("Table " + TableName + " already exists!");
        else{
            Table NewTable = new Table();
            NewTable.setCopiedFrom(ParentTable);
            NewTable.putTempFlagAs(TempTable);
            AddTableToDatabase(TableName, NewTable);
            return NewTable;
        }
    }
    public void DeleteEntries(String TableName,
                              ArrayList<JsonObject> ObjToCmp,
                              ArrayList<String>CompStr, ArrayList<String> Logic){
        try {
            Table u = GetTableInDatabase(TableName);
            ArrayList<Map<String,JsonObject>> m = u.lookUpEntries(ObjToCmp,CompStr, Logic);
            Iterator<Map<String,JsonObject>> i = m.iterator();
            while (i.hasNext()){
                for (String d : i.next().keySet()){
                    u.deleteEntries(d);
                }
            }
        }catch(DatabaseHandlingException de){
            System.out.println(de.getMessage());
            de.printStackTrace();
        }catch (TableHandlingException t){
            System.out.println(t.getMessage());
            t.printStackTrace();
        }
    }

    //update Entries given the following constraints
    public void UpdateEntries(String TableName, JsonObject replaceWith,
                              ArrayList<JsonObject> ObjToCmp,
                              ArrayList<String>CompStr, ArrayList<String> compLog){
        try {
            Table u = GetTableInDatabase(TableName);
            ArrayList<Map<String,JsonObject>> mapsReturned = u.lookUpEntries(ObjToCmp,CompStr, compLog);
            if(replaceWith.size()==0){ // they user is requesting a filter
                u.setEntries((TreeMap<String, JsonObject>) mapsReturned.get(0));
                return;
            }
            Iterator<Map<String,JsonObject>> i = mapsReturned.iterator();
            while (i.hasNext()){
                Map<String, JsonObject> currMap = i.next();
                if(!u.getPrimaryKeys().get(0).equals("0")){ //PrimaryKeys were set
                    // go through map
                    Iterator<Map.Entry<String, JsonObject>> curMapIt = currMap.entrySet().iterator();
                    while(curMapIt.hasNext()){
                        // get the first entry to mess with
                        Map.Entry<String, JsonObject> me = curMapIt.next();
                        // make a json object copy
                        JsonObject JOcopy = me.getValue().deepCopy();
                        // go through the values wanting to be replaced by the user and add them to the object
                        for(Map.Entry<String,JsonElement> rjo : replaceWith.entrySet()){
                            JOcopy.add(rjo.getKey(),rjo.getValue());
                        }
                        //generate a new primary key
                        String NewPK = u.GatherPrimaryKeys(JOcopy);
                        // put it back in the map for further processing
                        currMap.put(NewPK,JOcopy);
                        // delete the object from the table
                        u.deleteEntries(me.getKey());
                    }

                }
                //Set<Map.Entry<String, JsonObject>> currMapset = currMap.entrySet();
                for(Map.Entry<String, JsonObject> jo : currMap.entrySet()){
                    String key = jo.getKey();
                    JsonObject ob = jo.getValue().deepCopy();
                    for(Map.Entry<String,JsonElement> rjo : replaceWith.entrySet()){
                        ob.add(rjo.getKey(),rjo.getValue());
                    }
                    currMap.put(key,ob);
                }

                ///////
                //apply other operations here
                /////////////
                u.updateEntry(currMap);
            }
        }catch (DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }catch (TableHandlingException t){
            System.out.println(t.getMessage());
            t.printStackTrace();
        }
    }








    //retrieve a particular data base in the Tables value
    public Table GetTableInDatabase(String TableName) throws DatabaseHandlingException{
        if(!Tables.containsKey(TableName)) throw new DatabaseHandlingException("Table name : "+ TableName +
                " does not exist!");
        return Tables.get(TableName);
    }
    // close this instance of Java Database Object
    public void CloseDatabase() throws DatabaseHandlingException{
        RemoveTemporaryTables();
        // send this class to the file handler
        Database lastInstance = new Database();
        if(FileName.isEmpty() && Name.isEmpty())
            throw new DatabaseHandlingException("A filename could not be determined!");
        else if(!FileName.isEmpty()){
            // there was a previous instance
            lastInstance.OpenDatabase(FileName);
            RevertToOldInstances(lastInstance);
            FileSlave.Write(FileName,this);
        }
        else{
            FileSlave.Write(Name + ".db",this);
        }
    }
    // close this instance of Java Database Object
    public void OpenDatabase(String DatabaseFileName)throws DatabaseHandlingException{
        Database dataIn = FileSlave.Parse(DatabaseFileName, this);
        if (dataIn==null) throw new DatabaseHandlingException("Database : "+ DatabaseFileName + " was not found!");
        this.Name = dataIn.Name;
        this.Tables = dataIn.Tables;
        this.FileName = DatabaseFileName;
        // send this class to the JsonParser
    }
    // close a particular table only if the write flag is not set
    // if the write flag is set then we need to keep it open until the user calls CloseDatabase();
    public void CloseTable(String TableName){
        try {
            if(!GetTableInDatabase(TableName).rtnWriteFlag()){RemoveTableFromDatabase(TableName);}
        }catch(DatabaseHandlingException d){
            System.out.println(d.getMessage());
            d.printStackTrace();
        }
    }

}