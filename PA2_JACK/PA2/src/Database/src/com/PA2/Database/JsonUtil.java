package Database.src.com.PA2.Database;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

class JsonUtil {

    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static TreeMap<String,JsonObject> deepCopyMap(TreeMap<String,JsonObject> obj){
        TreeMap<String,JsonObject> newCopy = new TreeMap<>();
        for(Map.Entry<String,JsonObject> i : obj.entrySet()){
            JsonObject objToInsert = i.getValue().deepCopy();
            newCopy.put(i.getKey(),objToInsert);
        }
        return newCopy;
    }

}

//    private JsonUtil() {}
//    static final Comparator<Map.Entry<String,JsonObject>> MAP_OBJECT_KEY_and_JSON_VALUE_SET_COMPARATOR = new Comparator<Map.Entry<String,JsonObject>>() {
//        @Override
//        public int compare(Map.Entry<String, JsonObject> lhs, Map.Entry<String, JsonObject> rhs) {
//            return lhs==null ?
//                    (rhs==null ? 0 : -1) :
//                    (rhs==null ? 1 : lhs.getKey().compareTo(rhs.getKey()));
//        }
//    };
//
//     static final Comparator<Map.Entry<String,JsonObject>> MAP_OBJECT_JSON_VALUE_SET_COMPARATOR = new Comparator<Map.Entry<String,JsonObject>>() {
//        @Override
//        public int compare(Map.Entry<String, JsonObject> lhs, Map.Entry<String, JsonObject> rhs) {
//            return lhs==null ?
//                    (rhs==null ? 0 : -1) :
//                    (rhs==null ? 1 : JsonUtil.JSON_OBJECT_STRICT_COMPARATOR.compare(lhs.getValue(),rhs.getValue()));
//        }
//    };
//
//    static final Comparator<JsonObject> JSON_OBJECT_STRICT_COMPARATOR = new Comparator<JsonObject>() {
//        @Override
//        public int compare(JsonObject lhs, JsonObject rhs) {
//            if (lhs.size()> rhs.size()) return 1;
//            else if (lhs.size() < rhs.size()) return 1;
//            return lhs==null ?
//                    (rhs==null ? 0 : -1) :
//                    (rhs==null ? 1 : JsonUtil.strictComparison(lhs, rhs));
//        }
//    };
//
//    static final Comparator<JsonObject> JSON_OBJECT_LAZY_COMPARATOR = new Comparator<JsonObject>() {
//        @Override
//        public int compare(JsonObject lhs, JsonObject rhs) {
//            int rtn;
//            return lhs==null ?
//                    (rhs==null ? 0 : -1) :
//                    (rhs==null ? 1 : JsonUtil.JSON_ELEMENT_SET_COMPARATOR.compare(lhs,rhs));
//        }
//    };
//
//     static final Comparator<Map.Entry<String,JsonElement>> JSON_OBJECT_KEY_SET_COMPARATOR = new Comparator<Map.Entry<String,JsonElement>>() {
//        @Override
//        public int compare(Map.Entry<String, JsonElement> lhs, Map.Entry<String, JsonElement> rhs) {
//            return lhs==null ?
//                    (rhs==null ? 0 : -1) :
//                    (rhs==null ? 1 : lhs.getKey().compareTo(rhs.getKey()));
//        }
//    };
//     static final Comparator<Map.Entry<String,JsonElement>> JSON_OBJECT_VALUE_SET_COMPARATOR = new Comparator<Map.Entry<String,JsonElement>>() {
//        @Override
//        public int compare(Map.Entry<String, JsonElement> lhs, Map.Entry<String, JsonElement> rhs) {
////            Set<JsonElement> Slhs = lhs.
////            return lhs==null ?
////                    (rhs==null ? 0 : -1) :
////                    (rhs==null ? 1 : lhs.getKey().compareTo(rhs.getKey()));
//            return JsonUtil.JSON_ELEMENT_SET_COMPARATOR.compare(lhs.getValue(), rhs.getValue());
//        }
//    };
//
//     static final Comparator<JsonElement> JSON_ELEMENT_SET_COMPARATOR = new Comparator<JsonElement>() {
//        @Override
//        public int compare(JsonElement x, JsonElement y) {
//            // REMEMBER: x = y ? 0; < ? (-); > ? (+);
//                int cmp = 0;
//                // Check whether both jsonElement are not null
//                if (x != null && y != null) {
//                    // Check whether both jsonElement are objects
//                    if (x.isJsonObject() && y.isJsonObject()) {
//                        Set<Map.Entry<String, JsonElement>> ens1 = ((JsonObject) x).entrySet();
//                        Set<Map.Entry<String, JsonElement>> ens2 = ((JsonObject) y).entrySet();
//                        JsonObject json2obj = (JsonObject) y;
//                        if (ens1 != null && ens2 != null && (ens2.size() == ens1.size()))
//                            // Iterate JSON Elements with Key values
//                            for (Map.Entry<String, JsonElement> en : ens1)
//                                cmp = cmp + this.compare(en.getValue(), json2obj.get(en.getKey()));
//                        else {
//                            if(((JsonObject)x).size() > ((JsonObject) y).size()) return 1;
//                            else return -1;
//                        }
//                    }
//                    // Check whether both jsonElement are arrays
//                    else if (x.isJsonArray() && y.isJsonArray()) {
//                        JsonArray jarr1 = x.getAsJsonArray();
//                        JsonArray jarr2 = y.getAsJsonArray();
//                        if (jarr1.size() != jarr2.size()) return -1;
//                        else {
//                            // Iterate JSON Array to JSON Elements
//                            for (JsonElement je1 : jarr1) {
//                                int flag = 0;
//                                for(JsonElement je2 : jarr2){
//                                    flag = this.compare(je1, je2);
//                                    if(flag==0){
//                                        jarr2.remove(je2);
//                                        break;
//                                    }
//                                }
//                                cmp = cmp + flag;
//                            }
//                        }
//                    }
//                    // Check whether both jsonElement are null
//                    else if (x.isJsonNull() && y.isJsonNull()) return 0;
//                    // Check whether both jsonElement are primitives
//                    else if (x.isJsonPrimitive() && y.isJsonPrimitive()) {
//                        if (x.equals(y)) return 0;
//                        else return 1;
//                    }
//                    else return -1; //false
//                } else if (x == null && y == null) return 0;
//                else return -1;
//                return cmp;
//        }
//    };
//
//    /**
//     * Sort all elements in Json objects, to ensure that they are identically serialized.
//     * @param el
//     * @return
//     */
//    public static JsonElement deepSort(JsonElement el) {
//        if (el == null) {
//            return null;
//        } else if (el.isJsonArray()) {
//            JsonArray sortedArray = new JsonArray();
//            for (JsonElement subEl : (JsonArray)el) {
//                sortedArray.add(deepSort(subEl));
//            }
//            return sortedArray;
//        } else if (el.isJsonObject()){
//            List<Map.Entry<String,JsonElement>> entrySet = new ArrayList<Map.Entry<String,JsonElement>>(((JsonObject)el).entrySet());
//            Collections.sort(entrySet, JSON_OBJECT_KEY_SET_COMPARATOR);
//            JsonObject sortedObject = new JsonObject();
//            for (Map.Entry<String,JsonElement> entry : entrySet) {
//                sortedObject.add(entry.getKey(), deepSort(entry.getValue()));
//            }
//            return sortedObject;
//        } else {
//            return el;
//        }
//    }
//
//    private static int strictComparison(JsonObject lhs , JsonObject rhs){
//        int rtn = 0;
//        Iterator<Map.Entry<String,JsonElement>> rs = rhs.entrySet().iterator();
//        Iterator<Map.Entry<String,JsonElement>> ls = lhs.entrySet().iterator();
//        while(rs.hasNext() && ls.hasNext()){
//            rtn = rtn + (JsonUtil.JSON_OBJECT_KEY_SET_COMPARATOR.compare(ls.next(),rs.next()) +
//                    JsonUtil.JSON_OBJECT_VALUE_SET_COMPARATOR.compare(ls.next(),rs.next()));
//        }
//        return rtn;
//    }
//
//    /**
//     * Returns a deep copy of the JsonElement.  not thread safe.
//     * @param el
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//    public static <T extends JsonElement> T deepCopy(T el) {
//        if (el == null) {
//            return (T) JsonNull.INSTANCE;
//        } else if (el.isJsonArray()) {
//            JsonArray array = new JsonArray();
//            for (JsonElement subEl : el.getAsJsonArray()) {
//                array.add(deepCopy(subEl));
//            }
//            return (T)array;
//        } else if (el.isJsonObject()) {
//            JsonObject object = new JsonObject();
//            for (Map.Entry<String, JsonElement> entry : el.getAsJsonObject().entrySet()) {
//                object.add(entry.getKey(), deepCopy(entry.getValue()));
//            }
//            return (T)object;
//        } else {
//            return (T)el;
//        }
//    }
//}