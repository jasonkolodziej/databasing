package Database.src.com.PA2.Database;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * This Class contains functions that works on the column class.
 *
 * Where there are functions that allow JSONized values to be inserted and remade into Java Objects.
 * There are some internal functions that are only are called by hierarchy classes.
 **/
final class Column implements Comparable<Column>, Serializable {
    @Expose()
    private String ColumnName;
    @Expose()
    private String ColumnType;
    @Expose()
    // used to handle VARCHAR instance type columns, where the value related to the
    private int Constraint = 0;
    Column(){}
    Column(String Name, String Type){this.ColumnName=Name; this.ColumnType=Type; this.Constraint = 0;}
    Column(String Name, String Type, int Constraint){this.ColumnName=Name;
        this.ColumnType=Type; this.Constraint = Constraint;}

    public int getConstraint() { return Constraint; }

    public void setConstraint(int constraint) { Constraint = constraint; }

    public String getName() {
        return ColumnName;
    }

    public void setName(String name) {
        ColumnName = name;
    }

    public String getType() {
        return ColumnType;
    }

    public void setType(String type) {
        ColumnType = type;
    }

    /**** Usage by the Table and Database Java Classes ****/
    boolean compareNames(Column other) { return this.ColumnName.equals(other.ColumnName); }

    /*** For Java Map Class usage ****/
    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }
    public int compareTo(Column o) {
        int i =this.ColumnName.compareTo(o.ColumnName);
        int j = this.ColumnType.compareTo(o.ColumnType);
        return(i+j==0 && (i!=0) && (j!=0)) ? -1 : i+j;
    }
    //////////////////////////////////
}