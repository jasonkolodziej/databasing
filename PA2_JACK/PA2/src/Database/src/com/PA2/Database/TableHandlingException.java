package Database.src.com.PA2.Database;

class TableHandlingException extends Exception {
    public TableHandlingException(String whatException)
    {
        super(whatException);
    }
}