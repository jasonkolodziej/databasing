package Database.src.com.PA2.Database;

public class DatabaseHandlingException extends Exception {
    public DatabaseHandlingException(String whatException) {
        super(whatException);
    }
}