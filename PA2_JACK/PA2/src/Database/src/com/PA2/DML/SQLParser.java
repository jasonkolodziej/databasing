// Generated from /Users/johnkeegan/Desktop/PA2/src/Database/src/com/PA2/DML/SQL.g4 by ANTLR 4.7
package Database.src.com.PA2.DML;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, CREATE=21, TABLE=22, OPEN=23, CLOSE=24, 
		WRITE=25, EXIT=26, SHOW=27, UPDATE=28, INSERT=29, INTO=30, DELETE=31, 
		SET=32, VALUES=33, WHERE=34, FROM=35, RELATION=36, SELECT=37, PROJECT=38, 
		RENAME=39, VARCHAR=40, NULL=41, IDENTIFIER=42, INTEGER=43, ALPHA=44, DIGIT=45, 
		WS=46;
	public static final int
		RULE_program = 0, RULE_command = 1, RULE_openCMD = 2, RULE_closeCMD = 3, 
		RULE_writeCMD = 4, RULE_exitCMD = 5, RULE_showCMD = 6, RULE_createCMD = 7, 
		RULE_updateCMD = 8, RULE_insertCMD = 9, RULE_deleteCMD = 10, RULE_query = 11, 
		RULE_expr = 12, RULE_atomicEXPR = 13, RULE_selectionEXP = 14, RULE_projectionEXP = 15, 
		RULE_renamingEXP = 16, RULE_unionEXP = 17, RULE_differenceEXP = 18, RULE_productEXP = 19, 
		RULE_condition = 20, RULE_conjunction = 21, RULE_comparison = 22, RULE_type = 23, 
		RULE_literal = 24, RULE_operator = 25, RULE_operand = 26, RULE_relationName = 27, 
		RULE_attributeName = 28, RULE_typedAttributeList = 29, RULE_attributeList = 30;
	public static final String[] ruleNames = {
		"program", "command", "openCMD", "closeCMD", "writeCMD", "exitCMD", "showCMD", 
		"createCMD", "updateCMD", "insertCMD", "deleteCMD", "query", "expr", "atomicEXPR", 
		"selectionEXP", "projectionEXP", "renamingEXP", "unionEXP", "differenceEXP", 
		"productEXP", "condition", "conjunction", "comparison", "type", "literal", 
		"operator", "operand", "relationName", "attributeName", "typedAttributeList", 
		"attributeList"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'('", "')'", "'PRIMARY KEY'", "'='", "','", "'<-'", "'+'", 
		"'-'", "'*'", "'||'", "'&&'", "'VARCHAR'", "'INTEGER'", "'<'", "'>'", 
		"'<='", "'>='", "'!='", "'=='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "CREATE", "TABLE", 
		"OPEN", "CLOSE", "WRITE", "EXIT", "SHOW", "UPDATE", "INSERT", "INTO", 
		"DELETE", "SET", "VALUES", "WHERE", "FROM", "RELATION", "SELECT", "PROJECT", 
		"RENAME", "VARCHAR", "NULL", "IDENTIFIER", "INTEGER", "ALPHA", "DIGIT", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<QueryContext> query() {
			return getRuleContexts(QueryContext.class);
		}
		public QueryContext query(int i) {
			return getRuleContext(QueryContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CREATE) | (1L << OPEN) | (1L << CLOSE) | (1L << WRITE) | (1L << EXIT) | (1L << SHOW) | (1L << UPDATE) | (1L << INSERT) | (1L << DELETE) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(64);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CREATE:
				case OPEN:
				case CLOSE:
				case WRITE:
				case EXIT:
				case SHOW:
				case UPDATE:
				case INSERT:
				case DELETE:
					{
					setState(62);
					command();
					}
					break;
				case IDENTIFIER:
					{
					setState(63);
					query();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public OpenCMDContext openCMD() {
			return getRuleContext(OpenCMDContext.class,0);
		}
		public CloseCMDContext closeCMD() {
			return getRuleContext(CloseCMDContext.class,0);
		}
		public WriteCMDContext writeCMD() {
			return getRuleContext(WriteCMDContext.class,0);
		}
		public ExitCMDContext exitCMD() {
			return getRuleContext(ExitCMDContext.class,0);
		}
		public ShowCMDContext showCMD() {
			return getRuleContext(ShowCMDContext.class,0);
		}
		public CreateCMDContext createCMD() {
			return getRuleContext(CreateCMDContext.class,0);
		}
		public UpdateCMDContext updateCMD() {
			return getRuleContext(UpdateCMDContext.class,0);
		}
		public InsertCMDContext insertCMD() {
			return getRuleContext(InsertCMDContext.class,0);
		}
		public DeleteCMDContext deleteCMD() {
			return getRuleContext(DeleteCMDContext.class,0);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_command);
		try {
			setState(78);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(69);
				openCMD();
				}
				break;
			case CLOSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(70);
				closeCMD();
				}
				break;
			case WRITE:
				enterOuterAlt(_localctx, 3);
				{
				setState(71);
				writeCMD();
				}
				break;
			case EXIT:
				enterOuterAlt(_localctx, 4);
				{
				setState(72);
				exitCMD();
				}
				break;
			case SHOW:
				enterOuterAlt(_localctx, 5);
				{
				setState(73);
				showCMD();
				}
				break;
			case CREATE:
				enterOuterAlt(_localctx, 6);
				{
				setState(74);
				createCMD();
				}
				break;
			case UPDATE:
				enterOuterAlt(_localctx, 7);
				{
				setState(75);
				updateCMD();
				}
				break;
			case INSERT:
				enterOuterAlt(_localctx, 8);
				{
				setState(76);
				insertCMD();
				}
				break;
			case DELETE:
				enterOuterAlt(_localctx, 9);
				{
				setState(77);
				deleteCMD();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpenCMDContext extends ParserRuleContext {
		public TerminalNode OPEN() { return getToken(SQLParser.OPEN, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public OpenCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_openCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOpenCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOpenCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOpenCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpenCMDContext openCMD() throws RecognitionException {
		OpenCMDContext _localctx = new OpenCMDContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_openCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			match(OPEN);
			setState(81);
			relationName();
			setState(82);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloseCMDContext extends ParserRuleContext {
		public TerminalNode CLOSE() { return getToken(SQLParser.CLOSE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public CloseCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closeCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCloseCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCloseCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCloseCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloseCMDContext closeCMD() throws RecognitionException {
		CloseCMDContext _localctx = new CloseCMDContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_closeCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			match(CLOSE);
			setState(85);
			relationName();
			setState(86);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WriteCMDContext extends ParserRuleContext {
		public TerminalNode WRITE() { return getToken(SQLParser.WRITE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public WriteCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_writeCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterWriteCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitWriteCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitWriteCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WriteCMDContext writeCMD() throws RecognitionException {
		WriteCMDContext _localctx = new WriteCMDContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_writeCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88);
			match(WRITE);
			setState(89);
			relationName();
			setState(90);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExitCMDContext extends ParserRuleContext {
		public TerminalNode EXIT() { return getToken(SQLParser.EXIT, 0); }
		public ExitCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exitCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExitCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExitCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExitCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExitCMDContext exitCMD() throws RecognitionException {
		ExitCMDContext _localctx = new ExitCMDContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_exitCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(EXIT);
			setState(93);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShowCMDContext extends ParserRuleContext {
		public TerminalNode SHOW() { return getToken(SQLParser.SHOW, 0); }
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public ShowCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_showCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterShowCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitShowCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitShowCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShowCMDContext showCMD() throws RecognitionException {
		ShowCMDContext _localctx = new ShowCMDContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_showCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			match(SHOW);
			setState(96);
			atomicEXPR();
			setState(97);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateCMDContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(SQLParser.CREATE, 0); }
		public TerminalNode TABLE() { return getToken(SQLParser.TABLE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TypedAttributeListContext typedAttributeList() {
			return getRuleContext(TypedAttributeListContext.class,0);
		}
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public CreateCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreateCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreateCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreateCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateCMDContext createCMD() throws RecognitionException {
		CreateCMDContext _localctx = new CreateCMDContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_createCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(CREATE);
			setState(100);
			match(TABLE);
			setState(101);
			relationName();
			setState(102);
			match(T__1);
			setState(103);
			typedAttributeList();
			setState(104);
			match(T__2);
			setState(105);
			match(T__3);
			setState(106);
			match(T__1);
			setState(107);
			attributeList();
			setState(108);
			match(T__2);
			setState(109);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpdateCMDContext extends ParserRuleContext {
		public TerminalNode UPDATE() { return getToken(SQLParser.UPDATE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode SET() { return getToken(SQLParser.SET, 0); }
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public TerminalNode WHERE() { return getToken(SQLParser.WHERE, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public UpdateCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_updateCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUpdateCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUpdateCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUpdateCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UpdateCMDContext updateCMD() throws RecognitionException {
		UpdateCMDContext _localctx = new UpdateCMDContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_updateCMD);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(UPDATE);
			setState(112);
			relationName();
			setState(113);
			match(SET);
			setState(114);
			attributeName();
			setState(115);
			match(T__4);
			setState(116);
			literal();
			setState(124);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(117);
				match(T__5);
				setState(118);
				attributeName();
				setState(119);
				match(T__4);
				setState(120);
				literal();
				}
				}
				setState(126);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(127);
			match(WHERE);
			setState(128);
			condition();
			setState(129);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertCMDContext extends ParserRuleContext {
		public TerminalNode INSERT() { return getToken(SQLParser.INSERT, 0); }
		public TerminalNode INTO() { return getToken(SQLParser.INTO, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode VALUES() { return getToken(SQLParser.VALUES, 0); }
		public TerminalNode FROM() { return getToken(SQLParser.FROM, 0); }
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public TerminalNode RELATION() { return getToken(SQLParser.RELATION, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public InsertCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterInsertCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitInsertCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitInsertCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsertCMDContext insertCMD() throws RecognitionException {
		InsertCMDContext _localctx = new InsertCMDContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_insertCMD);
		int _la;
		try {
			setState(157);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(131);
				match(INSERT);
				setState(132);
				match(INTO);
				setState(133);
				relationName();
				setState(134);
				match(VALUES);
				setState(135);
				match(FROM);
				setState(136);
				match(T__1);
				setState(137);
				literal();
				setState(142);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(138);
					match(T__5);
					setState(139);
					literal();
					}
					}
					setState(144);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(145);
				match(T__2);
				setState(146);
				match(T__0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(148);
				match(INSERT);
				setState(149);
				match(INTO);
				setState(150);
				relationName();
				setState(151);
				match(VALUES);
				setState(152);
				match(FROM);
				setState(153);
				match(RELATION);
				setState(154);
				expr();
				setState(155);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteCMDContext extends ParserRuleContext {
		public TerminalNode DELETE() { return getToken(SQLParser.DELETE, 0); }
		public TerminalNode FROM() { return getToken(SQLParser.FROM, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode WHERE() { return getToken(SQLParser.WHERE, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public DeleteCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeleteCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeleteCMD(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDeleteCMD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeleteCMDContext deleteCMD() throws RecognitionException {
		DeleteCMDContext _localctx = new DeleteCMDContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_deleteCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			match(DELETE);
			setState(160);
			match(FROM);
			setState(161);
			relationName();
			setState(162);
			match(WHERE);
			setState(163);
			condition();
			setState(164);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitQuery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitQuery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			relationName();
			setState(167);
			match(T__6);
			setState(168);
			expr();
			setState(169);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public SelectionEXPContext selectionEXP() {
			return getRuleContext(SelectionEXPContext.class,0);
		}
		public ProjectionEXPContext projectionEXP() {
			return getRuleContext(ProjectionEXPContext.class,0);
		}
		public RenamingEXPContext renamingEXP() {
			return getRuleContext(RenamingEXPContext.class,0);
		}
		public UnionEXPContext unionEXP() {
			return getRuleContext(UnionEXPContext.class,0);
		}
		public DifferenceEXPContext differenceEXP() {
			return getRuleContext(DifferenceEXPContext.class,0);
		}
		public ProductEXPContext productEXP() {
			return getRuleContext(ProductEXPContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_expr);
		try {
			setState(178);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(171);
				atomicEXPR();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(172);
				selectionEXP();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(173);
				projectionEXP();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(174);
				renamingEXP();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(175);
				unionEXP();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(176);
				differenceEXP();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(177);
				productEXP();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicEXPRContext extends ParserRuleContext {
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AtomicEXPRContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicEXPR; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAtomicEXPR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAtomicEXPR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAtomicEXPR(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomicEXPRContext atomicEXPR() throws RecognitionException {
		AtomicEXPRContext _localctx = new AtomicEXPRContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_atomicEXPR);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				{
				setState(180);
				relationName();
				}
				break;
			case T__1:
				{
				setState(181);
				match(T__1);
				setState(182);
				expr();
				setState(183);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectionEXPContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(SQLParser.SELECT, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public SelectionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelectionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelectionEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelectionEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectionEXPContext selectionEXP() throws RecognitionException {
		SelectionEXPContext _localctx = new SelectionEXPContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_selectionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			match(SELECT);
			setState(188);
			match(T__1);
			setState(189);
			condition();
			setState(190);
			match(T__2);
			setState(191);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProjectionEXPContext extends ParserRuleContext {
		public TerminalNode PROJECT() { return getToken(SQLParser.PROJECT, 0); }
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public ProjectionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projectionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProjectionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProjectionEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitProjectionEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProjectionEXPContext projectionEXP() throws RecognitionException {
		ProjectionEXPContext _localctx = new ProjectionEXPContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_projectionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193);
			match(PROJECT);
			setState(194);
			match(T__1);
			setState(195);
			attributeList();
			setState(196);
			match(T__2);
			setState(197);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RenamingEXPContext extends ParserRuleContext {
		public TerminalNode RENAME() { return getToken(SQLParser.RENAME, 0); }
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public RenamingEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_renamingEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterRenamingEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitRenamingEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitRenamingEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RenamingEXPContext renamingEXP() throws RecognitionException {
		RenamingEXPContext _localctx = new RenamingEXPContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_renamingEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(199);
			match(RENAME);
			setState(200);
			match(T__1);
			setState(201);
			attributeList();
			setState(202);
			match(T__2);
			setState(203);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnionEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public UnionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnionEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUnionEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnionEXPContext unionEXP() throws RecognitionException {
		UnionEXPContext _localctx = new UnionEXPContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_unionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			atomicEXPR();
			setState(206);
			match(T__7);
			setState(207);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DifferenceEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public DifferenceEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_differenceEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDifferenceEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDifferenceEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDifferenceEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DifferenceEXPContext differenceEXP() throws RecognitionException {
		DifferenceEXPContext _localctx = new DifferenceEXPContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_differenceEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			atomicEXPR();
			setState(210);
			match(T__8);
			setState(211);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProductEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public ProductEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_productEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProductEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProductEXP(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitProductEXP(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProductEXPContext productEXP() throws RecognitionException {
		ProductEXPContext _localctx = new ProductEXPContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_productEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(213);
			atomicEXPR();
			setState(214);
			match(T__9);
			setState(215);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public List<ConjunctionContext> conjunction() {
			return getRuleContexts(ConjunctionContext.class);
		}
		public ConjunctionContext conjunction(int i) {
			return getRuleContext(ConjunctionContext.class,i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(217);
			conjunction();
			setState(222);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(218);
				match(T__10);
				setState(219);
				conjunction();
				}
				}
				setState(224);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConjunctionContext extends ParserRuleContext {
		public List<ComparisonContext> comparison() {
			return getRuleContexts(ComparisonContext.class);
		}
		public ComparisonContext comparison(int i) {
			return getRuleContext(ComparisonContext.class,i);
		}
		public ConjunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterConjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitConjunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitConjunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConjunctionContext conjunction() throws RecognitionException {
		ConjunctionContext _localctx = new ConjunctionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_conjunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			comparison();
			setState(230);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__11) {
				{
				{
				setState(226);
				match(T__11);
				setState(227);
				comparison();
				}
				}
				setState(232);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public List<OperandContext> operand() {
			return getRuleContexts(OperandContext.class);
		}
		public OperandContext operand(int i) {
			return getRuleContext(OperandContext.class,i);
		}
		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class,0);
		}
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitComparison(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitComparison(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_comparison);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARCHAR:
			case NULL:
			case IDENTIFIER:
			case INTEGER:
				{
				setState(233);
				operand();
				setState(234);
				operator();
				setState(235);
				operand();
				}
				break;
			case T__1:
				{
				setState(237);
				match(T__1);
				setState(238);
				condition();
				setState(239);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(SQLParser.INTEGER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_type);
		try {
			setState(248);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__12:
				enterOuterAlt(_localctx, 1);
				{
				setState(243);
				match(T__12);
				setState(244);
				match(T__1);
				setState(245);
				match(INTEGER);
				setState(246);
				match(T__2);
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 2);
				{
				setState(247);
				match(T__13);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode NULL() { return getToken(SQLParser.NULL, 0); }
		public TerminalNode INTEGER() { return getToken(SQLParser.INTEGER, 0); }
		public TerminalNode VARCHAR() { return getToken(SQLParser.VARCHAR, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(250);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARCHAR) | (1L << NULL) | (1L << INTEGER))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(252);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOperand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOperand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOperand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_operand);
		try {
			setState(256);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(254);
				match(IDENTIFIER);
				}
				break;
			case VARCHAR:
			case NULL:
			case INTEGER:
				enterOuterAlt(_localctx, 2);
				{
				setState(255);
				literal();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public RelationNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterRelationName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitRelationName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitRelationName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationNameContext relationName() throws RecognitionException {
		RelationNameContext _localctx = new RelationNameContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_relationName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public AttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAttributeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAttributeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeNameContext attributeName() throws RecognitionException {
		AttributeNameContext _localctx = new AttributeNameContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_attributeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(260);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedAttributeListContext extends ParserRuleContext {
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TypedAttributeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedAttributeList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTypedAttributeList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTypedAttributeList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTypedAttributeList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypedAttributeListContext typedAttributeList() throws RecognitionException {
		TypedAttributeListContext _localctx = new TypedAttributeListContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_typedAttributeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			attributeName();
			setState(263);
			type();
			setState(270);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(264);
				match(T__5);
				setState(265);
				attributeName();
				setState(266);
				type();
				}
				}
				setState(272);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeListContext extends ParserRuleContext {
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public AttributeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAttributeList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAttributeList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAttributeList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeListContext attributeList() throws RecognitionException {
		AttributeListContext _localctx = new AttributeListContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_attributeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(273);
			attributeName();
			setState(278);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(274);
				match(T__5);
				setState(275);
				attributeName();
				}
				}
				setState(280);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\60\u011c\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \3\2"+
		"\3\2\7\2C\n\2\f\2\16\2F\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3Q"+
		"\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3"+
		"\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\n}\n\n\f\n\16\n\u0080\13\n\3\n\3\n"+
		"\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u008f\n\13"+
		"\f\13\16\13\u0092\13\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\5\13\u00a0\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r"+
		"\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00b5\n\16\3\17\3\17"+
		"\3\17\3\17\3\17\5\17\u00bc\n\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23"+
		"\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\7\26\u00df\n\26"+
		"\f\26\16\26\u00e2\13\26\3\27\3\27\3\27\7\27\u00e7\n\27\f\27\16\27\u00ea"+
		"\13\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u00f4\n\30\3\31\3"+
		"\31\3\31\3\31\3\31\5\31\u00fb\n\31\3\32\3\32\3\33\3\33\3\34\3\34\5\34"+
		"\u0103\n\34\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\7\37\u010f"+
		"\n\37\f\37\16\37\u0112\13\37\3 \3 \3 \7 \u0117\n \f \16 \u011a\13 \3 "+
		"\2\2!\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>"+
		"\2\4\4\2*+--\3\2\21\26\2\u0117\2D\3\2\2\2\4P\3\2\2\2\6R\3\2\2\2\bV\3\2"+
		"\2\2\nZ\3\2\2\2\f^\3\2\2\2\16a\3\2\2\2\20e\3\2\2\2\22q\3\2\2\2\24\u009f"+
		"\3\2\2\2\26\u00a1\3\2\2\2\30\u00a8\3\2\2\2\32\u00b4\3\2\2\2\34\u00bb\3"+
		"\2\2\2\36\u00bd\3\2\2\2 \u00c3\3\2\2\2\"\u00c9\3\2\2\2$\u00cf\3\2\2\2"+
		"&\u00d3\3\2\2\2(\u00d7\3\2\2\2*\u00db\3\2\2\2,\u00e3\3\2\2\2.\u00f3\3"+
		"\2\2\2\60\u00fa\3\2\2\2\62\u00fc\3\2\2\2\64\u00fe\3\2\2\2\66\u0102\3\2"+
		"\2\28\u0104\3\2\2\2:\u0106\3\2\2\2<\u0108\3\2\2\2>\u0113\3\2\2\2@C\5\4"+
		"\3\2AC\5\30\r\2B@\3\2\2\2BA\3\2\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2E\3\3"+
		"\2\2\2FD\3\2\2\2GQ\5\6\4\2HQ\5\b\5\2IQ\5\n\6\2JQ\5\f\7\2KQ\5\16\b\2LQ"+
		"\5\20\t\2MQ\5\22\n\2NQ\5\24\13\2OQ\5\26\f\2PG\3\2\2\2PH\3\2\2\2PI\3\2"+
		"\2\2PJ\3\2\2\2PK\3\2\2\2PL\3\2\2\2PM\3\2\2\2PN\3\2\2\2PO\3\2\2\2Q\5\3"+
		"\2\2\2RS\7\31\2\2ST\58\35\2TU\7\3\2\2U\7\3\2\2\2VW\7\32\2\2WX\58\35\2"+
		"XY\7\3\2\2Y\t\3\2\2\2Z[\7\33\2\2[\\\58\35\2\\]\7\3\2\2]\13\3\2\2\2^_\7"+
		"\34\2\2_`\7\3\2\2`\r\3\2\2\2ab\7\35\2\2bc\5\34\17\2cd\7\3\2\2d\17\3\2"+
		"\2\2ef\7\27\2\2fg\7\30\2\2gh\58\35\2hi\7\4\2\2ij\5<\37\2jk\7\5\2\2kl\7"+
		"\6\2\2lm\7\4\2\2mn\5> \2no\7\5\2\2op\7\3\2\2p\21\3\2\2\2qr\7\36\2\2rs"+
		"\58\35\2st\7\"\2\2tu\5:\36\2uv\7\7\2\2v~\5\62\32\2wx\7\b\2\2xy\5:\36\2"+
		"yz\7\7\2\2z{\5\62\32\2{}\3\2\2\2|w\3\2\2\2}\u0080\3\2\2\2~|\3\2\2\2~\177"+
		"\3\2\2\2\177\u0081\3\2\2\2\u0080~\3\2\2\2\u0081\u0082\7$\2\2\u0082\u0083"+
		"\5*\26\2\u0083\u0084\7\3\2\2\u0084\23\3\2\2\2\u0085\u0086\7\37\2\2\u0086"+
		"\u0087\7 \2\2\u0087\u0088\58\35\2\u0088\u0089\7#\2\2\u0089\u008a\7%\2"+
		"\2\u008a\u008b\7\4\2\2\u008b\u0090\5\62\32\2\u008c\u008d\7\b\2\2\u008d"+
		"\u008f\5\62\32\2\u008e\u008c\3\2\2\2\u008f\u0092\3\2\2\2\u0090\u008e\3"+
		"\2\2\2\u0090\u0091\3\2\2\2\u0091\u0093\3\2\2\2\u0092\u0090\3\2\2\2\u0093"+
		"\u0094\7\5\2\2\u0094\u0095\7\3\2\2\u0095\u00a0\3\2\2\2\u0096\u0097\7\37"+
		"\2\2\u0097\u0098\7 \2\2\u0098\u0099\58\35\2\u0099\u009a\7#\2\2\u009a\u009b"+
		"\7%\2\2\u009b\u009c\7&\2\2\u009c\u009d\5\32\16\2\u009d\u009e\7\3\2\2\u009e"+
		"\u00a0\3\2\2\2\u009f\u0085\3\2\2\2\u009f\u0096\3\2\2\2\u00a0\25\3\2\2"+
		"\2\u00a1\u00a2\7!\2\2\u00a2\u00a3\7%\2\2\u00a3\u00a4\58\35\2\u00a4\u00a5"+
		"\7$\2\2\u00a5\u00a6\5*\26\2\u00a6\u00a7\7\3\2\2\u00a7\27\3\2\2\2\u00a8"+
		"\u00a9\58\35\2\u00a9\u00aa\7\t\2\2\u00aa\u00ab\5\32\16\2\u00ab\u00ac\7"+
		"\3\2\2\u00ac\31\3\2\2\2\u00ad\u00b5\5\34\17\2\u00ae\u00b5\5\36\20\2\u00af"+
		"\u00b5\5 \21\2\u00b0\u00b5\5\"\22\2\u00b1\u00b5\5$\23\2\u00b2\u00b5\5"+
		"&\24\2\u00b3\u00b5\5(\25\2\u00b4\u00ad\3\2\2\2\u00b4\u00ae\3\2\2\2\u00b4"+
		"\u00af\3\2\2\2\u00b4\u00b0\3\2\2\2\u00b4\u00b1\3\2\2\2\u00b4\u00b2\3\2"+
		"\2\2\u00b4\u00b3\3\2\2\2\u00b5\33\3\2\2\2\u00b6\u00bc\58\35\2\u00b7\u00b8"+
		"\7\4\2\2\u00b8\u00b9\5\32\16\2\u00b9\u00ba\7\5\2\2\u00ba\u00bc\3\2\2\2"+
		"\u00bb\u00b6\3\2\2\2\u00bb\u00b7\3\2\2\2\u00bc\35\3\2\2\2\u00bd\u00be"+
		"\7\'\2\2\u00be\u00bf\7\4\2\2\u00bf\u00c0\5*\26\2\u00c0\u00c1\7\5\2\2\u00c1"+
		"\u00c2\5\34\17\2\u00c2\37\3\2\2\2\u00c3\u00c4\7(\2\2\u00c4\u00c5\7\4\2"+
		"\2\u00c5\u00c6\5> \2\u00c6\u00c7\7\5\2\2\u00c7\u00c8\5\34\17\2\u00c8!"+
		"\3\2\2\2\u00c9\u00ca\7)\2\2\u00ca\u00cb\7\4\2\2\u00cb\u00cc\5> \2\u00cc"+
		"\u00cd\7\5\2\2\u00cd\u00ce\5\34\17\2\u00ce#\3\2\2\2\u00cf\u00d0\5\34\17"+
		"\2\u00d0\u00d1\7\n\2\2\u00d1\u00d2\5\34\17\2\u00d2%\3\2\2\2\u00d3\u00d4"+
		"\5\34\17\2\u00d4\u00d5\7\13\2\2\u00d5\u00d6\5\34\17\2\u00d6\'\3\2\2\2"+
		"\u00d7\u00d8\5\34\17\2\u00d8\u00d9\7\f\2\2\u00d9\u00da\5\34\17\2\u00da"+
		")\3\2\2\2\u00db\u00e0\5,\27\2\u00dc\u00dd\7\r\2\2\u00dd\u00df\5,\27\2"+
		"\u00de\u00dc\3\2\2\2\u00df\u00e2\3\2\2\2\u00e0\u00de\3\2\2\2\u00e0\u00e1"+
		"\3\2\2\2\u00e1+\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e3\u00e8\5.\30\2\u00e4"+
		"\u00e5\7\16\2\2\u00e5\u00e7\5.\30\2\u00e6\u00e4\3\2\2\2\u00e7\u00ea\3"+
		"\2\2\2\u00e8\u00e6\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9-\3\2\2\2\u00ea\u00e8"+
		"\3\2\2\2\u00eb\u00ec\5\66\34\2\u00ec\u00ed\5\64\33\2\u00ed\u00ee\5\66"+
		"\34\2\u00ee\u00f4\3\2\2\2\u00ef\u00f0\7\4\2\2\u00f0\u00f1\5*\26\2\u00f1"+
		"\u00f2\7\5\2\2\u00f2\u00f4\3\2\2\2\u00f3\u00eb\3\2\2\2\u00f3\u00ef\3\2"+
		"\2\2\u00f4/\3\2\2\2\u00f5\u00f6\7\17\2\2\u00f6\u00f7\7\4\2\2\u00f7\u00f8"+
		"\7-\2\2\u00f8\u00fb\7\5\2\2\u00f9\u00fb\7\20\2\2\u00fa\u00f5\3\2\2\2\u00fa"+
		"\u00f9\3\2\2\2\u00fb\61\3\2\2\2\u00fc\u00fd\t\2\2\2\u00fd\63\3\2\2\2\u00fe"+
		"\u00ff\t\3\2\2\u00ff\65\3\2\2\2\u0100\u0103\7,\2\2\u0101\u0103\5\62\32"+
		"\2\u0102\u0100\3\2\2\2\u0102\u0101\3\2\2\2\u0103\67\3\2\2\2\u0104\u0105"+
		"\7,\2\2\u01059\3\2\2\2\u0106\u0107\7,\2\2\u0107;\3\2\2\2\u0108\u0109\5"+
		":\36\2\u0109\u0110\5\60\31\2\u010a\u010b\7\b\2\2\u010b\u010c\5:\36\2\u010c"+
		"\u010d\5\60\31\2\u010d\u010f\3\2\2\2\u010e\u010a\3\2\2\2\u010f\u0112\3"+
		"\2\2\2\u0110\u010e\3\2\2\2\u0110\u0111\3\2\2\2\u0111=\3\2\2\2\u0112\u0110"+
		"\3\2\2\2\u0113\u0118\5:\36\2\u0114\u0115\7\b\2\2\u0115\u0117\5:\36\2\u0116"+
		"\u0114\3\2\2\2\u0117\u011a\3\2\2\2\u0118\u0116\3\2\2\2\u0118\u0119\3\2"+
		"\2\2\u0119?\3\2\2\2\u011a\u0118\3\2\2\2\21BDP~\u0090\u009f\u00b4\u00bb"+
		"\u00e0\u00e8\u00f3\u00fa\u0102\u0110\u0118";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}