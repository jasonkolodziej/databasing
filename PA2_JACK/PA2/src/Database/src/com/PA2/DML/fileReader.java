package Database.src.com.PA2.DML;
/*
    Package -- grammer --
        -- Used for file to access user generated
*/

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.*;

/*
    Notes:
        It's important to note we override SQLLexer's and SQLParser's syntax error reporting by throwing runtime
        exception, then catching this in main.
*/
public class fileReader {
    public fileReader() {
    }

    public static void main(String[] args) {
        /* Read the contents of the file from the command line. */
        System.out.println( System.getProperty("user.dir") );
        try {
            FileReader file = new FileReader("input.txt");
            BufferedReader curr_reader = new BufferedReader(file);
            String line;
            SQLBaseListener listener = new SQLBaseListener();
            while ((line = curr_reader.readLine()) != null) {
                if (!line.equals("")) {
                    SQLLexer lexer = new SQLLexer(CharStreams.fromString(line));
                    SQLParser parser = new SQLParser(new CommonTokenStream(lexer));
                    ParseTree root = parser.program();
                    ParseTreeWalker walker = new ParseTreeWalker();
                    walker.walk( listener, root );
                }
                System.out.println(line);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Cannot find the file.");
            System.exit(-1);
        }
        catch (IOException e) {
            System.out.println( "readLine() failure." );
            System.exit(-1);
        }
    }
}
