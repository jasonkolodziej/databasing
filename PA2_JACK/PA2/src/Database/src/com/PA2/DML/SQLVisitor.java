// Generated from /Users/johnkeegan/Desktop/PA2/src/Database/src/com/PA2/DML/SQL.g4 by ANTLR 4.7
package Database.src.com.PA2.DML;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SQLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SQLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SQLParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(SQLParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommand(SQLParser.CommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#openCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpenCMD(SQLParser.OpenCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#closeCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloseCMD(SQLParser.CloseCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#writeCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWriteCMD(SQLParser.WriteCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#exitCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExitCMD(SQLParser.ExitCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#showCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShowCMD(SQLParser.ShowCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#createCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateCMD(SQLParser.CreateCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#updateCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdateCMD(SQLParser.UpdateCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#insertCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertCMD(SQLParser.InsertCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#deleteCMD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeleteCMD(SQLParser.DeleteCMDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuery(SQLParser.QueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SQLParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#atomicEXPR}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomicEXPR(SQLParser.AtomicEXPRContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#selectionEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectionEXP(SQLParser.SelectionEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#projectionEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjectionEXP(SQLParser.ProjectionEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#renamingEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRenamingEXP(SQLParser.RenamingEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#unionEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnionEXP(SQLParser.UnionEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#differenceEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDifferenceEXP(SQLParser.DifferenceEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#productEXP}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProductEXP(SQLParser.ProductEXPContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(SQLParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#conjunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConjunction(SQLParser.ConjunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#comparison}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison(SQLParser.ComparisonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(SQLParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(SQLParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(SQLParser.OperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#operand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperand(SQLParser.OperandContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#relationName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationName(SQLParser.RelationNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#attributeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeName(SQLParser.AttributeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#typedAttributeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedAttributeList(SQLParser.TypedAttributeListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#attributeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeList(SQLParser.AttributeListContext ctx);
}