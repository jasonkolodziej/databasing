// FIXME: Needed for writing to file. import java.io.FileWriter;                      // Uses BufferedWriter and FileWriter to send results to output.txt
// FIXME: Needed for writing to file. import java.io.BufferedWriter;
import java.io.IOException;                     // Exception thrown by BufferedWriter on failure opening file.
import java.util.Scanner;                       // Command line utility to help parse file.

import javafx.util.converter.CharacterStringConverter;
import org.antlr.v4.runtime.CharStreams;        // Allows lexer to read file's arguments.
import org.antlr.v4.runtime.CommonTokenStream;  // Creates Token stream based on lexer results.
import org.antlr.v4.runtime.tree.ParseTree;     // Parses tree to decide if this works.
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/* Custom exception handling class for some logical errors during SQL parsing. */
class SQLException extends Exception
{
    // Constructor
    public SQLException() {
        System.out.println( "Undefined SQLException occurred...awkward.");
        System.exit( -1 );
    }
    // Constructor accepting messages to print to the user
    public SQLException( String details )
    {
        super( details );
    }
}

/* This file parses 'input.txt', holding SQL statements which we determine are valid / invalid. */
public class FileReader
{
    public static void main( String[] args )
    {

        System.out.print( "Hello world! Beginning scan of file input.txt..." );
        Scanner fileReader = new Scanner( System.in );
        System.out.println( "done!");

        /* Iterate through the input file and evaluate the expressions passed. */
        Integer count = 1;
        try
        {
            while (fileReader.hasNext()) {
                String currentSQLInstruction = fileReader.nextLine();
                if (!currentSQLInstruction.equals(""))
                {
                    SQLLexer lexer = new SQLLexer(CharStreams.fromString(currentSQLInstruction));
                    SQLParser parser = new SQLParser(new CommonTokenStream(lexer));
                    ParseTree root = parser.program();
                    /*
                        If the number of syntax errors for the current statement
                        are greater than 1, terminate the program with a syntax error
                    */
                    if ( parser.getNumberOfSyntaxErrors() >= 1)
                    {
                        System.out.println( "error occured ");
                        throw new SQLException("SyntaxError Occured\nStatement - \"" + currentSQLInstruction + "\"");
                    }
                    ParseTreeWalker walker = new ParseTreeWalker();
                    SQLBaseListener listener = new SQLBaseListener();
                    walker.walk( listener, root);
                    /* Pending there are no syntax errors, then we begin to parse the statement. */
                }
            }
        }
        catch ( SQLException err )
        {
            System.err.print(err);
            System.exit(-1 );
        }


    }
}
