// Generated from SQL.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLParser}.
 */
public interface SQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(SQLParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(SQLParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(SQLParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(SQLParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#operator}.
	 * @param ctx the parse tree
	 */
	void enterOperator(SQLParser.OperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#operator}.
	 * @param ctx the parse tree
	 */
	void exitOperator(SQLParser.OperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#operand}.
	 * @param ctx the parse tree
	 */
	void enterOperand(SQLParser.OperandContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#operand}.
	 * @param ctx the parse tree
	 */
	void exitOperand(SQLParser.OperandContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#relationName}.
	 * @param ctx the parse tree
	 */
	void enterRelationName(SQLParser.RelationNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#relationName}.
	 * @param ctx the parse tree
	 */
	void exitRelationName(SQLParser.RelationNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void enterAttributeName(SQLParser.AttributeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void exitAttributeName(SQLParser.AttributeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#typedAttributeList}.
	 * @param ctx the parse tree
	 */
	void enterTypedAttributeList(SQLParser.TypedAttributeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#typedAttributeList}.
	 * @param ctx the parse tree
	 */
	void exitTypedAttributeList(SQLParser.TypedAttributeListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#attributeList}.
	 * @param ctx the parse tree
	 */
	void enterAttributeList(SQLParser.AttributeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#attributeList}.
	 * @param ctx the parse tree
	 */
	void exitAttributeList(SQLParser.AttributeListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(SQLParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(SQLParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(SQLParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(SQLParser.CommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(SQLParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(SQLParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#openCMD}.
	 * @param ctx the parse tree
	 */
	void enterOpenCMD(SQLParser.OpenCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#openCMD}.
	 * @param ctx the parse tree
	 */
	void exitOpenCMD(SQLParser.OpenCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#closeCMD}.
	 * @param ctx the parse tree
	 */
	void enterCloseCMD(SQLParser.CloseCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#closeCMD}.
	 * @param ctx the parse tree
	 */
	void exitCloseCMD(SQLParser.CloseCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#writeCMD}.
	 * @param ctx the parse tree
	 */
	void enterWriteCMD(SQLParser.WriteCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#writeCMD}.
	 * @param ctx the parse tree
	 */
	void exitWriteCMD(SQLParser.WriteCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#exitCMD}.
	 * @param ctx the parse tree
	 */
	void enterExitCMD(SQLParser.ExitCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#exitCMD}.
	 * @param ctx the parse tree
	 */
	void exitExitCMD(SQLParser.ExitCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#showCMD}.
	 * @param ctx the parse tree
	 */
	void enterShowCMD(SQLParser.ShowCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#showCMD}.
	 * @param ctx the parse tree
	 */
	void exitShowCMD(SQLParser.ShowCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#createCMD}.
	 * @param ctx the parse tree
	 */
	void enterCreateCMD(SQLParser.CreateCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#createCMD}.
	 * @param ctx the parse tree
	 */
	void exitCreateCMD(SQLParser.CreateCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#updateCMD}.
	 * @param ctx the parse tree
	 */
	void enterUpdateCMD(SQLParser.UpdateCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#updateCMD}.
	 * @param ctx the parse tree
	 */
	void exitUpdateCMD(SQLParser.UpdateCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#insertCMD}.
	 * @param ctx the parse tree
	 */
	void enterInsertCMD(SQLParser.InsertCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#insertCMD}.
	 * @param ctx the parse tree
	 */
	void exitInsertCMD(SQLParser.InsertCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#deleteCMD}.
	 * @param ctx the parse tree
	 */
	void enterDeleteCMD(SQLParser.DeleteCMDContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#deleteCMD}.
	 * @param ctx the parse tree
	 */
	void exitDeleteCMD(SQLParser.DeleteCMDContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(SQLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(SQLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#atomicEXPR}.
	 * @param ctx the parse tree
	 */
	void enterAtomicEXPR(SQLParser.AtomicEXPRContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#atomicEXPR}.
	 * @param ctx the parse tree
	 */
	void exitAtomicEXPR(SQLParser.AtomicEXPRContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#selectionEXP}.
	 * @param ctx the parse tree
	 */
	void enterSelectionEXP(SQLParser.SelectionEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#selectionEXP}.
	 * @param ctx the parse tree
	 */
	void exitSelectionEXP(SQLParser.SelectionEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#projectionEXP}.
	 * @param ctx the parse tree
	 */
	void enterProjectionEXP(SQLParser.ProjectionEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#projectionEXP}.
	 * @param ctx the parse tree
	 */
	void exitProjectionEXP(SQLParser.ProjectionEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#renamingEXP}.
	 * @param ctx the parse tree
	 */
	void enterRenamingEXP(SQLParser.RenamingEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#renamingEXP}.
	 * @param ctx the parse tree
	 */
	void exitRenamingEXP(SQLParser.RenamingEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unionEXP}.
	 * @param ctx the parse tree
	 */
	void enterUnionEXP(SQLParser.UnionEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unionEXP}.
	 * @param ctx the parse tree
	 */
	void exitUnionEXP(SQLParser.UnionEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#differenceEXP}.
	 * @param ctx the parse tree
	 */
	void enterDifferenceEXP(SQLParser.DifferenceEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#differenceEXP}.
	 * @param ctx the parse tree
	 */
	void exitDifferenceEXP(SQLParser.DifferenceEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#productEXP}.
	 * @param ctx the parse tree
	 */
	void enterProductEXP(SQLParser.ProductEXPContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#productEXP}.
	 * @param ctx the parse tree
	 */
	void exitProductEXP(SQLParser.ProductEXPContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(SQLParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(SQLParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void enterConjunction(SQLParser.ConjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void exitConjunction(SQLParser.ConjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(SQLParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(SQLParser.ComparisonContext ctx);
}