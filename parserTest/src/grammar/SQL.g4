grammar SQL;

/* Regexs ordered from most specific matches to least. */
CREATE          : [Cc][Rr][Ee][Aa][Tt][Ee];         // Create Command
TABLE           : [Tt][Aa][Bb][Ll][Ee];             // Create [ Table | Database ]
OPEN            : [Oo][Pp[Ee][Nn];                  // Open Command
CLOSE           : [Cc][Ll][Oo][Ss][Ee];             // Close Command
WRITE           : [Ww][Rr][Ii][Tt][Ee];             // Write Command
EXIT            : [Ee][Xx][Ii][Tt];                 // Exit Command
SHOW            : [Ss][Hh][Oo][Ww];                 // Show Command
UPDATE          : [Uu][Pp][Dd][Aa][Tt][Ee];         // Update Expression
INSERT          : [Ii][Nn][Ss][Ee][Rr][Tt];         // Insert Expression
INTO            : [Ii][Nn][Tt][Oo];                 // Into Keyword
DELETE          : [Dd][Ee][Ll][Ee][Tt][Ee];         // Delete Command
SET             : [Ss][Ee][Tt];                     // Operation?
VALUES          : [Vv][Aa][Ll][Uu][Ee][Ss];         // Values Keyword
WHERE           : [Ww][Hh][Ee][Rr][Ee];             // Where Evaluation
FROM            : [Ff][Rr][Oo][Mm];                 // From Keyword
RELATION        : [Rr][Ee][Ll][Aa][Tt][Ii][Oo][Nn]; // Relation Keyword
SELECT          : [Ss][Ee][Ll][Ee][Cc][Tt];         // Select Expression
PROJECT         : [Pp][Rr][Oo][Jj][Ee][Cc][Tt];     // Project Expression
RENAME          : [Rr][Ee][Nn][Aa][Mm][Ee];         // Rename Expression
VARCHAR         :'"'(~["])*'"';                     // FIXME: Will break Value Attribute
NULL            : 'NULL' | 'null' ;                 // Null Type
IDENTIFIER      : ALPHA (ALPHA | DIGIT)*;           // Identity Attribute
INTEGER         : DIGIT (DIGIT)*;                   // Value Attribute
ALPHA           :[a-zA-Z_]+;                        // Entered text
DIGIT           :[0-9]+;                            // Int value
/*
    Identifiers, organized by regular expression
    -- type     -- Represents string input for declaring variables in creating a table.
    -- literal  -- The operand's value, which is string or int type.
    -- operator -- Defines boolean expressions between two operands.
    -- relationName -- The name of the relation in question.
    -- typedAttributeList -- Attribute and type declarations list.
    -- attributeList    -- Attibute listing.
*/
type                    : 'VARCHAR' '(' INTEGER ')' | 'INTEGER';
literal                 : NULL | INTEGER | VARCHAR ;
operator                : ('<' | '>' | '<=' | '>=' | '!=' | '==');
operand                 : IDENTIFIER | literal;
relationName            : IDENTIFIER;
attributeName           : IDENTIFIER;
typedAttributeList      : attributeName type(',' attributeName type)*;
attributeList           : attributeName (',' attributeName)*;

/* Main thread */
program
    : (command | query)*;
/* Command instructions */
command
    : openCMD
    | closeCMD
    | writeCMD
    | exitCMD
    | showCMD
    | createCMD
    | updateCMD
    | insertCMD
    | deleteCMD
    ;
/* Queries */
query
    : relationName '<-' expr ';'
    ;
/* -- Open Command --  */
openCMD
    : OPEN relationName ';'
    ;
/* -- Close Command -- */
closeCMD
    : CLOSE relationName ';'
    ;
/* -- Write Command -- */
writeCMD
    : WRITE relationName ';'
    ;
/* Exit Command */
exitCMD
    : EXIT ';'
    ;
/* Show Command */
showCMD
    : SHOW atomicEXPR ';';
/* Create Command */
createCMD
    : CREATE TABLE relationName '(' typedAttributeList ')' 'PRIMARY KEY' '(' attributeList ')' ';';
/* Update Command */
updateCMD
    : UPDATE relationName SET attributeName '=' literal( ',' attributeName '=' literal)* WHERE condition ';';
/* Insert Command */
insertCMD
    : INSERT INTO relationName VALUES FROM '(' literal (',' literal)* ')'';'
    | INSERT INTO relationName VALUES FROM RELATION expr';';
/* Delete Command */
deleteCMD
    : DELETE FROM relationName WHERE condition ';'
    ;
/* Query Expressions */
expr
    : atomicEXPR
    | selectionEXP
    | projectionEXP
    | renamingEXP
    | unionEXP
    | differenceEXP
    | productEXP
    ;
atomicEXPR
    : (relationName
    | '(' expr ')')
    ;
/* Selection Expression */
selectionEXP
    : SELECT '(' condition ')' atomicEXPR
    ;
/* Projection Expression */
projectionEXP
    : PROJECT '('attributeList')' atomicEXPR
    ;
/* Renaming Expression */
renamingEXP
    : RENAME '('attributeList')' atomicEXPR
    ;
/* Addition */
unionEXP
    : atomicEXPR '+' atomicEXPR
    ;
/* Difference */
differenceEXP
    : atomicEXPR '-' atomicEXPR
    ;
/* Product */
productEXP
    : atomicEXPR '*' atomicEXPR
    ;
/*
    Conditional logic
    -- condition -- Combines all comparisons into ||s if necessary
    -- comparison -- Fetches the operations into &&s if necessary.
    -- conjunction -- Combines operation with && into one statement
*/
condition
    : conjunction ( '||' conjunction )*
    ;
conjunction
    : comparison ( '&&' comparison)*
    ;
comparison
    : ( operand operator operand
    | '(' condition ')' )
    ;
/* Whitespace */
WS :[ \t\r\n]+ -> skip;
