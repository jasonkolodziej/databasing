/*
    Package -- grammer --
        -- Used for file to access user generated
*/
import grammar.*;
import java.io.FileWriter;                      // Uses BufferedWriter and FileWriter to send results to output.txt
import java.io.BufferedWriter;
import java.io.IOException;                     // Exception thrown by BufferedWriter on failure opening file.
import java.util.Scanner;                       // Command line utility to help parse file.
import org.antlr.v4.runtime.CharStreams;        // Allows lexer to read file's arguments.
import org.antlr.v4.runtime.CommonTokenStream;  // Creates Token stream based on lexer results.
import org.antlr.v4.runtime.tree.ParseTree;     // Parses tree to decide if this works.
/*
    Notes:
        It's important to note we override SQLLexer's and SQLParser's syntax error reporting by throwing runtime
        exception, then catching this in main.
*/
public class FileReader {
    public FileReader() {
    }

    public static void main(String[] args) {
        /* Read the contents of the file from the command line. */
        Scanner file = new Scanner(System.in);
        /* Create the class that handles syntax erros. */
        SQLSyntaxError errorHandler = new SQLSyntaxError();
        /*
            currentLine holds the value of the line read as the file is being parsed.
        */
        String currentLine = new String();
        try {
            /*
                outputWriter instantiates a writer object to send output to 'output.txt'.
            */
            BufferedWriter outputWriter = new BufferedWriter(
                    new FileWriter("output.txt")
            );
            /*  While we are not at EOF, send each line to the parser ( if not empty ) and allow parser to decide
                if it's a valid statement.
            */
            Integer count = 1;
            while (file.hasNext()) {
                /* Reads the current line of the file. */
                try {
                    currentLine = file.nextLine();
                    /* Though not necessary, */
                    if (!currentLine.equals("")) {
                        /* Create the lexer and replace the error-handling for syntax errors. */
                        SQLLexer lexer = new SQLLexer(CharStreams.fromString(currentLine));
                        lexer.removeErrorListeners();
                        lexer.addErrorListener(errorHandler);

                        /* Create the parser and replace the error-handling for syntax errors. */
                        SQLParser parser = new SQLParser(new CommonTokenStream(lexer));
                        parser.removeErrorListeners();
                        parser.addErrorListener(errorHandler);
                        ParseTree root = parser.program();
                        outputWriter.write("Line " + count + " passed.\n");
                        count += 1;
                    }
                }
                catch (RuntimeException SyntaxError) {
                    /* Report failure to outputWriter as well. */
                    outputWriter.write( "Line " + count + " failed.\n");
                    count += 1;
                }
            }
            outputWriter.close();
            System.out.println( "Output send to 'output.txt'." );
        }
        catch (IOException outFileIssue) {
            /* If the file does not exist, throw exception. */
            System.out.println( outFileIssue.getMessage());
        }
    }
}
