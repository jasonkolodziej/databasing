// Generated from SQL.g4 by ANTLR 4.7.1
package grammar;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, CREATE=21, TABLE=22, OPEN=23, CLOSE=24, 
		WRITE=25, EXIT=26, SHOW=27, UPDATE=28, INSERT=29, INTO=30, DELETE=31, 
		SET=32, VALUES=33, WHERE=34, FROM=35, RELATION=36, SELECT=37, PROJECT=38, 
		RENAME=39, VARCHAR=40, NULL=41, IDENTIFIER=42, INTEGER=43, ALPHA=44, DIGIT=45, 
		WS=46;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "CREATE", "TABLE", "OPEN", "CLOSE", "WRITE", 
		"EXIT", "SHOW", "UPDATE", "INSERT", "INTO", "DELETE", "SET", "VALUES", 
		"WHERE", "FROM", "RELATION", "SELECT", "PROJECT", "RENAME", "VARCHAR", 
		"NULL", "IDENTIFIER", "INTEGER", "ALPHA", "DIGIT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'VARCHAR'", "'('", "')'", "'INTEGER'", "'<'", "'>'", "'<='", "'>='", 
		"'!='", "'=='", "','", "'<-'", "';'", "'PRIMARY KEY'", "'='", "'+'", "'-'", 
		"'*'", "'||'", "'&&'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "CREATE", "TABLE", 
		"OPEN", "CLOSE", "WRITE", "EXIT", "SHOW", "UPDATE", "INSERT", "INTO", 
		"DELETE", "SET", "VALUES", "WHERE", "FROM", "RELATION", "SELECT", "PROJECT", 
		"RENAME", "VARCHAR", "NULL", "IDENTIFIER", "INTEGER", "ALPHA", "DIGIT", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SQLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\60\u014d\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4"+
		"\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\t\3"+
		"\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3"+
		"\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3"+
		"\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3"+
		"\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3"+
		"\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3 \3 "+
		"\3 \3 \3 \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3"+
		"#\3#\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3"+
		"\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3(\3)\3)\7)\u011d\n)"+
		"\f)\16)\u0120\13)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\5*\u012c\n*\3+\3+\3+\7"+
		"+\u0131\n+\f+\16+\u0134\13+\3,\3,\7,\u0138\n,\f,\16,\u013b\13,\3-\6-\u013e"+
		"\n-\r-\16-\u013f\3.\6.\u0143\n.\r.\16.\u0144\3/\6/\u0148\n/\r/\16/\u0149"+
		"\3/\3/\2\2\60\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16"+
		"\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34"+
		"\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60\3\2\34\4\2EEee\4"+
		"\2TTtt\4\2GGgg\4\2CCcc\4\2VVvv\4\2DDdd\4\2NNnn\4\2QQqq\7\2GGRR]]ggrr\4"+
		"\2PPpp\4\2UUuu\4\2YYyy\4\2KKkk\4\2ZZzz\4\2JJjj\4\2WWww\4\2RRrr\4\2FFf"+
		"f\4\2XXxx\4\2HHhh\4\2OOoo\4\2LLll\3\2$$\5\2C\\aac|\3\2\62;\5\2\13\f\17"+
		"\17\"\"\2\u0154\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3"+
		"\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2"+
		"\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3"+
		"\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2"+
		"\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\2"+
		"9\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3"+
		"\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2"+
		"\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\3"+
		"_\3\2\2\2\5g\3\2\2\2\7i\3\2\2\2\tk\3\2\2\2\13s\3\2\2\2\ru\3\2\2\2\17w"+
		"\3\2\2\2\21z\3\2\2\2\23}\3\2\2\2\25\u0080\3\2\2\2\27\u0083\3\2\2\2\31"+
		"\u0085\3\2\2\2\33\u0088\3\2\2\2\35\u008a\3\2\2\2\37\u0096\3\2\2\2!\u0098"+
		"\3\2\2\2#\u009a\3\2\2\2%\u009c\3\2\2\2\'\u009e\3\2\2\2)\u00a1\3\2\2\2"+
		"+\u00a4\3\2\2\2-\u00ab\3\2\2\2/\u00b1\3\2\2\2\61\u00b5\3\2\2\2\63\u00bb"+
		"\3\2\2\2\65\u00c1\3\2\2\2\67\u00c6\3\2\2\29\u00cb\3\2\2\2;\u00d2\3\2\2"+
		"\2=\u00d9\3\2\2\2?\u00de\3\2\2\2A\u00e5\3\2\2\2C\u00e9\3\2\2\2E\u00f0"+
		"\3\2\2\2G\u00f6\3\2\2\2I\u00fb\3\2\2\2K\u0104\3\2\2\2M\u010b\3\2\2\2O"+
		"\u0113\3\2\2\2Q\u011a\3\2\2\2S\u012b\3\2\2\2U\u012d\3\2\2\2W\u0135\3\2"+
		"\2\2Y\u013d\3\2\2\2[\u0142\3\2\2\2]\u0147\3\2\2\2_`\7X\2\2`a\7C\2\2ab"+
		"\7T\2\2bc\7E\2\2cd\7J\2\2de\7C\2\2ef\7T\2\2f\4\3\2\2\2gh\7*\2\2h\6\3\2"+
		"\2\2ij\7+\2\2j\b\3\2\2\2kl\7K\2\2lm\7P\2\2mn\7V\2\2no\7G\2\2op\7I\2\2"+
		"pq\7G\2\2qr\7T\2\2r\n\3\2\2\2st\7>\2\2t\f\3\2\2\2uv\7@\2\2v\16\3\2\2\2"+
		"wx\7>\2\2xy\7?\2\2y\20\3\2\2\2z{\7@\2\2{|\7?\2\2|\22\3\2\2\2}~\7#\2\2"+
		"~\177\7?\2\2\177\24\3\2\2\2\u0080\u0081\7?\2\2\u0081\u0082\7?\2\2\u0082"+
		"\26\3\2\2\2\u0083\u0084\7.\2\2\u0084\30\3\2\2\2\u0085\u0086\7>\2\2\u0086"+
		"\u0087\7/\2\2\u0087\32\3\2\2\2\u0088\u0089\7=\2\2\u0089\34\3\2\2\2\u008a"+
		"\u008b\7R\2\2\u008b\u008c\7T\2\2\u008c\u008d\7K\2\2\u008d\u008e\7O\2\2"+
		"\u008e\u008f\7C\2\2\u008f\u0090\7T\2\2\u0090\u0091\7[\2\2\u0091\u0092"+
		"\7\"\2\2\u0092\u0093\7M\2\2\u0093\u0094\7G\2\2\u0094\u0095\7[\2\2\u0095"+
		"\36\3\2\2\2\u0096\u0097\7?\2\2\u0097 \3\2\2\2\u0098\u0099\7-\2\2\u0099"+
		"\"\3\2\2\2\u009a\u009b\7/\2\2\u009b$\3\2\2\2\u009c\u009d\7,\2\2\u009d"+
		"&\3\2\2\2\u009e\u009f\7~\2\2\u009f\u00a0\7~\2\2\u00a0(\3\2\2\2\u00a1\u00a2"+
		"\7(\2\2\u00a2\u00a3\7(\2\2\u00a3*\3\2\2\2\u00a4\u00a5\t\2\2\2\u00a5\u00a6"+
		"\t\3\2\2\u00a6\u00a7\t\4\2\2\u00a7\u00a8\t\5\2\2\u00a8\u00a9\t\6\2\2\u00a9"+
		"\u00aa\t\4\2\2\u00aa,\3\2\2\2\u00ab\u00ac\t\6\2\2\u00ac\u00ad\t\5\2\2"+
		"\u00ad\u00ae\t\7\2\2\u00ae\u00af\t\b\2\2\u00af\u00b0\t\4\2\2\u00b0.\3"+
		"\2\2\2\u00b1\u00b2\t\t\2\2\u00b2\u00b3\t\n\2\2\u00b3\u00b4\t\13\2\2\u00b4"+
		"\60\3\2\2\2\u00b5\u00b6\t\2\2\2\u00b6\u00b7\t\b\2\2\u00b7\u00b8\t\t\2"+
		"\2\u00b8\u00b9\t\f\2\2\u00b9\u00ba\t\4\2\2\u00ba\62\3\2\2\2\u00bb\u00bc"+
		"\t\r\2\2\u00bc\u00bd\t\3\2\2\u00bd\u00be\t\16\2\2\u00be\u00bf\t\6\2\2"+
		"\u00bf\u00c0\t\4\2\2\u00c0\64\3\2\2\2\u00c1\u00c2\t\4\2\2\u00c2\u00c3"+
		"\t\17\2\2\u00c3\u00c4\t\16\2\2\u00c4\u00c5\t\6\2\2\u00c5\66\3\2\2\2\u00c6"+
		"\u00c7\t\f\2\2\u00c7\u00c8\t\20\2\2\u00c8\u00c9\t\t\2\2\u00c9\u00ca\t"+
		"\r\2\2\u00ca8\3\2\2\2\u00cb\u00cc\t\21\2\2\u00cc\u00cd\t\22\2\2\u00cd"+
		"\u00ce\t\23\2\2\u00ce\u00cf\t\5\2\2\u00cf\u00d0\t\6\2\2\u00d0\u00d1\t"+
		"\4\2\2\u00d1:\3\2\2\2\u00d2\u00d3\t\16\2\2\u00d3\u00d4\t\13\2\2\u00d4"+
		"\u00d5\t\f\2\2\u00d5\u00d6\t\4\2\2\u00d6\u00d7\t\3\2\2\u00d7\u00d8\t\6"+
		"\2\2\u00d8<\3\2\2\2\u00d9\u00da\t\16\2\2\u00da\u00db\t\13\2\2\u00db\u00dc"+
		"\t\6\2\2\u00dc\u00dd\t\t\2\2\u00dd>\3\2\2\2\u00de\u00df\t\23\2\2\u00df"+
		"\u00e0\t\4\2\2\u00e0\u00e1\t\b\2\2\u00e1\u00e2\t\4\2\2\u00e2\u00e3\t\6"+
		"\2\2\u00e3\u00e4\t\4\2\2\u00e4@\3\2\2\2\u00e5\u00e6\t\f\2\2\u00e6\u00e7"+
		"\t\4\2\2\u00e7\u00e8\t\6\2\2\u00e8B\3\2\2\2\u00e9\u00ea\t\24\2\2\u00ea"+
		"\u00eb\t\5\2\2\u00eb\u00ec\t\b\2\2\u00ec\u00ed\t\21\2\2\u00ed\u00ee\t"+
		"\4\2\2\u00ee\u00ef\t\f\2\2\u00efD\3\2\2\2\u00f0\u00f1\t\r\2\2\u00f1\u00f2"+
		"\t\20\2\2\u00f2\u00f3\t\4\2\2\u00f3\u00f4\t\3\2\2\u00f4\u00f5\t\4\2\2"+
		"\u00f5F\3\2\2\2\u00f6\u00f7\t\25\2\2\u00f7\u00f8\t\3\2\2\u00f8\u00f9\t"+
		"\t\2\2\u00f9\u00fa\t\26\2\2\u00faH\3\2\2\2\u00fb\u00fc\t\3\2\2\u00fc\u00fd"+
		"\t\4\2\2\u00fd\u00fe\t\b\2\2\u00fe\u00ff\t\5\2\2\u00ff\u0100\t\6\2\2\u0100"+
		"\u0101\t\16\2\2\u0101\u0102\t\t\2\2\u0102\u0103\t\13\2\2\u0103J\3\2\2"+
		"\2\u0104\u0105\t\f\2\2\u0105\u0106\t\4\2\2\u0106\u0107\t\b\2\2\u0107\u0108"+
		"\t\4\2\2\u0108\u0109\t\2\2\2\u0109\u010a\t\6\2\2\u010aL\3\2\2\2\u010b"+
		"\u010c\t\22\2\2\u010c\u010d\t\3\2\2\u010d\u010e\t\t\2\2\u010e\u010f\t"+
		"\27\2\2\u010f\u0110\t\4\2\2\u0110\u0111\t\2\2\2\u0111\u0112\t\6\2\2\u0112"+
		"N\3\2\2\2\u0113\u0114\t\3\2\2\u0114\u0115\t\4\2\2\u0115\u0116\t\13\2\2"+
		"\u0116\u0117\t\5\2\2\u0117\u0118\t\26\2\2\u0118\u0119\t\4\2\2\u0119P\3"+
		"\2\2\2\u011a\u011e\7$\2\2\u011b\u011d\n\30\2\2\u011c\u011b\3\2\2\2\u011d"+
		"\u0120\3\2\2\2\u011e\u011c\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0121\3\2"+
		"\2\2\u0120\u011e\3\2\2\2\u0121\u0122\7$\2\2\u0122R\3\2\2\2\u0123\u0124"+
		"\7P\2\2\u0124\u0125\7W\2\2\u0125\u0126\7N\2\2\u0126\u012c\7N\2\2\u0127"+
		"\u0128\7p\2\2\u0128\u0129\7w\2\2\u0129\u012a\7n\2\2\u012a\u012c\7n\2\2"+
		"\u012b\u0123\3\2\2\2\u012b\u0127\3\2\2\2\u012cT\3\2\2\2\u012d\u0132\5"+
		"Y-\2\u012e\u0131\5Y-\2\u012f\u0131\5[.\2\u0130\u012e\3\2\2\2\u0130\u012f"+
		"\3\2\2\2\u0131\u0134\3\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133"+
		"V\3\2\2\2\u0134\u0132\3\2\2\2\u0135\u0139\5[.\2\u0136\u0138\5[.\2\u0137"+
		"\u0136\3\2\2\2\u0138\u013b\3\2\2\2\u0139\u0137\3\2\2\2\u0139\u013a\3\2"+
		"\2\2\u013aX\3\2\2\2\u013b\u0139\3\2\2\2\u013c\u013e\t\31\2\2\u013d\u013c"+
		"\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140"+
		"Z\3\2\2\2\u0141\u0143\t\32\2\2\u0142\u0141\3\2\2\2\u0143\u0144\3\2\2\2"+
		"\u0144\u0142\3\2\2\2\u0144\u0145\3\2\2\2\u0145\\\3\2\2\2\u0146\u0148\t"+
		"\33\2\2\u0147\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u0147\3\2\2\2\u0149"+
		"\u014a\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\b/\2\2\u014c^\3\2\2\2\13"+
		"\2\u011e\u012b\u0130\u0132\u0139\u013f\u0144\u0149\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}