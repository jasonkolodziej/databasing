package grammar;
import org.antlr.v4.runtime.*;
/*
    Custom class to override error reporting for syntaxError is BaseErrorListener
    After finding BaseErrorListener, the method throws a Runtime Exception instead.
    Sources 
        -- https://stackoverflow.com/questions/44515370/how-to-override-error-reporting-in-c-target-of-antlr4
*/
public class SQLSyntaxError extends BaseErrorListener{
    @Override
    public void syntaxError( Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e )
    {
        throw new RuntimeException("Invalid syntax.");
    }

}
