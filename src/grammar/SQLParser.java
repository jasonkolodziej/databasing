// Generated from SQL.g4 by ANTLR 4.7.1
package grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, CREATE=21, TABLE=22, OPEN=23, CLOSE=24, 
		WRITE=25, EXIT=26, SHOW=27, UPDATE=28, INSERT=29, INTO=30, DELETE=31, 
		SET=32, VALUES=33, WHERE=34, FROM=35, RELATION=36, SELECT=37, PROJECT=38, 
		RENAME=39, VARCHAR=40, NULL=41, IDENTIFIER=42, INTEGER=43, ALPHA=44, DIGIT=45, 
		WS=46;
	public static final int
		RULE_type = 0, RULE_literal = 1, RULE_operator = 2, RULE_operand = 3, 
		RULE_relationName = 4, RULE_attributeName = 5, RULE_typedAttributeList = 6, 
		RULE_attributeList = 7, RULE_program = 8, RULE_command = 9, RULE_query = 10, 
		RULE_openCMD = 11, RULE_closeCMD = 12, RULE_writeCMD = 13, RULE_exitCMD = 14, 
		RULE_showCMD = 15, RULE_createCMD = 16, RULE_updateCMD = 17, RULE_insertCMD = 18, 
		RULE_deleteCMD = 19, RULE_expr = 20, RULE_atomicEXPR = 21, RULE_selectionEXP = 22, 
		RULE_projectionEXP = 23, RULE_renamingEXP = 24, RULE_unionEXP = 25, RULE_differenceEXP = 26, 
		RULE_productEXP = 27, RULE_condition = 28, RULE_conjunction = 29, RULE_comparison = 30;
	public static final String[] ruleNames = {
		"type", "literal", "operator", "operand", "relationName", "attributeName", 
		"typedAttributeList", "attributeList", "program", "command", "query", 
		"openCMD", "closeCMD", "writeCMD", "exitCMD", "showCMD", "createCMD", 
		"updateCMD", "insertCMD", "deleteCMD", "expr", "atomicEXPR", "selectionEXP", 
		"projectionEXP", "renamingEXP", "unionEXP", "differenceEXP", "productEXP", 
		"condition", "conjunction", "comparison"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'VARCHAR'", "'('", "')'", "'INTEGER'", "'<'", "'>'", "'<='", "'>='", 
		"'!='", "'=='", "','", "'<-'", "';'", "'PRIMARY KEY'", "'='", "'+'", "'-'", 
		"'*'", "'||'", "'&&'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "CREATE", "TABLE", 
		"OPEN", "CLOSE", "WRITE", "EXIT", "SHOW", "UPDATE", "INSERT", "INTO", 
		"DELETE", "SET", "VALUES", "WHERE", "FROM", "RELATION", "SELECT", "PROJECT", 
		"RENAME", "VARCHAR", "NULL", "IDENTIFIER", "INTEGER", "ALPHA", "DIGIT", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class TypeContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(SQLParser.INTEGER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_type);
		try {
			setState(67);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(62);
				match(T__0);
				setState(63);
				match(T__1);
				setState(64);
				match(INTEGER);
				setState(65);
				match(T__2);
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 2);
				{
				setState(66);
				match(T__3);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode NULL() { return getToken(SQLParser.NULL, 0); }
		public TerminalNode INTEGER() { return getToken(SQLParser.INTEGER, 0); }
		public TerminalNode VARCHAR() { return getToken(SQLParser.VARCHAR, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARCHAR) | (1L << NULL) | (1L << INTEGER))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOperator(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOperand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOperand(this);
		}
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_operand);
		try {
			setState(75);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(73);
				match(IDENTIFIER);
				}
				break;
			case VARCHAR:
			case NULL:
			case INTEGER:
				enterOuterAlt(_localctx, 2);
				{
				setState(74);
				literal();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public RelationNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterRelationName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitRelationName(this);
		}
	}

	public final RelationNameContext relationName() throws RecognitionException {
		RelationNameContext _localctx = new RelationNameContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_relationName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public AttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAttributeName(this);
		}
	}

	public final AttributeNameContext attributeName() throws RecognitionException {
		AttributeNameContext _localctx = new AttributeNameContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_attributeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedAttributeListContext extends ParserRuleContext {
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TypedAttributeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedAttributeList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTypedAttributeList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTypedAttributeList(this);
		}
	}

	public final TypedAttributeListContext typedAttributeList() throws RecognitionException {
		TypedAttributeListContext _localctx = new TypedAttributeListContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_typedAttributeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			attributeName();
			setState(82);
			type();
			setState(89);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(83);
				match(T__10);
				setState(84);
				attributeName();
				setState(85);
				type();
				}
				}
				setState(91);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeListContext extends ParserRuleContext {
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public AttributeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAttributeList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAttributeList(this);
		}
	}

	public final AttributeListContext attributeList() throws RecognitionException {
		AttributeListContext _localctx = new AttributeListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_attributeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			attributeName();
			setState(97);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(93);
				match(T__10);
				setState(94);
				attributeName();
				}
				}
				setState(99);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<QueryContext> query() {
			return getRuleContexts(QueryContext.class);
		}
		public QueryContext query(int i) {
			return getRuleContext(QueryContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CREATE) | (1L << OPEN) | (1L << CLOSE) | (1L << WRITE) | (1L << EXIT) | (1L << SHOW) | (1L << UPDATE) | (1L << INSERT) | (1L << DELETE) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(102);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CREATE:
				case OPEN:
				case CLOSE:
				case WRITE:
				case EXIT:
				case SHOW:
				case UPDATE:
				case INSERT:
				case DELETE:
					{
					setState(100);
					command();
					}
					break;
				case IDENTIFIER:
					{
					setState(101);
					query();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public OpenCMDContext openCMD() {
			return getRuleContext(OpenCMDContext.class,0);
		}
		public CloseCMDContext closeCMD() {
			return getRuleContext(CloseCMDContext.class,0);
		}
		public WriteCMDContext writeCMD() {
			return getRuleContext(WriteCMDContext.class,0);
		}
		public ExitCMDContext exitCMD() {
			return getRuleContext(ExitCMDContext.class,0);
		}
		public ShowCMDContext showCMD() {
			return getRuleContext(ShowCMDContext.class,0);
		}
		public CreateCMDContext createCMD() {
			return getRuleContext(CreateCMDContext.class,0);
		}
		public UpdateCMDContext updateCMD() {
			return getRuleContext(UpdateCMDContext.class,0);
		}
		public InsertCMDContext insertCMD() {
			return getRuleContext(InsertCMDContext.class,0);
		}
		public DeleteCMDContext deleteCMD() {
			return getRuleContext(DeleteCMDContext.class,0);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCommand(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_command);
		try {
			setState(116);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(107);
				openCMD();
				}
				break;
			case CLOSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(108);
				closeCMD();
				}
				break;
			case WRITE:
				enterOuterAlt(_localctx, 3);
				{
				setState(109);
				writeCMD();
				}
				break;
			case EXIT:
				enterOuterAlt(_localctx, 4);
				{
				setState(110);
				exitCMD();
				}
				break;
			case SHOW:
				enterOuterAlt(_localctx, 5);
				{
				setState(111);
				showCMD();
				}
				break;
			case CREATE:
				enterOuterAlt(_localctx, 6);
				{
				setState(112);
				createCMD();
				}
				break;
			case UPDATE:
				enterOuterAlt(_localctx, 7);
				{
				setState(113);
				updateCMD();
				}
				break;
			case INSERT:
				enterOuterAlt(_localctx, 8);
				{
				setState(114);
				insertCMD();
				}
				break;
			case DELETE:
				enterOuterAlt(_localctx, 9);
				{
				setState(115);
				deleteCMD();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			relationName();
			setState(119);
			match(T__11);
			setState(120);
			expr();
			setState(121);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpenCMDContext extends ParserRuleContext {
		public TerminalNode OPEN() { return getToken(SQLParser.OPEN, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public OpenCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_openCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOpenCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOpenCMD(this);
		}
	}

	public final OpenCMDContext openCMD() throws RecognitionException {
		OpenCMDContext _localctx = new OpenCMDContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_openCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			match(OPEN);
			setState(124);
			relationName();
			setState(125);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloseCMDContext extends ParserRuleContext {
		public TerminalNode CLOSE() { return getToken(SQLParser.CLOSE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public CloseCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closeCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCloseCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCloseCMD(this);
		}
	}

	public final CloseCMDContext closeCMD() throws RecognitionException {
		CloseCMDContext _localctx = new CloseCMDContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_closeCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(CLOSE);
			setState(128);
			relationName();
			setState(129);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WriteCMDContext extends ParserRuleContext {
		public TerminalNode WRITE() { return getToken(SQLParser.WRITE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public WriteCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_writeCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterWriteCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitWriteCMD(this);
		}
	}

	public final WriteCMDContext writeCMD() throws RecognitionException {
		WriteCMDContext _localctx = new WriteCMDContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_writeCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			match(WRITE);
			setState(132);
			relationName();
			setState(133);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExitCMDContext extends ParserRuleContext {
		public TerminalNode EXIT() { return getToken(SQLParser.EXIT, 0); }
		public ExitCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exitCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExitCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExitCMD(this);
		}
	}

	public final ExitCMDContext exitCMD() throws RecognitionException {
		ExitCMDContext _localctx = new ExitCMDContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_exitCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(EXIT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShowCMDContext extends ParserRuleContext {
		public TerminalNode SHOW() { return getToken(SQLParser.SHOW, 0); }
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public ShowCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_showCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterShowCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitShowCMD(this);
		}
	}

	public final ShowCMDContext showCMD() throws RecognitionException {
		ShowCMDContext _localctx = new ShowCMDContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_showCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(SHOW);
			setState(138);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateCMDContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(SQLParser.CREATE, 0); }
		public TerminalNode TABLE() { return getToken(SQLParser.TABLE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TypedAttributeListContext typedAttributeList() {
			return getRuleContext(TypedAttributeListContext.class,0);
		}
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public CreateCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreateCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreateCMD(this);
		}
	}

	public final CreateCMDContext createCMD() throws RecognitionException {
		CreateCMDContext _localctx = new CreateCMDContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_createCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(CREATE);
			setState(141);
			match(TABLE);
			setState(142);
			relationName();
			setState(143);
			match(T__1);
			setState(144);
			typedAttributeList();
			setState(145);
			match(T__2);
			setState(146);
			match(T__13);
			setState(147);
			match(T__1);
			setState(148);
			attributeList();
			setState(149);
			match(T__2);
			setState(150);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpdateCMDContext extends ParserRuleContext {
		public TerminalNode UPDATE() { return getToken(SQLParser.UPDATE, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode SET() { return getToken(SQLParser.SET, 0); }
		public List<AttributeNameContext> attributeName() {
			return getRuleContexts(AttributeNameContext.class);
		}
		public AttributeNameContext attributeName(int i) {
			return getRuleContext(AttributeNameContext.class,i);
		}
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public TerminalNode WHERE() { return getToken(SQLParser.WHERE, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public UpdateCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_updateCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUpdateCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUpdateCMD(this);
		}
	}

	public final UpdateCMDContext updateCMD() throws RecognitionException {
		UpdateCMDContext _localctx = new UpdateCMDContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_updateCMD);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			match(UPDATE);
			setState(153);
			relationName();
			setState(154);
			match(SET);
			setState(155);
			attributeName();
			setState(156);
			match(T__14);
			setState(157);
			literal();
			setState(165);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(158);
				match(T__10);
				setState(159);
				attributeName();
				setState(160);
				match(T__14);
				setState(161);
				literal();
				}
				}
				setState(167);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(168);
			match(WHERE);
			setState(169);
			condition();
			setState(170);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertCMDContext extends ParserRuleContext {
		public TerminalNode INSERT() { return getToken(SQLParser.INSERT, 0); }
		public TerminalNode INTO() { return getToken(SQLParser.INTO, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode VALUES() { return getToken(SQLParser.VALUES, 0); }
		public TerminalNode FROM() { return getToken(SQLParser.FROM, 0); }
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public TerminalNode RELATION() { return getToken(SQLParser.RELATION, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public InsertCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterInsertCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitInsertCMD(this);
		}
	}

	public final InsertCMDContext insertCMD() throws RecognitionException {
		InsertCMDContext _localctx = new InsertCMDContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_insertCMD);
		int _la;
		try {
			setState(198);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(172);
				match(INSERT);
				setState(173);
				match(INTO);
				setState(174);
				relationName();
				setState(175);
				match(VALUES);
				setState(176);
				match(FROM);
				setState(177);
				match(T__1);
				setState(178);
				literal();
				setState(183);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__10) {
					{
					{
					setState(179);
					match(T__10);
					setState(180);
					literal();
					}
					}
					setState(185);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(186);
				match(T__2);
				setState(187);
				match(T__12);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(189);
				match(INSERT);
				setState(190);
				match(INTO);
				setState(191);
				relationName();
				setState(192);
				match(VALUES);
				setState(193);
				match(FROM);
				setState(194);
				match(RELATION);
				setState(195);
				expr();
				setState(196);
				match(T__12);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteCMDContext extends ParserRuleContext {
		public TerminalNode DELETE() { return getToken(SQLParser.DELETE, 0); }
		public TerminalNode FROM() { return getToken(SQLParser.FROM, 0); }
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public TerminalNode WHERE() { return getToken(SQLParser.WHERE, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public DeleteCMDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteCMD; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeleteCMD(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeleteCMD(this);
		}
	}

	public final DeleteCMDContext deleteCMD() throws RecognitionException {
		DeleteCMDContext _localctx = new DeleteCMDContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_deleteCMD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(DELETE);
			setState(201);
			match(FROM);
			setState(202);
			relationName();
			setState(203);
			match(WHERE);
			setState(204);
			condition();
			setState(205);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public SelectionEXPContext selectionEXP() {
			return getRuleContext(SelectionEXPContext.class,0);
		}
		public ProjectionEXPContext projectionEXP() {
			return getRuleContext(ProjectionEXPContext.class,0);
		}
		public RenamingEXPContext renamingEXP() {
			return getRuleContext(RenamingEXPContext.class,0);
		}
		public UnionEXPContext unionEXP() {
			return getRuleContext(UnionEXPContext.class,0);
		}
		public DifferenceEXPContext differenceEXP() {
			return getRuleContext(DifferenceEXPContext.class,0);
		}
		public ProductEXPContext productEXP() {
			return getRuleContext(ProductEXPContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_expr);
		try {
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(207);
				atomicEXPR();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(208);
				selectionEXP();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(209);
				projectionEXP();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(210);
				renamingEXP();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(211);
				unionEXP();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(212);
				differenceEXP();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(213);
				productEXP();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicEXPRContext extends ParserRuleContext {
		public RelationNameContext relationName() {
			return getRuleContext(RelationNameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AtomicEXPRContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicEXPR; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAtomicEXPR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAtomicEXPR(this);
		}
	}

	public final AtomicEXPRContext atomicEXPR() throws RecognitionException {
		AtomicEXPRContext _localctx = new AtomicEXPRContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_atomicEXPR);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				{
				setState(216);
				relationName();
				}
				break;
			case T__1:
				{
				setState(217);
				match(T__1);
				setState(218);
				expr();
				setState(219);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectionEXPContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(SQLParser.SELECT, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public SelectionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelectionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelectionEXP(this);
		}
	}

	public final SelectionEXPContext selectionEXP() throws RecognitionException {
		SelectionEXPContext _localctx = new SelectionEXPContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_selectionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			match(SELECT);
			setState(224);
			match(T__1);
			setState(225);
			condition();
			setState(226);
			match(T__2);
			setState(227);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProjectionEXPContext extends ParserRuleContext {
		public TerminalNode PROJECT() { return getToken(SQLParser.PROJECT, 0); }
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public ProjectionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projectionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProjectionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProjectionEXP(this);
		}
	}

	public final ProjectionEXPContext projectionEXP() throws RecognitionException {
		ProjectionEXPContext _localctx = new ProjectionEXPContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_projectionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			match(PROJECT);
			setState(230);
			match(T__1);
			setState(231);
			attributeList();
			setState(232);
			match(T__2);
			setState(233);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RenamingEXPContext extends ParserRuleContext {
		public TerminalNode RENAME() { return getToken(SQLParser.RENAME, 0); }
		public AttributeListContext attributeList() {
			return getRuleContext(AttributeListContext.class,0);
		}
		public AtomicEXPRContext atomicEXPR() {
			return getRuleContext(AtomicEXPRContext.class,0);
		}
		public RenamingEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_renamingEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterRenamingEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitRenamingEXP(this);
		}
	}

	public final RenamingEXPContext renamingEXP() throws RecognitionException {
		RenamingEXPContext _localctx = new RenamingEXPContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_renamingEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			match(RENAME);
			setState(236);
			match(T__1);
			setState(237);
			attributeList();
			setState(238);
			match(T__2);
			setState(239);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnionEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public UnionEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unionEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnionEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnionEXP(this);
		}
	}

	public final UnionEXPContext unionEXP() throws RecognitionException {
		UnionEXPContext _localctx = new UnionEXPContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_unionEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			atomicEXPR();
			setState(242);
			match(T__15);
			setState(243);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DifferenceEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public DifferenceEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_differenceEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDifferenceEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDifferenceEXP(this);
		}
	}

	public final DifferenceEXPContext differenceEXP() throws RecognitionException {
		DifferenceEXPContext _localctx = new DifferenceEXPContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_differenceEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(245);
			atomicEXPR();
			setState(246);
			match(T__16);
			setState(247);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProductEXPContext extends ParserRuleContext {
		public List<AtomicEXPRContext> atomicEXPR() {
			return getRuleContexts(AtomicEXPRContext.class);
		}
		public AtomicEXPRContext atomicEXPR(int i) {
			return getRuleContext(AtomicEXPRContext.class,i);
		}
		public ProductEXPContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_productEXP; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProductEXP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProductEXP(this);
		}
	}

	public final ProductEXPContext productEXP() throws RecognitionException {
		ProductEXPContext _localctx = new ProductEXPContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_productEXP);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			atomicEXPR();
			setState(250);
			match(T__17);
			setState(251);
			atomicEXPR();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public List<ConjunctionContext> conjunction() {
			return getRuleContexts(ConjunctionContext.class);
		}
		public ConjunctionContext conjunction(int i) {
			return getRuleContext(ConjunctionContext.class,i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(253);
			conjunction();
			setState(258);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__18) {
				{
				{
				setState(254);
				match(T__18);
				setState(255);
				conjunction();
				}
				}
				setState(260);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConjunctionContext extends ParserRuleContext {
		public List<ComparisonContext> comparison() {
			return getRuleContexts(ComparisonContext.class);
		}
		public ComparisonContext comparison(int i) {
			return getRuleContext(ComparisonContext.class,i);
		}
		public ConjunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterConjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitConjunction(this);
		}
	}

	public final ConjunctionContext conjunction() throws RecognitionException {
		ConjunctionContext _localctx = new ConjunctionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_conjunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			comparison();
			setState(266);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__19) {
				{
				{
				setState(262);
				match(T__19);
				setState(263);
				comparison();
				}
				}
				setState(268);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public List<OperandContext> operand() {
			return getRuleContexts(OperandContext.class);
		}
		public OperandContext operand(int i) {
			return getRuleContext(OperandContext.class,i);
		}
		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class,0);
		}
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitComparison(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_comparison);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(277);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARCHAR:
			case NULL:
			case IDENTIFIER:
			case INTEGER:
				{
				setState(269);
				operand();
				setState(270);
				operator();
				setState(271);
				operand();
				}
				break;
			case T__1:
				{
				setState(273);
				match(T__1);
				setState(274);
				condition();
				setState(275);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\60\u011a\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \3\2"+
		"\3\2\3\2\3\2\3\2\5\2F\n\2\3\3\3\3\3\4\3\4\3\5\3\5\5\5N\n\5\3\6\3\6\3\7"+
		"\3\7\3\b\3\b\3\b\3\b\3\b\3\b\7\bZ\n\b\f\b\16\b]\13\b\3\t\3\t\3\t\7\tb"+
		"\n\t\f\t\16\te\13\t\3\n\3\n\7\ni\n\n\f\n\16\nl\13\n\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\5\13w\n\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3"+
		"\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u00a6\n\23\f\23\16\23"+
		"\u00a9\13\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\7\24\u00b8\n\24\f\24\16\24\u00bb\13\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00c9\n\24\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00d9\n\26"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u00e0\n\27\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33"+
		"\3\33\3\33\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\7\36"+
		"\u0103\n\36\f\36\16\36\u0106\13\36\3\37\3\37\3\37\7\37\u010b\n\37\f\37"+
		"\16\37\u010e\13\37\3 \3 \3 \3 \3 \3 \3 \3 \5 \u0118\n \3 \2\2!\2\4\6\b"+
		"\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>\2\4\4\2*+--\3"+
		"\2\7\f\2\u0115\2E\3\2\2\2\4G\3\2\2\2\6I\3\2\2\2\bM\3\2\2\2\nO\3\2\2\2"+
		"\fQ\3\2\2\2\16S\3\2\2\2\20^\3\2\2\2\22j\3\2\2\2\24v\3\2\2\2\26x\3\2\2"+
		"\2\30}\3\2\2\2\32\u0081\3\2\2\2\34\u0085\3\2\2\2\36\u0089\3\2\2\2 \u008b"+
		"\3\2\2\2\"\u008e\3\2\2\2$\u009a\3\2\2\2&\u00c8\3\2\2\2(\u00ca\3\2\2\2"+
		"*\u00d8\3\2\2\2,\u00df\3\2\2\2.\u00e1\3\2\2\2\60\u00e7\3\2\2\2\62\u00ed"+
		"\3\2\2\2\64\u00f3\3\2\2\2\66\u00f7\3\2\2\28\u00fb\3\2\2\2:\u00ff\3\2\2"+
		"\2<\u0107\3\2\2\2>\u0117\3\2\2\2@A\7\3\2\2AB\7\4\2\2BC\7-\2\2CF\7\5\2"+
		"\2DF\7\6\2\2E@\3\2\2\2ED\3\2\2\2F\3\3\2\2\2GH\t\2\2\2H\5\3\2\2\2IJ\t\3"+
		"\2\2J\7\3\2\2\2KN\7,\2\2LN\5\4\3\2MK\3\2\2\2ML\3\2\2\2N\t\3\2\2\2OP\7"+
		",\2\2P\13\3\2\2\2QR\7,\2\2R\r\3\2\2\2ST\5\f\7\2T[\5\2\2\2UV\7\r\2\2VW"+
		"\5\f\7\2WX\5\2\2\2XZ\3\2\2\2YU\3\2\2\2Z]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2"+
		"\\\17\3\2\2\2][\3\2\2\2^c\5\f\7\2_`\7\r\2\2`b\5\f\7\2a_\3\2\2\2be\3\2"+
		"\2\2ca\3\2\2\2cd\3\2\2\2d\21\3\2\2\2ec\3\2\2\2fi\5\24\13\2gi\5\26\f\2"+
		"hf\3\2\2\2hg\3\2\2\2il\3\2\2\2jh\3\2\2\2jk\3\2\2\2k\23\3\2\2\2lj\3\2\2"+
		"\2mw\5\30\r\2nw\5\32\16\2ow\5\34\17\2pw\5\36\20\2qw\5 \21\2rw\5\"\22\2"+
		"sw\5$\23\2tw\5&\24\2uw\5(\25\2vm\3\2\2\2vn\3\2\2\2vo\3\2\2\2vp\3\2\2\2"+
		"vq\3\2\2\2vr\3\2\2\2vs\3\2\2\2vt\3\2\2\2vu\3\2\2\2w\25\3\2\2\2xy\5\n\6"+
		"\2yz\7\16\2\2z{\5*\26\2{|\7\17\2\2|\27\3\2\2\2}~\7\31\2\2~\177\5\n\6\2"+
		"\177\u0080\7\17\2\2\u0080\31\3\2\2\2\u0081\u0082\7\32\2\2\u0082\u0083"+
		"\5\n\6\2\u0083\u0084\7\17\2\2\u0084\33\3\2\2\2\u0085\u0086\7\33\2\2\u0086"+
		"\u0087\5\n\6\2\u0087\u0088\7\17\2\2\u0088\35\3\2\2\2\u0089\u008a\7\34"+
		"\2\2\u008a\37\3\2\2\2\u008b\u008c\7\35\2\2\u008c\u008d\5,\27\2\u008d!"+
		"\3\2\2\2\u008e\u008f\7\27\2\2\u008f\u0090\7\30\2\2\u0090\u0091\5\n\6\2"+
		"\u0091\u0092\7\4\2\2\u0092\u0093\5\16\b\2\u0093\u0094\7\5\2\2\u0094\u0095"+
		"\7\20\2\2\u0095\u0096\7\4\2\2\u0096\u0097\5\20\t\2\u0097\u0098\7\5\2\2"+
		"\u0098\u0099\7\17\2\2\u0099#\3\2\2\2\u009a\u009b\7\36\2\2\u009b\u009c"+
		"\5\n\6\2\u009c\u009d\7\"\2\2\u009d\u009e\5\f\7\2\u009e\u009f\7\21\2\2"+
		"\u009f\u00a7\5\4\3\2\u00a0\u00a1\7\r\2\2\u00a1\u00a2\5\f\7\2\u00a2\u00a3"+
		"\7\21\2\2\u00a3\u00a4\5\4\3\2\u00a4\u00a6\3\2\2\2\u00a5\u00a0\3\2\2\2"+
		"\u00a6\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00aa"+
		"\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00ab\7$\2\2\u00ab\u00ac\5:\36\2\u00ac"+
		"\u00ad\7\17\2\2\u00ad%\3\2\2\2\u00ae\u00af\7\37\2\2\u00af\u00b0\7 \2\2"+
		"\u00b0\u00b1\5\n\6\2\u00b1\u00b2\7#\2\2\u00b2\u00b3\7%\2\2\u00b3\u00b4"+
		"\7\4\2\2\u00b4\u00b9\5\4\3\2\u00b5\u00b6\7\r\2\2\u00b6\u00b8\5\4\3\2\u00b7"+
		"\u00b5\3\2\2\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2"+
		"\2\2\u00ba\u00bc\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7\5\2\2\u00bd"+
		"\u00be\7\17\2\2\u00be\u00c9\3\2\2\2\u00bf\u00c0\7\37\2\2\u00c0\u00c1\7"+
		" \2\2\u00c1\u00c2\5\n\6\2\u00c2\u00c3\7#\2\2\u00c3\u00c4\7%\2\2\u00c4"+
		"\u00c5\7&\2\2\u00c5\u00c6\5*\26\2\u00c6\u00c7\7\17\2\2\u00c7\u00c9\3\2"+
		"\2\2\u00c8\u00ae\3\2\2\2\u00c8\u00bf\3\2\2\2\u00c9\'\3\2\2\2\u00ca\u00cb"+
		"\7!\2\2\u00cb\u00cc\7%\2\2\u00cc\u00cd\5\n\6\2\u00cd\u00ce\7$\2\2\u00ce"+
		"\u00cf\5:\36\2\u00cf\u00d0\7\17\2\2\u00d0)\3\2\2\2\u00d1\u00d9\5,\27\2"+
		"\u00d2\u00d9\5.\30\2\u00d3\u00d9\5\60\31\2\u00d4\u00d9\5\62\32\2\u00d5"+
		"\u00d9\5\64\33\2\u00d6\u00d9\5\66\34\2\u00d7\u00d9\58\35\2\u00d8\u00d1"+
		"\3\2\2\2\u00d8\u00d2\3\2\2\2\u00d8\u00d3\3\2\2\2\u00d8\u00d4\3\2\2\2\u00d8"+
		"\u00d5\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d8\u00d7\3\2\2\2\u00d9+\3\2\2\2"+
		"\u00da\u00e0\5\n\6\2\u00db\u00dc\7\4\2\2\u00dc\u00dd\5*\26\2\u00dd\u00de"+
		"\7\5\2\2\u00de\u00e0\3\2\2\2\u00df\u00da\3\2\2\2\u00df\u00db\3\2\2\2\u00e0"+
		"-\3\2\2\2\u00e1\u00e2\7\'\2\2\u00e2\u00e3\7\4\2\2\u00e3\u00e4\5:\36\2"+
		"\u00e4\u00e5\7\5\2\2\u00e5\u00e6\5,\27\2\u00e6/\3\2\2\2\u00e7\u00e8\7"+
		"(\2\2\u00e8\u00e9\7\4\2\2\u00e9\u00ea\5\20\t\2\u00ea\u00eb\7\5\2\2\u00eb"+
		"\u00ec\5,\27\2\u00ec\61\3\2\2\2\u00ed\u00ee\7)\2\2\u00ee\u00ef\7\4\2\2"+
		"\u00ef\u00f0\5\20\t\2\u00f0\u00f1\7\5\2\2\u00f1\u00f2\5,\27\2\u00f2\63"+
		"\3\2\2\2\u00f3\u00f4\5,\27\2\u00f4\u00f5\7\22\2\2\u00f5\u00f6\5,\27\2"+
		"\u00f6\65\3\2\2\2\u00f7\u00f8\5,\27\2\u00f8\u00f9\7\23\2\2\u00f9\u00fa"+
		"\5,\27\2\u00fa\67\3\2\2\2\u00fb\u00fc\5,\27\2\u00fc\u00fd\7\24\2\2\u00fd"+
		"\u00fe\5,\27\2\u00fe9\3\2\2\2\u00ff\u0104\5<\37\2\u0100\u0101\7\25\2\2"+
		"\u0101\u0103\5<\37\2\u0102\u0100\3\2\2\2\u0103\u0106\3\2\2\2\u0104\u0102"+
		"\3\2\2\2\u0104\u0105\3\2\2\2\u0105;\3\2\2\2\u0106\u0104\3\2\2\2\u0107"+
		"\u010c\5> \2\u0108\u0109\7\26\2\2\u0109\u010b\5> \2\u010a\u0108\3\2\2"+
		"\2\u010b\u010e\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d="+
		"\3\2\2\2\u010e\u010c\3\2\2\2\u010f\u0110\5\b\5\2\u0110\u0111\5\6\4\2\u0111"+
		"\u0112\5\b\5\2\u0112\u0118\3\2\2\2\u0113\u0114\7\4\2\2\u0114\u0115\5:"+
		"\36\2\u0115\u0116\7\5\2\2\u0116\u0118\3\2\2\2\u0117\u010f\3\2\2\2\u0117"+
		"\u0113\3\2\2\2\u0118?\3\2\2\2\21EM[chjv\u00a7\u00b9\u00c8\u00d8\u00df"+
		"\u0104\u010c\u0117";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}